//
//  RequestRideVC.m
//  Groupool
//
//  Created by HN on 05/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "RequestRideVC.h"
#import "UIColor+GroupoolColors.h"
#import "ActionSheetDatePicker.h"
#import "ActionSheetStringPicker.h"
#import "CustomPagerViewController.h"
#import "UIView+Toast.h"
#import "FeedVC.h"



@interface RequestRideVC ()<UIGestureRecognizerDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    NSString *theDayString;
    NSString *tDate;
    
}

@property UIView *pickerView;

@end

@implementation RequestRideVC
{
    NSData *receivedData;
    NSString *userID;
    NSString *message;
    NSString *anonymous;
    NSString *startGrpID;
    NSString *endGrpID;
    
    NSMutableArray *fromGroupsName;
    NSMutableArray *toGroupsName;
    NSMutableArray *groupesName;
    NSMutableArray *rideTimeArray;
    NSMutableArray *fromGroupIDArray;
    NSMutableArray *toGroupIDArray;
    
    
    
    
}
@synthesize btnSelect;


-(void) viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    
    toggleIsON = NO;
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    [self fetchData];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    self.btnPost.layer.cornerRadius = 3;
    self.btnCancel.layer.cornerRadius = 3;
    
    [self.daySelector addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.selectedTime = [[NSDate new] dateByAddingTimeInterval:2700];
    
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
   
    NSDateFormatter *formatter;
    NSString *createdTime;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    createdTime = [formatter stringFromDate:self.selectedTime];

    
    [self.selectTime setTitle:createdTime forState:UIControlStateNormal];
    
}

- (void) backButtonAction {
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - Load Route Data from server
- (void)fetchData {
    [self.view makeToastActivity:CSToastPositionCenter];
    @try {
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_ROUTE_URL,userID]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
                 groupesName = [[NSMutableArray alloc] init];
                 fromGroupsName = [[NSMutableArray alloc] init];
                 toGroupsName = [[NSMutableArray alloc] init];
                 rideTimeArray = [[NSMutableArray alloc] init];
                 fromGroupIDArray = [[NSMutableArray alloc] init];
                 toGroupIDArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *routes in jsonArray) {
                     
                     [fromGroupsName addObject:routes[@"from_group_name"]];
                     [toGroupsName addObject:routes[@"to_group_name"]];
                     [rideTimeArray addObject:routes[@"modified_time"]];
                     [fromGroupIDArray addObject:routes[@"from_group_id"]];
                     [toGroupIDArray addObject:routes[@"to_group_id"]];
                     
                     
                 }
                 
                 [self checkForUpdatedData];
                 [self.view hideToastActivity];
             }
             else
             {
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self.view hideToastActivity];
                     [self reFetchData];
                     
                     
                 }];
                 [alert addAction:alertAction];
                 
                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}
- (void) reFetchData{
    [self fetchData];
}
- (void)checkForUpdatedData {
    
    BOOL fromGrpisEmpty = ([fromGroupsName count] == 0);
    BOOL toGrpisEmpty = ([toGroupsName count] == 0);
  
    
    if (!fromGrpisEmpty && !toGrpisEmpty) {
    
        NSString *tmpStr = [NSString stringWithFormat:@"%@ > %@",(fromGroupsName)[(NSUInteger) 0],(toGroupsName)[(NSUInteger) 0]];
        [btnSelect setTitle:tmpStr forState:UIControlStateNormal];
    
    }
   
}


- (IBAction)segmentAction:(UISegmentedControl *)sender {
    theDayString = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
    isDaySelected = YES;
  
    
    if (sender.selectedSegmentIndex == 0) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *now = [[NSDate alloc] init];
        tDate = [dateFormat stringFromDate:now];
  
    }else{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *now = [[NSDate alloc] init];
        int daysToAdd = 1;
        NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
        tDate = [dateFormat stringFromDate:tomorrowsDate];
  
    }
    
    NSLog(@"Date:%@",tDate);
    
}


#pragma mark - Request Ride Action
-(IBAction) postAction:(id)sender {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    
    style.messageFont = [UIFont systemFontOfSize:12];
    
    
    NSString *createdTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    createdTime = [dateFormatter stringFromDate:self.selectedTime];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    
    
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    if(formStTime < nowTime && (_daySelector.selectedSegmentIndex == 0)) {
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionBottom style:style];
        return;
        
    }
    
    
    NSString *theDate;
    NSString *timeString;
    
    if ([theDayString isEqualToString:@""] || theDayString == nil) {
        
        if (_daySelector.selectedSegmentIndex == 0) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [[NSDate alloc] init];
            tDate = [dateFormat stringFromDate:now];
           
        }else{
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *now = [[NSDate alloc] init];
            int daysToAdd = 1;
            NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            tDate = [dateFormat stringFromDate:tomorrowsDate];
            
        }
        
        
    }
    
    theDate = tDate;
    
    timeString = self.selectTime.titleLabel.text;
    message = [NSString stringWithFormat:@"%@ @ around %@ %@",self.btnSelect.titleLabel.text,timeString,[self.daySelector titleForSegmentAtIndex:self.daySelector.selectedSegmentIndex]];
    
    NSLog(@"Message:%@",message);
    
    if (anonymous == nil || [anonymous isEqualToString:@""]) {
        anonymous = @"0";
    }
    
    if (startGrpID == nil || endGrpID == nil) {
        startGrpID = [fromGroupIDArray objectAtIndex:0];
        endGrpID = [toGroupIDArray objectAtIndex:0];
    }
    
    /************** Post Method for Requesting Ride                ***********/
    
    
    @try {
        
        [self.view makeToastActivity:CSToastPositionCenter];
        
        userID = [userID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        message = [message stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        anonymous = [anonymous stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        startGrpID = [startGrpID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        endGrpID = [endGrpID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        timeString = [timeString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        
        NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%@/%@",userID,message,anonymous,startGrpID,endGrpID,theDate,timeString];
       
        
         NSString *encodedUrlString=[NSString stringWithFormat:@"%@/%@",REQUEST_RIDE_URL,urlString];
        
        NSLog(@"URL:%@",encodedUrlString);
        
        
        NSURL *url=[NSURL URLWithString:encodedUrlString];
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"RideRequested"];
                 if ([dataString isEqualToString:@"2"])
                 {
                     
                     [self.view hideToastActivity];
                     
                     
                     [self.view makeToast:@"You already have multiple rides for selected route/day! Contact info@groupool.in for help." duration:2 position:CSToastPositionBottom style:style];
                     
                 }
                 else
                 {
                     [self.view hideToastActivity];
                     [self dismissViewControllerAnimated:YES completion:nil];
                 }
                 
                 
             }
             else
             {
                 [self.view hideToastActivity];
                 
                 
                 [self.view makeToast:@"Could not connect to server. Please try again" duration:2 position:CSToastPositionBottom style:style];
                 
             }
         }];
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@", exception);
    }
    
}

#pragma mark - Cancel Action
-(IBAction) cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - Time Formatter Implementation
- (NSString *)timeFormatter:(NSString *)timeString {
    NSScanner *timescanner = [NSScanner scannerWithString:timeString];
    int year,month,day,hour, minutes;
    [timescanner scanInt:&year];
    [timescanner scanString:@"-" intoString:nil];
    [timescanner scanInt:&month];
    [timescanner scanString:@"-" intoString:nil];
    [timescanner scanInt:&day];
    [timescanner scanString:@" " intoString:nil];
    [timescanner scanInt:&hour];
    [timescanner scanString:@":" intoString:nil];
    [timescanner scanInt:&minutes];
    
    timeString = [NSString stringWithFormat:@"%02d:%02d",hour,minutes];
    return timeString;
}

#pragma mark - Route Selection
- (IBAction)selectClicked:(id)sender {
    
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    NSString *tmpObject,*tmpObject1;
    reverseRouteArray = [[NSMutableArray alloc] init];
    reverseRouteEndGroupIDArray = [[NSMutableArray alloc] init];
    reverseRouteStartGroupIDArray = [[NSMutableArray alloc] init];
    
    for (int i=0;i<[fromGroupsName count];i++)
    {
        // From - To group name
        tmpObject = [NSString stringWithFormat:@"%@ > %@",
                             [fromGroupsName objectAtIndex:i],
                             [toGroupsName objectAtIndex:i]];
        
        tmpObject1 = [NSString stringWithFormat:@"%@ > %@",
                     [toGroupsName objectAtIndex:i],
                     [fromGroupsName objectAtIndex:i]];
        
        [newArray addObject:tmpObject];
        [newArray addObject:tmpObject1];
        [reverseRouteArray addObject:tmpObject];
        [reverseRouteArray addObject:tmpObject1];
        tmpObject = nil;
        tmpObject1 = nil;
        
        // Reverse start group ID
        
        tmpObject = [fromGroupIDArray objectAtIndex:i];
        tmpObject1 = [toGroupIDArray objectAtIndex:i];
        [reverseRouteStartGroupIDArray addObject:tmpObject];
        [reverseRouteStartGroupIDArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        // Reverse end group ID
        
        tmpObject = [toGroupIDArray objectAtIndex:i];
        tmpObject1 = [fromGroupIDArray objectAtIndex:i];
        [reverseRouteEndGroupIDArray addObject:tmpObject];
        [reverseRouteEndGroupIDArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
    }
    
    if (!([userSelectedRoute isEqualToString:@""]) && (userSelectedRoute != nil)) {
        
        [self.btnSelect setTitle:userSelectedRoute forState:UIControlStateNormal];
        
    }
    
    NSArray *tmpArray = [newArray mutableCopy];
   
    [ActionSheetStringPicker showPickerWithTitle:@"Select Route" rows:tmpArray initialSelection:self.selectedIndex target:self successAction:@selector(groupSelected:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
    
    
    
}
- (void)groupSelected:(NSNumber *)selectedIndex element:(id)element {
    self.selectedIndex = [selectedIndex intValue];
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    btnSelect.titleLabel.text = (reverseRouteArray)[(NSUInteger) self.selectedIndex];
    
    userSelectedRoute = (reverseRouteArray)[(NSUInteger) self.selectedIndex];
    
    startGrpID = (reverseRouteStartGroupIDArray)[(NSUInteger) self.selectedIndex];
    endGrpID = (reverseRouteEndGroupIDArray)[(NSUInteger) self.selectedIndex];
    
    
    //    [self.selectTime setTitle:[self timeFormatter:timeString]forState:UIControlStateNormal];
    
}
- (void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Time Selection
- (IBAction)setTimeAction:(id)sender {
    
    self.selectedTime = [[NSDate new] dateByAddingTimeInterval:2700];
    
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
    
    
    ActionSheetDatePicker *aDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Time" datePickerMode:UIDatePickerModeTime  selectedDate:self.selectedTime target:self action:@selector(timeWasSelected:element:) origin:sender cancelAction:@selector(cancelTimeSelection:)];
    aDatePicker.minuteInterval = minuteInterval;
    
    [aDatePicker showActionSheetPicker];
    [aDatePicker setLocale:[NSLocale systemLocale]];
    
}
- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {

    self.selectedTime = selectedTime;
    NSString *createdTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    createdTime = [dateFormatter stringFromDate:selectedTime];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    
    
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    if(formStTime < nowTime && (_daySelector.selectedSegmentIndex == 0)) {
       
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionBottom style:style];
        
    }
    else
    {
        [self.selectTime setTitle:createdTime forState:UIControlStateNormal];
      
    }
    
    
    
    
}
- (void)cancelTimeSelection:(id)sender {
    NSLog(@"Time selection canceled..");
}


- (IBAction)daySelectionAction:(UISegmentedControl *)sender {
    
    
   
    _postView.hidden = NO;
}

#pragma mark - Select Anonymous Post Action
- (IBAction)postSelectionAction:(id)sender {
    
    
    if(toggleIsON){
        
        anonymous = @"0";
        
    }
    else {
        anonymous = @"1";
        
    }
    toggleIsON = !toggleIsON;
    [_btnPostType setImage:[UIImage imageNamed:toggleIsON ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
}

@end
