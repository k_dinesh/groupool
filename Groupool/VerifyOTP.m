//
//  VerifyOTP.m
//
//
//  Created by Parth Pandya on 1/20/16.
//
//

#import "VerifyOTP.h"
#import "AddRouteVC.h"
#import "UIColor+GroupoolColors.h"
#import "CustomPagerViewController.h"
#import "UIView+Toast.h"


@interface VerifyOTP ()
{
    NSString *userID;
    
    CALayer *txtOTPborder;
    CGFloat txtOTPborderWidth ;

}
@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
CGFloat animatedDistance;
@implementation VerifyOTP

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    self.title = @"Verify OTP";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    _txtOTP.delegate = self;
    
    txtOTPborder = [CALayer layer];
    txtOTPborderWidth = 1.5;
    txtOTPborder.borderColor = [UIColor lightGrayColor].CGColor;
    txtOTPborder.frame = CGRectMake(0, _txtOTP.frame.size.height - txtOTPborderWidth, _txtOTP.frame.size.width, _txtOTP.frame.size.height);
    txtOTPborder.borderWidth = txtOTPborderWidth;
    [_txtOTP.layer addSublayer:txtOTPborder];
    _txtOTP.layer.masksToBounds = YES;
    
    _verifyOTPButton.layer.cornerRadius = 5;
    [self startCountdown];
    
    skipOTPVerification.hidden = YES;
    
   
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];

        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isUserNew"] isEqualToString:@"Yes"]) {
            return;
            
        }
        else
        {
    [self sendOTPToMobile];
        }
    
    
}

#pragma mark - Textfield Delegate Implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == _txtOTP)
    {
        txtOTPborder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_txtOTP resignFirstResponder];
    if([_txtOTP resignFirstResponder])
    {
        txtOTPborder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}


#pragma mark - Verify OTP Implementation
- (IBAction)verifyButtonAction:(id)sender {
    
    if ([self.txtOTP.text isEqualToString:@""]) {
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        [self.view makeToast:@"Please enter OTP" duration:1 position:CSToastPositionBottom style:style];
        
    }
    else{
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",SIGN_UP_CHECK_OTP_URL,userID,self.txtOTP.text]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
                 
             {
                 [self.view hideToastActivity];
                 
                 
                     if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"otp_validation"] isEqualToString:@"profile"])
                         
                 {
                     
                     [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"MobileVerified"];
                     
                     NSLog(@"IS MOBILE VERIFIED:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileVerified"]);
                     
                     
                     [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"isUserNew"];
                     
                     if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"newRootNotAdded"] isEqualToString:@"No"])
                     {
                         AddRouteVC *addRoute =[self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
                         UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
                         [self presentViewController:navRoute animated:YES completion:nil];
                         
                     }
                     
                     CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
                     
                     UINavigationController *navMain = [[UINavigationController alloc] initWithRootViewController:homeView];
                     [self presentViewController:navMain animated:YES completion:nil];
                     
                     
                 }
                 else{
                     
                     
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"addNewRoot"];
                     [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"MobileVerified"];
                     [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"newRootNotAdded"];
                     AddRouteVC *addRoute = [self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
                     addRoute.isUserNew = YES;
                     UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
                     [self.navigationController presentViewController:navRoute animated:YES completion:nil];
                 }
                 
                 
             }
             else
             {
                 _txtOTP.text = @"";
                 CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];;
                 style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                 [self.view makeToast:@"Wrong OTP" duration:1 position:CSToastPositionBottom style:style];
                 
             }
         }];
    }
    
}

#pragma mark - Skip Verification Process
- (void)startCountdown {
    _counter = 45;
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(countdownTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
}
- (void)countdownTimer:(NSTimer *)timer {
    _counter--;
    _counterLabel.textColor = [UIColor groupoolMainColor];
        _counterLabel.text = [NSString stringWithFormat:@"%lu",(long)_counter];
    if (_counter <= 0) {
        [timer invalidate];
        _counterLabel.textColor = [UIColor lightGrayColor];
        _counterLabel.lineBreakMode = NSLineBreakByWordWrapping;

       _counterLabel.text = @"Haven't received OTP message yet? You can skip for now and verify it inside the app.";
        skipOTPVerification.hidden = NO;
    }
}
- (IBAction)skipOTPVerification:(id)sender {
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isUserNew"] isEqualToString:@"Yes"])
    {
         [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"isUserNew"];
        AddRouteVC *addRoute = [self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
        addRoute.isUserNew = YES;
        UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
        [self.navigationController presentViewController:navRoute animated:YES completion:nil];
    }
    else
    {
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"newRootNotAdded"] isEqualToString:@"No"])
        {
            AddRouteVC *addRoute =[self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
            UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
            [self presentViewController:navRoute animated:YES completion:nil];
            
        }
        
        CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
        
        UINavigationController *navMain = [[UINavigationController alloc] initWithRootViewController:homeView];
        [self presentViewController:navMain animated:YES completion:nil];
    }
}

-(void)sendOTPToMobile {
    
    
    //    http://groupool.in/service/index.php?/UploadDownload_groupool/verifyOTP/user_id/type/mobile
    // user_id=247, type=C, mobile=(by default '0')
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@",SIGN_UP_VERIFY_OTP,userID,@"C",@"0"]];
    
    
 
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         
         if (data.length > 0 && connectionError == nil)
         {
             
         }
         else
         {
             NSLog(@"Connection could not be made");
             
         }
     }];
    
}
@end
