//
//  RequestRideView.m
//  Groupool
//
//  Created by HN on 05/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "RequestRideView.h"

@implementation RequestRideView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self setup];
    }
    
    //    if ((self = [super initWithCoder:aDecoder])){
    //        [self addSubview:
    //         [[[NSBundle mainBundle] loadNibNamed:@"RequestRideView"
    //                                        owner:self
    //                                      options:nil] objectAtIndex:0]];
    //    }
    return self;
}

-(void)setup{
    [[NSBundle mainBundle] loadNibNamed:@"RequestRideView" owner:self options:nil];
    [self addSubview:self.view];
}


- (IBAction)cancelAction:(id)sender {
}

- (IBAction)postAction:(id)sender {
}
@end
