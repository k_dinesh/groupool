//
//  RequestRideVC.h
//  Groupool
//
//  Created by HN on 05/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RequestRideVC : UIViewController
{

   
    BOOL toggleIsON;
    BOOL isDaySelected;
    
    // Reverse route objects
    NSMutableArray *reverseRouteArray;
    NSMutableArray *reverseRouteStartGroupIDArray;
    NSMutableArray *reverseRouteEndGroupIDArray;
    
    NSString *userSelectedRoute;
        
}

/******** Group Selection ********/
@property (retain, nonatomic) IBOutlet UIButton *btnSelect;
- (IBAction)selectClicked:(id)sender;
/******** Group Selection ********/

// Integer for string selecter array index
@property (nonatomic, assign) NSInteger selectedIndex;
// Date for timeselector
@property (nonatomic, strong) NSDate *selectedTime;


/******** Time Selection ********/
- (IBAction)setTimeAction:(id)sender;
@property (retain,nonatomic) IBOutlet UIButton *selectTime;
/******** Time Selection ********/

/******** Post Type ********/
@property (strong, nonatomic) IBOutlet UIView *postView;
@property (strong, nonatomic) IBOutlet UIButton *btnPostType;
- (IBAction)postSelectionAction:(id)sender;
/******** Post Type ********/

/******** Day Select ********/
@property (strong, nonatomic) IBOutlet UISegmentedControl *daySelector;
- (IBAction)daySelectionAction:(id)sender;
/******** Day Select ********/

@property (weak, nonatomic) IBOutlet UIView *btnPost;
@property (weak, nonatomic) IBOutlet UIView *btnCancel;
- (IBAction)postAction:(id)sender;
- (IBAction)cancelAction:(id)sender;




@end
