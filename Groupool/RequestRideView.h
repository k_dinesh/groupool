//
//  RequestRideView.h
//  Groupool
//
//  Created by HN on 05/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestRideView : UIView
- (IBAction)cancelAction:(id)sender;
- (IBAction)postAction:(id)sender;
@property (nonatomic,strong) IBOutlet UIView *view;
@end
