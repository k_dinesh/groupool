//
//  VerifyOTP.h
//  
//
//  Created by Parth Pandya on 1/20/16.
//
//

#import <UIKit/UIKit.h>

@interface VerifyOTP : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UIButton *skipOTPVerification;
}


@property (strong, nonatomic) IBOutlet UITextField *txtOTP;

@property (weak, nonatomic) IBOutlet UIButton *verifyOTPButton;
- (IBAction)verifyButtonAction:(id)sender;
@property (nonatomic) NSInteger counter;

@property (strong, nonatomic) IBOutlet UILabel *counterLabel;

- (IBAction)skipOTPVerification:(id)sender;


@end
