//
//  MyRidesDataModel.h
//  Groupool
//
//  Created by Parth Pandya on 05/04/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyRidesDataModel : NSObject

@property (nonatomic,strong) NSString *mySort;
@property (nonatomic,strong) NSString *offeredRideID;
@property (nonatomic,strong) NSString *days;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *startGroupID;
@property (nonatomic,strong) NSString *endGroupID;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *joinedRideID;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *fromGroupLocation;
@property (nonatomic,strong) NSString *fromGroupName;
@property (nonatomic,strong) NSString *toGroupLocation;
@property (nonatomic,strong) NSString *toGroupName;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *profilePic;
@property (nonatomic,strong) NSString *smsVerified;
@property (nonatomic,strong) NSString *emailVerified;
@property (nonatomic,strong) NSString *officeEmailVerified;
@property (nonatomic,strong) NSString *facebookID;
@property (nonatomic,strong) NSString *fbFriends;
@property (nonatomic,strong) NSString *company;


@end
