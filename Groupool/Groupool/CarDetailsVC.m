//
//  CarDetailsVC.m
//  Groupool
//
//  Created by Parth Pandya on 14/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CarDetailsVC.h"
#import "UIView+Toast.h"
#import "CustomPagerViewController.h"
#import "ProfileVC.h"


@interface CarDetailsVC ()

@end

@implementation CarDetailsVC
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Add Car Details";
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    txtCarDetails.delegate = self;
    txtCarNumber.delegate = self;
    
    self.popUpView.layer.cornerRadius = 3;
    
    [btnHatchBack setImage:[UIImage imageNamed:@"car_two_selected.png"] forState:UIControlStateNormal];
    aCarType = @"HB";

    tcdBorder = [CALayer layer];
    tcdBorderWidth = 1.5;
    tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcdBorder.frame = CGRectMake(0, txtCarDetails.frame.size.height - tcdBorderWidth,txtCarDetails.frame.size.width,txtCarDetails.frame.size.width);
    tcdBorder.borderWidth = tcdBorderWidth;
    [txtCarDetails.layer addSublayer:tcdBorder];
    txtCarDetails.layer.masksToBounds = YES;
    
    tcnBorder = [CALayer layer];
    tcnBorderWidth = 1.5;
    tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcnBorder.frame = CGRectMake(0, txtCarNumber.frame.size.height - tcnBorderWidth,txtCarNumber.frame.size.width,txtCarNumber.frame.size.width);
    tcnBorder.borderWidth = tcnBorderWidth;
    [txtCarNumber.layer addSublayer:tcnBorder];
    txtCarNumber.layer.masksToBounds = YES;
    
}


- (IBAction)saveAndOffer:(id)sender {
    
    [self.view endEditing:YES];
   
    if (([aCarType isEqualToString:@""]) || ([txtCarDetails.text isEqualToString:@""])|| ([txtCarNumber.text isEqualToString:@""]))
    {
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Please enter details and cartype" duration:2 position:CSToastPositionBottom style:style];
    }
    else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]
             || [[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"])
    {

        NSString *alertMessage;
    
    
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        self.carType = aCarType;
        self.carDetails = txtCarDetails.text;
        self.carNumber = txtCarNumber.text;
        
        [[NSUserDefaults standardUserDefaults] setValue:self.carType forKey:@"CarType"];
        [[NSUserDefaults standardUserDefaults] setValue:self.carDetails forKey:@"CarDetails"];
        [[NSUserDefaults standardUserDefaults] setValue:self.carNumber forKey:@"CarNumber"];
        
        
        @try {
            
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:self.userID forKey:@"created_by"];
            [dataDict setValue:self.startGrpID forKey:@"start_group_id"];
            [dataDict setValue:self.womenOnly forKey:@"women_only"];
            [dataDict setValue:self.isActice forKey:@"is_active"];
            [dataDict setValue:self.carNumber forKey:@"car_number"];
            [dataDict setValue:self.editedID forKey:@"edited_id"];
            [dataDict setValue:self.carType forKey:@"car_type"];
            [dataDict setValue:self.offeredSeat forKey:@"offered_seats"];
            [dataDict setValue:self.rideID forKey:@"id"];
            [dataDict setValue:self.time forKey:@"time"];
            [dataDict setValue:self.price forKey:@"price"];
            [dataDict setValue:self.carDetails forKey:@"car_details"];
            [dataDict setValue:self.dayString forKey:@"days"];
            [dataDict setValue:self.endGrpID forKey:@"end_group_id"];
            [dataDict setValue:self.theDate forKey:@"tdate"];
            [dataDict setValue:self.userID forKey:@"user_id"];
            [dataDict setValue:self.comment forKey:@"comment"];
            [dataDict setValue:self.phpID forKey:@"phpid"];
            [dataDict setValue:self.OS forKey:@"offered_via"];
            
            
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            
            NSString *postStr;
            if (! jsonData) {
                NSLog(@"Got an error: %@", error);
                [self.view hideToastActivity];
            } else {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
               
                postStr = [NSString stringWithFormat:@"[%@]",jsonString];
               
            }
            
            NSError *err = nil;
            NSURLResponse *response;
            NSData *localData = nil;
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:OFFER_RIDE_URL]];
            
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[postStr dataUsingEncoding:NSUTF8StringEncoding]];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response,
                                                       NSData *data, NSError *connectionError)
             {
                 if (data.length > 0 && connectionError == nil)
                     
                 {
                     
                     [self.view hideToastActivity];
                     
                     CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"] ;
                     UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
                     
                     [self.navigationController presentViewController:navHome animated:YES completion:nil];
                     
                 }
                 else
                 {
                     [self.view hideToastActivity];
                     
                     CSToastStyle *style;
                     style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                     [self.view makeToast:@"Could not connect to server. Please try again" duration:1 position:CSToastPositionBottom style:style];
                    
                 }
                 
                 
             }];
            
        }
        @catch (NSException *exception) {
            [self.view hideToastActivity];
            NSLog(@"%@", exception);
        }
        
    }
    
    
}



- (IBAction)carTypeSelection:(UIButton *)sender {
    
    switch (sender.tag) {
        case 0:
            aCarType = @"HB";
            [btnHatchBack setImage:[UIImage imageNamed:@"car_two_selected.png"] forState:UIControlStateNormal];
            [btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
            break;
        case 1:
            aCarType = @"SE";
            [btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [btnSedan setImage:[UIImage imageNamed:@"car_one_selected.png"] forState:UIControlStateNormal];
            [btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
            break;
        case 2:
            aCarType = @"SU";
            [btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [btnSUV setImage:[UIImage imageNamed:@"car_three_selected.png"] forState:UIControlStateNormal];
            [btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
            break;
        case 3:
            aCarType = @"OT";
            [btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [btnOther setImage:[UIImage imageNamed:@"car_four_selected.png"] forState:UIControlStateNormal];
            break;
            
        default:
            
            break;
    }
    
    
}

#pragma mark - Textfield Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([txtCarDetails resignFirstResponder]) {
        tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
    if ([txtCarNumber resignFirstResponder]) {
        tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == txtCarDetails)
    {
        tcdBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
    if (textField == txtCarNumber) {
        tcnBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
    if (textField == txtCarDetails)
    {
        tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == txtCarNumber) {
        tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return NO;
}

- (void) backButtonAction {
    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
    [self.navigationController presentViewController:navHome animated:YES completion:nil];
    
}

@end
