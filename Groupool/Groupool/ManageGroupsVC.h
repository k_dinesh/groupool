//
//  ManageGroupsVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AddRouteVC.h"

@protocol ManageRouteDelegate;

@interface ManageGroupsVC : UIViewController<UITableViewDataSource,UITableViewDelegate,AddRouteDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
- (IBAction)addNewRouteAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addNewRouteButton;



- (IBAction)backToLanding:(id)sender;
@property (assign,nonatomic) id<ManageRouteDelegate>mDelegate;
@property (strong) UIView *customView;

@property (strong,nonatomic) NSMutableArray *rdNameArray;
@property (strong,nonatomic) NSMutableArray *rdLocationArray;
@property (strong,nonatomic) NSMutableArray *rdMemberCountArray;
@property (strong,nonatomic) NSMutableArray *rdRideCountArray;

@property (strong,nonatomic) NSMutableArray *fromGroupsName;
@property (strong,nonatomic) NSMutableArray *toGroupsName;
@property (strong,nonatomic) NSMutableArray *routeIDArray;
@property (strong,nonatomic) NSMutableArray *homeToOfficeArray;
@property (strong,nonatomic) NSMutableArray *officeToHomeArray;
@property (strong,nonatomic) NSMutableArray *repeatRouteIDArray;

@end

@protocol ManageRouteDelegate <NSObject>

- (void) refreshDropdown:(ManageGroupsVC *)refreshData;

@end