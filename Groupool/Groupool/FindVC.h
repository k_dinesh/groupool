//
//  FindVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestRideVC.h"
#import "OfferVC.h"
#import "CarDetailsVC.h"
#import "FindDataModel.h"
#import "FindTableViewCellVC.h"


@interface FindVC : UIViewController<UITableViewDelegate,UITableViewDataSource,OfferRideDelegate,carDetailDelegate>
{
    NSMutableArray *groupesName;
    NSMutableArray *placesArray;
    // Array List of Routes Dropdown list.
    NSMutableArray *fromGroupsNameArray;
    NSMutableArray *toGroupsNameArray;
    NSMutableArray *routeIDArray;
    NSMutableArray *fromGroupIDArray;
    NSMutableArray *toGroupIDArray;
    
    //    Array for List of Offered Ride

    NSMutableArray *DaysArray;
    NSArray *rideDayArray;
    
    NSString *userID;
    NSString *rideDay;
    
    
    NSString *selectedGroup;
    BOOL isCalled;
    
    // Reverse Route objects
    NSMutableArray *reverseRouteArray;
    NSMutableArray *reverseRouteFromGroupID;
    NSMutableArray *reverseRouteToGroupID;
    
}

// Ride Request Button
@property (strong, nonatomic) IBOutlet UIButton *requestRide;
//Group Selection Button
@property (strong, nonatomic) IBOutlet UIButton *selectGroup;
// Day Selection Button
@property (strong, nonatomic) IBOutlet UIButton *selectDay;

@property (nonatomic, strong) NSDate *selectedTime;

@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, assign) NSInteger selectedIndexForDay;

@property (nonatomic,strong) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UILabel *NoRideLabel;
@property (weak, nonatomic) IBOutlet UILabel *noRideLabel1;
@property (strong, nonatomic) IBOutlet UILabel *lblRouteSelection;
@property (strong, nonatomic) IBOutlet UILabel *lblDaySelection;


@property (nonatomic,strong) NSMutableArray *OfferedRideData;

@property (weak, nonatomic) IBOutlet UIButton *InviteFriendsBtn;

// Ride Details Popup Controls
@property (strong) UIView *customView;

@property (strong,nonatomic) NSString *userSelectedGroup;
@property (strong,nonatomic) NSString *userSelectedTime;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSString *rideDate;
@property (strong, nonatomic) NSString *startGroupID;
@property (strong, nonatomic) NSString *endGroupID;

- (IBAction)InviteFriendsAction:(id)sender;
- (IBAction)selectGroupPressed:(id)sender;
- (IBAction)selectDayPressed:(id)sender;

@end
