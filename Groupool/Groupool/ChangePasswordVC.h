//
//  ChangePasswordVC.h
//  Groupool
//
//  Created by HN on 21/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmNewPassword;



@end
