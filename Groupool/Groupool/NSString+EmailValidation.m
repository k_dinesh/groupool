//
//  NSString+EmailValidation.m
//  Groupool
//
//  Created by Parth Pandya on 09/04/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "NSString+EmailValidation.h"

@implementation NSString (EmailValidation)

-(BOOL)isValidEmail {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @".+@.+\\.[A-Za-z-]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

@end
