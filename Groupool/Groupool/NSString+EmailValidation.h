//
//  NSString+EmailValidation.h
//  Groupool
//
//  Created by Parth Pandya on 09/04/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EmailValidation)
-(BOOL)isValidEmail;

@end
