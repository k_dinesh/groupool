//
//  LoginVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPagerViewController.h"
#import "AppStartVC.h"
#import "UserDetails.h"

@interface LoginVC : UIViewController<UITextFieldDelegate>

@property (strong,nonatomic) UserDetails *user;

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
- (IBAction)loginButtonAction:(id)sender;


- (IBAction)forgotPasswordBtnAction:(id)sender;





@end
