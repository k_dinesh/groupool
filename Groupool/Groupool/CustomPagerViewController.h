//
//  CustomPageViewController.h
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import "PagerViewController.h"



@interface CustomPagerViewController : PagerViewController 

@property (strong,nonatomic) UIView *customView;
@property (strong,nonatomic) UIButton *infoButton;

@property (strong,nonatomic) UIViewController *imageVC;
@property (strong,nonatomic) UIScrollView *imageScroller;
@property (strong,nonatomic) UIPageControl *imagePager;

@end
