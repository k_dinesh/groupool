//
//  MyRideTableViewCellVC.h
//  Groupool
//
//  Created by HN on 04/04/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRideTableViewCellVC : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *rideDayAndDate;
@property (strong, nonatomic) IBOutlet UILabel *rideGroupName;
@property (strong, nonatomic) IBOutlet UILabel *rideUserName;
@property (strong, nonatomic) IBOutlet UILabel *ridePrice;

@property (weak, nonatomic) IBOutlet UIButton *btnEditTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteRide;










@end
