//
//  LeftMenuVC.m
//  Groupool
//
//  Created by HN on 01/12/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "LeftMenuVC.h"

@interface LeftMenuVC ()
{
    
}
@property (strong, nonatomic) UITableView *myTableView;
@end

@implementation LeftMenuVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && ![UIApplication sharedApplication].isStatusBarHidden)
    {
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    }
    
//    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
//                                 CGRectMake(0, 0, self.tableView.frame.size.width, 100.0)];
//    
//    sectionHeaderView.backgroundColor = [UIColor blueColor];
//    

}



- (void)setFixedStatusBar
{
    self.myTableView = self.tableView;
    
    self.view = [[UIView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = self.myTableView.backgroundColor;
    [self.view addSubview:self.myTableView];
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(self.view.frame.size.width,self.view.frame.size.height), 20)];
    statusBarView.backgroundColor = [UIColor colorWithRed:0 green:191 blue:199 alpha:0.1];
    [self.view addSubview:statusBarView];
}


@end
