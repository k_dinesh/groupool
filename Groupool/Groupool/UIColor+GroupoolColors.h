//
//  UIColor+GroupoolColors.h
//  Groupool
//
//  Created by HN on 16/12/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GroupoolColors)
+ (UIColor *)groupoolMainColor;
+ (UIColor *)buttonBackgroundColor;
+ (UIColor *)darkGroupoolColor;
+ (UIColor *)grayTextColor;
+ (UIColor *)systemMessageColor;
+ (UIColor *)feedTableCellColor;
+ (UIColor *)tableBackgroundColor;
+ (UIColor *)buttonTextColor;
@end
