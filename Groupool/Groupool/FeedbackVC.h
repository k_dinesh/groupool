//
//  FeedbackVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackVC : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *txtFeedback;
@property (weak, nonatomic) IBOutlet UIButton *btnSendFeedback;


- (IBAction)sendFeedbackAction:(id)sender;

@end
