//
//  OfferDataModel.h
//  Groupool
//
//  Created by HN on 31/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferDataModel : NSObject
{
    NSString *fromGroupName;
    NSString *toGroupName;
    NSString *fromGroupID;
    NSString *toGroupID;
    NSString *defaultRidePrice;
    NSString *startRideTime;
    NSString *routeID;
    NSString *maxRidePrice;
    NSString *userRidePrice;
    NSString *rideTime;
    
}

@property (nonatomic,copy) NSString *fromGroupName;
@property (nonatomic,copy) NSString *toGroupName;
@property (nonatomic,copy) NSString *fromGroupID;
@property (nonatomic,copy) NSString *toGroupID;
@property (nonatomic,copy) NSString *defaultRidePrice;
@property (nonatomic,copy) NSString *startRideTime;
@property (nonatomic,copy) NSString *routeID;
@property (nonatomic,copy) NSString *maxRidePrice;
@property (nonatomic,copy) NSString *userRidePrice;
@property (nonatomic,copy) NSString *rideTime;





+ (OfferDataModel *) getInstance;

@end
