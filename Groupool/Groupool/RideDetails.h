//
//  RideDetails.h
//  Groupool
//
//  Created by HN on 16/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RideDetails : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *startGrpID;
@property (nonatomic,strong) NSString *womenOnly;
@property (nonatomic,strong) NSString *isActice;
@property (nonatomic,strong) NSString *carNumber;
@property (nonatomic,strong) NSString *editedID;
@property (nonatomic,strong) NSString *carType;
@property (nonatomic,strong) NSString *offeredSeat;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *carDetails;
@property (nonatomic,strong) NSString *endGrpID;
@property (nonatomic,strong) NSString *theDate;
@property (nonatomic,strong) NSString *comment;
@property (nonatomic,strong) NSString *phpID;
@property (nonatomic,strong) NSString *OS;
@property (nonatomic,strong) NSString *rideID;
@property (nonatomic,strong) NSString *dayString;

@end
