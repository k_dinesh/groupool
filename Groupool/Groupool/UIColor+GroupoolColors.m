//
//  UIColor+GroupoolColors.m
//  Groupool
//
//  Created by HN on 16/12/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "UIColor+GroupoolColors.h"

@implementation UIColor (GroupoolColors)

+ (UIColor *)groupoolMainColor
{
    return [UIColor colorWithRed:33.0f/255.0f green:150.0f/255.0f blue:243.0f/255.0f alpha:1] ;
}

+ (UIColor *)buttonBackgroundColor
{
    return [UIColor colorWithRed:75.0f/255.0f green:75.0f/255.0f blue:75.0f/255.0f alpha:1] ;
}

+ (UIColor *)darkGroupoolColor {
return [UIColor colorWithRed:0.0f/255.0f green:100.0f/255.0f blue:100.0f/255.0f alpha:1] ;
}

+ (UIColor *)grayTextColor{
    
//    return [UIColor colorWithRed:111.0f/255.0f green:113.0f/255.0f blue:121.0f/255.0f alpha:1] ;
return [UIColor colorWithRed:75.0f/255.0f green:75.0f/255.0f blue:75.0f/255.0f alpha:1] ;
    
}

+ (UIColor *)systemMessageColor {
    return [UIColor colorWithRed:74.0f/255.0f green:129.0f/255.0f blue:164.0f/255.0f alpha:1] ;
  
    
}
+ (UIColor *)feedTableCellColor {
    
    return [UIColor colorWithRed:212.0f/255.0f green:237.0f/255.0f blue:243.0f/255.0f alpha:1] ;
    
    
}

+ (UIColor *)tableBackgroundColor {
    
    return [UIColor colorWithRed:234.0f/255.0f green:234.0f/255.0f blue:234.0f/255.0f alpha:1] ;
    
}

+ (UIColor *)buttonTextColor {
    
    return [UIColor colorWithRed:30.00/255.00 green:123.00/255.00 blue:230.00/255.00 alpha:1] ;
    
}




@end
