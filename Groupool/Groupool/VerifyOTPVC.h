//
//  VerifyOTPVC.h
//  Groupool
//
//  Created by HN on 20/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyOTPVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitAction:(id)sender;

@end
