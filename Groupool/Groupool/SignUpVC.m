//
//  SignUpVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "SignUpVC.h"
#import "CustomPagerViewController.h"
#import "AppStartVC.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+GroupoolColors.h"
#import "AddRouteVC.h"
#import "VerifyOTP.h"
#import "UIImage+fixOrientation.h"
#import "UIView+Toast.h"
#import "NSString+EmailValidation.h"




@interface SignUpVC ()<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSString *gender;
    NSString *fullName;
    NSString *mobileNumber;
    NSString *personalEmail;
    NSString *password;
    NSString *officeEmail;
    NSString *userID;
    NSString *loginType;
    NSString *faceBookID;
    NSString *fbFriends;
    NSString *imageFileData;
    NSString *appVersion;
    NSString *country;
    NSString *gcmRegID;
    NSString *OS;
    
    NSMutableArray *tmpArr;
    UIActivityIndicatorView *indicator;
    
    CALayer *tfnBorder;
    CGFloat tfnBorderWidth ;
    CALayer *tmnBorder ;
    CGFloat tmnBorderWidth ;
    CALayer *tpeBorder;
    CGFloat tpeBorderWidth;
    CALayer *tpBorder;
    CGFloat tpBorderWidth;
    CALayer *toeBorder;
    CGFloat toeBorderWidth;
}

@end

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
CGFloat animatedDistance;
@implementation SignUpVC
@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    self.title = @"Sign Up";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.textField.delegate = self;
    self.txtFullName.delegate = self;
    self.txtMobileNumber.delegate = self;
    self.txtOfficeEmail.delegate = self;
    self.txtPassword.delegate = self;
    self.txtPersonalEmail.delegate = self;
    
    user = [[UserDetails alloc] init];
   
    
    self.userImage.layer.cornerRadius = 26;
    self.userImage.layer.masksToBounds = YES;
    
    _btnSignUp.layer.cornerRadius = 5;
    
    /* Gender Selection */
    [self.optGender addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [self.userImage addGestureRecognizer:tapSelectedImage];
    [self.userImage setUserInteractionEnabled:YES];
    
    self.txtMobileNumber.keyboardType = UIKeyboardTypeNumberPad;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationItem.leftBarButtonItem.title = @"";
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    
    // Full Name TextField Customization
    
    tfnBorder = [CALayer layer];
    tfnBorderWidth = 1.5;
    tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tfnBorder.frame = CGRectMake(0, _txtFullName.frame.size.height - tfnBorderWidth, _txtFullName.frame.size.width, _txtFullName.frame.size.height);
    tfnBorder.borderWidth = tfnBorderWidth;
    [_txtFullName.layer addSublayer:tfnBorder];
    _txtFullName.layer.masksToBounds = YES;
    
    // Mobile Number TextField Customization
    
    tmnBorder = [CALayer layer];
    tmnBorderWidth = 1.5;
    tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tmnBorder.frame = CGRectMake(0, _txtMobileNumber.frame.size.height - tmnBorderWidth, _txtMobileNumber.frame.size.width, _txtMobileNumber.frame.size.height);
    tmnBorder.borderWidth = tmnBorderWidth;
    [_txtMobileNumber.layer addSublayer:tmnBorder];
    _txtMobileNumber.layer.masksToBounds =  YES;
    
    // Personal Email TextField Customization
    
    tpeBorder = [CALayer layer];
    tpeBorderWidth = 1.5;
    tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tpeBorder.frame = CGRectMake(0, _txtPersonalEmail.frame.size.height - tpeBorderWidth, _txtPersonalEmail.frame.size.width, _txtPersonalEmail.frame.size.height);
    tpeBorder.borderWidth = tpeBorderWidth;
    [_txtPersonalEmail.layer addSublayer:tpeBorder];
    _txtPersonalEmail.layer.masksToBounds =  YES;
    
    // Password TextField Customization
    
    tpBorder = [CALayer layer];
    tpBorderWidth = 1.5;
    tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tpBorder.frame = CGRectMake(0, _txtPassword.frame.size.height - tpBorderWidth, _txtPassword.frame.size.width, _txtPassword.frame.size.height);
    tpBorder.borderWidth = tpBorderWidth;
    [_txtPassword.layer addSublayer:tpBorder];
    _txtPassword.layer.masksToBounds =  YES;
    
    // Office Email TextField Customization
    
    toeBorder = [CALayer layer];
    toeBorderWidth = 1.5;
    toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    toeBorder.frame = CGRectMake(0, _txtOfficeEmail.frame.size.height - toeBorderWidth, _txtOfficeEmail.frame.size.width, _txtOfficeEmail.frame.size.height);
    toeBorder.borderWidth = toeBorderWidth;
    [_txtOfficeEmail.layer addSublayer:toeBorder];
    _txtOfficeEmail.layer.masksToBounds =  YES;
    
    
    // Busy Indicator
    
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    indicator.frame = CGRectMake(0, 0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
}
- (void) backButtonAction {
    
    
    for (UIView *view in [self.view subviews])
    {
        if ([view isKindOfClass:[UITextField class]])
        {
            UITextField *textField = (UITextField *)view;
            textField.text = @"";
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sure want to return" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Return"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             
                             
                             [self dismissViewControllerAnimated:YES completion:nil];
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [indicator stopAnimating];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - TextField Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([_txtFullName resignFirstResponder])
    {
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtMobileNumber resignFirstResponder])
    {
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if ([_txtPersonalEmail resignFirstResponder])
    {
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if ([_txtPassword resignFirstResponder])
    {
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
        
    }
    if ([_txtOfficeEmail resignFirstResponder])
    {
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor  =  [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == _txtFullName) {
        
        tfnBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtMobileNumber) {
        
        tmnBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtPassword) {
        
        tpBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtPersonalEmail) {
        
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtOfficeEmail) {
        
        tpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tfnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
        toeBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return NO;
    
}


#pragma mark - Image selection Implementation
- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        
        
        CGFloat frameHeight = self.userImage.frame.size.height;
        CGRect imageViewFrame = CGRectInset(self.userImage.bounds, 0.0, (CGRectGetHeight(self.userImage.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:self.userImage]));
        if (userTappedOnimageView)
        {
         
            [self selectPhotos];
            
        }
        
    }
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    
    self.userImage.image = originalImage;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"newUserImage.png"];
    UIImage *image = originalImage; // imageView is my image from camera
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];
   
}
- (void)selectPhotos {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    
    // show device picture library
    picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    
    [self presentViewController:picker animated:YES completion:nil];
    
}


#pragma mark - Sign-Up Implementation
- (IBAction)signuUpAction:(id)sender {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];

    [self.view endEditing:YES];
    
    if ([_txtFullName.text isEqualToString:@""] ||[_txtMobileNumber.text isEqualToString:@""] ||
        [_txtPassword.text isEqualToString:@""] ||
        [_txtPersonalEmail.text isEqualToString:@""] ||
        [_txtOfficeEmail.text isEqualToString:@""])
    {
        [self.view makeToast:@"Please enter all information" duration:2.0 position:CSToastPositionBottom style:style];
        
    }
    else{
        
        NSString *str= @"[789][0-9]{9}";
        NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
        if([no evaluateWithObject:_txtMobileNumber.text]==NO)
        {
            
            [self.view makeToast:@"Please enter correct Mobile No." duration:1.5 position:CSToastPositionBottom style:style];
            return;
            
        }
        
        if (![_txtPersonalEmail.text isValidEmail])
        {
            [self.view makeToast:@"Please enter correct Personal Email ID." duration:1.5 position:CSToastPositionBottom style:style];
            return;
        }
        
        if (![_txtOfficeEmail.text isValidEmail])
        {
            [self.view makeToast:@"Please enter correct Office Email ID." duration:1.5 position:CSToastPositionBottom style:style];
            return;
        }
       
        if (!([_txtMobileNumber.text isEqualToString:@""])) {
            
            NSString *alertMessage = [NSString stringWithFormat:@"OTP will be sent to %@ ",_txtMobileNumber.text];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertMessage message:@"Do you want to continue?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
            {
             [self.view endEditing:YES];
                
             [self signUp];
                
            }];
            
            
            UIAlertAction *changeAction = [UIAlertAction actionWithTitle:@"CHANGE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            }];
            [alert addAction:changeAction];
            [alert addAction:okAction];
            
            [indicator stopAnimating];
            [self presentViewController:alert animated:YES completion:nil];
        }
      
    }
}
- (void) signUp {
    
        
    [self.view makeToastActivity:CSToastPositionCenter];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"newUserImage.png"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    NSData *imageData = UIImagePNGRepresentation(img);
    
    NSString *data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    
        if (data == nil) {
            data = @"";
        }
        if (user.gender == nil) {
            
            if (self.optGender.selectedSegmentIndex == 0) {
             user.gender = @"M";
            }
            else{
                user.gender = @"F";
            }
            
        }
        fullName = _txtFullName.text;
        mobileNumber = _txtMobileNumber.text;
        personalEmail = _txtPersonalEmail.text;
        password = _txtPassword.text;
        //  gender =
        officeEmail  = _txtOfficeEmail.text;
        loginType = @"C";
        faceBookID = @"0";
        fbFriends = @"0";
        appVersion = @"32";
        country = @"IN";
        userID =@"0";
        gcmRegID = [[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"];
    if (gcmRegID == nil) {
        gcmRegID = @"0";
    }
        OS = @"I";
        
        //    NSLog(@"1.Iamge Data:%@",imageFileData);
        NSLog(@"2.ID:%@",userID);
        NSLog(@"3.User Name:%@",fullName);
        NSLog(@"4.Gender:%@",user.gender);
        NSLog(@"5.Mobile Number:%@",mobileNumber);
        NSLog(@"6.Password:%@",password);
        NSLog(@"7.Email:%@",personalEmail);
        NSLog(@"8.Office Email:%@",officeEmail);
        NSLog(@"9.Login Type:%@",loginType);
        NSLog(@"10.Facebook ID:%@",faceBookID);
        NSLog(@"11.App Version:%@",appVersion);
        NSLog(@"12.Fb friends:%@",fbFriends);
        NSLog(@"13.Country:%@",country);
        NSLog(@"14.GCM ID:%@",gcmRegID);
        NSLog(@"15.OS:%@",OS);
        
        NSDictionary *params = @{
                                 @"image_url": data,
                                 @"id":userID,
                                 @"name":fullName,
                                 @"gender":user.gender,
                                 @"mobile":mobileNumber,
                                 @"password":password,
                                 @"email":personalEmail,
                                 @"office_email":officeEmail,
                                 @"login_type":loginType,
                                 @"facebook_id":faceBookID,
                                 @"user_app_version":appVersion,
                                 @"fb_friends":fbFriends,
                                 @"country":country,
                                 @"gcm_reg_id":gcmRegID,
                                 @"os":OS
                                 
                                 };
    
        @try {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            //-- Convert string into URL
            NSString *urlString = [NSString stringWithFormat:UPLOAD_DATA_URL];
            NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"POST"];
            
            NSString *boundary = @"14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            //-- Append data into posr url using following method
            NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params ];
            
            request.HTTPBody = httpBody;
            
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                
                if (data == NULL) {
                    
                    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                    style.messageFont = [UIFont systemFontOfSize:12];
                    style.messageAlignment = NSTextAlignmentCenter;
                    style.cornerRadius = 12;
                    [self.view makeToast:@"Server connection error" duration:3 position:CSToastPositionBottom style:style]
                    ;
                    return ;
                    
                }
                
                if (data.length > 0 && !connectionError) {
                    
//                    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                     NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    
                    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if ([dataString isEqualToString:@"0"]) {
                        
                        [self.view hideToastActivity];
                        
                        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                        style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                        [self.view makeToast:@"User already exists. Use \"Login > Forgot password\" if needed" duration:1 position:CSToastPositionBottom style:style];
                    }
                    else{
                        
                        if (!(jsonArray.count == 0)) {
                            
                            user = [[UserDetails alloc]init];
                            
                            for (NSDictionary *params in jsonArray) {
                                
                                user.name = params[@"name"];
                                [defaults setValue:user.name forKey:@"UserName"];
                                
                                user.userID = params[@"id"];
                                [defaults setValue:user.userID forKey:@"UserID"];
                                
                                user.gender = params[@"gender"];
                                [defaults setValue:user.gender forKey:@"Gender"];
                                
                                user.personalEmail = params[@"email"];
                                [defaults setValue:user.personalEmail forKey:@"Email"];
                                
                                user.mobileNumber = params[@"mobile"];
                                [defaults setValue:user.mobileNumber forKey:@"MobileNumber"];
                                
                                user.password = params[@"password"];
                                [defaults setValue:user.password forKey:@"Password"];
                                
                                user.officeEmail = params[@"office_email"];
                                [defaults setValue:user.officeEmail forKey:@"OfcEmail"];
                                
                                user.carType = params[@"car_type"];
                                [defaults setValue:user.carType forKey:@"CarType"];
                                
                                user.carDetails = params[@"car_details"];
                                [defaults setValue:user.carDetails forKey:@"CarDetails"];
                                
                                user.carNumber = params[@"car_number"];
                                [defaults setValue:user.carNumber forKey:@"CarNumber"];
                                
                                user.OTP = params[@"otp"];
                                [defaults setValue:user.OTP forKey:@"OTP"];
                                
                                user.companyName = params[@"company"];
                                [defaults setValue:user.companyName forKey:@"CompanyName"];
                                
                                user.appVersion = params[@"user_app_version"];
                                [defaults setValue:user.appVersion forKey:@"AppVersion"];
                                
                                user.officeEmailVerified = params[@"office_email_verified"];
                                [defaults setValue:user.officeEmailVerified forKey:@"OfficeEmailVerified"];
                                
                                
                                user.smsVerified = params[@"sms_verified"];
                                [defaults setValue:user.smsVerified forKey:@"MobileVerified"];
                                
                                NSString *userImage = params[@"profile_pic"];
                                NSURL *url = [NSURL URLWithString:userImage];
                                NSData *imageData = [NSData dataWithContentsOfURL:url];
                                
                                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                NSString *documentsDirectory = [paths objectAtIndex:0];
                                NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image1.png"];
                                
                                
                                [imageData writeToFile:savedImagePath atomically:NO];
                                
                                
                                [defaults synchronize];
                                
                                
                            }
                            
                            
                            // Clearing all Fields and ImageView
                            
                            [self.view endEditing:YES];
                            for (UIView *view in [self.view subviews])
                            {
                                if ([view isKindOfClass:[UITextField class]])
                                {
                                    UITextField *textField = (UITextField *)view;
                                    textField.text = @"";
                                }
                            }
            
                            [self.view hideToastActivity];
                            
                            [defaults setBool:YES forKey:@"isNewUser"];
                            [defaults setValue:@"Yes" forKey:@"isUserNew"];
                            
                            [defaults setValue:@"new_user" forKey:@"otp_validation"];
                            VerifyOTP *verifyOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTP"];
                            UINavigationController *navFrogotPwd = [[UINavigationController alloc] initWithRootViewController:verifyOTP];
                            [self presentViewController:navFrogotPwd animated:YES completion:nil];
                            
                        }
                        else{
                            
                            CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
                            style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                            [self.view makeToast:@"There was an error processing the sign up" duration:1 position:CSToastPositionBottom style:style];
                            for (UIView *view in [self.view subviews]) {
                                if ([view isKindOfClass:[UITextField class]]) {
                                    UITextField *textField = (UITextField *)view;
                                    textField.text = @"";
                                }
                            }
                               self.userImage.image = nil;
                            
                            [self.view hideToastActivity];
                            
                        }
                    }
                }
                if (connectionError) {
                   
                    [self.view hideToastActivity];
                    
                    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                    style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                    [self.view makeToast:@"Could not connect to Server. Please try again" duration:2 position:CSToastPositionBottom style:style];
                
                }
                
            }];
            
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
            [indicator stopAnimating];
            [self.view hideToastActivity];
        }
        
    
}

/***** Building API post call param body Method  *******/
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

/***** Gender Selection Action Method******/
- (IBAction)segmentAction:(UISegmentedControl *)sender {
   
    
    if (sender.selectedSegmentIndex == 0) {
        user.gender = @"M";
    }else{
        user.gender = @"F";
    }
    
   
    
    [[NSUserDefaults standardUserDefaults] setValue:user.gender forKey:@"Gender" ];
    
    
}

@end
