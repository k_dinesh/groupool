//
//  OfferDataModel.m
//  Groupool
//
//  Created by HN on 31/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "OfferDataModel.h"

@implementation OfferDataModel
@synthesize fromGroupID,toGroupID,fromGroupName,toGroupName,defaultRidePrice,startRideTime,routeID,maxRidePrice,userRidePrice,rideTime;

static OfferDataModel *instance;

+ (OfferDataModel *) getInstance
{
    @synchronized(self)
    {
        if(instance == nil)
        {
            instance = [OfferDataModel new];
        }
    }
    return instance;
}


@end
