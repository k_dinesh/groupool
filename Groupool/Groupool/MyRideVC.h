//
//  MyRideVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyRideTableViewCellVC.h"
#import "MyRidesDataModel.h"
#import "ProfileVC.h"
#import "CustomPagerViewController.h"

@interface MyRideVC : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;



@property (strong,nonatomic) NSMutableArray *rideDetails;

@property (nonatomic, strong) NSDate *selectedTime;


@property (strong) UIView *customView;



@end
