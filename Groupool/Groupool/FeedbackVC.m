//
//  FeedbackVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "FeedbackVC.h"
#import "MenuVC.h"
#import "CustomPagerViewController.h"
#import "UIColor+GroupoolColors.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"


@interface FeedbackVC ()
{
    UIActivityIndicatorView *indicator;
    
}
@end

@implementation FeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Feedback";
    _btnSendFeedback.layer.cornerRadius = 5;
    _txtFeedback.delegate = self;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    // Busy Indicator
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    indicator.frame = CGRectMake(0, 0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    
    [_txtFeedback.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_txtFeedback.layer setBorderWidth:1.0];
    
    
    _txtFeedback.layer.cornerRadius = 5;
    _txtFeedback.clipsToBounds = YES;
    
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    
}


- (void) backButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Textfield Delegates
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void) textViewDidEndEditing:(UITextView *)textView {
    [self.view endEditing:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)sendFeedbackAction:(id)sender {
    
    if([self.txtFeedback.text isEqualToString:@""])
    {
        
        CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Please enter feedback message" duration:1.0 position:CSToastPositionBottom style:style];
        
    }
    else
    {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDateFormatter *formatter;
        NSString        *createdTime;
        NSString        *userID;
        NSString        *messageID;
        NSString        *phpID;
        NSString        *isActive;
        NSString        *feedbackMessage;
        
        // Current Time Assigned
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        createdTime = [formatter stringFromDate:[NSDate date]];
        
        // User ID
        userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
        
        //Message ID
        messageID = @"0";
        
        // php ID
        phpID = @"0";
        
        // is Active
        isActive = @"1";
        
        // Description
        feedbackMessage = _txtFeedback.text;
        
        @try {
            
            NSMutableDictionary *dataDict = [NSMutableDictionary new];
            
            [dataDict setValue:messageID forKey:@"id"];
            [dataDict setValue:userID forKey:@"user_id"];
            [dataDict setValue:createdTime forKey:@"created_time"];
            [dataDict setValue:phpID forKey:@"phpid"];
            [dataDict setValue:isActive forKey:@"is_active"];
            [dataDict setValue:feedbackMessage forKey:@"description"];
            
            
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                               options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                 error:&error];
            
            NSString *postStr;
            if (! jsonData) {
                NSLog(@"Got an error: %@", error);
            } else {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
               
                postStr = [NSString stringWithFormat:@"[%@]",jsonString];
               
            }
            
            NSError *err = nil;
            NSURLResponse *response;
            NSData *localData = nil;
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FEEDBACK_POST_URL]];
            
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[postStr dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response,
                                                       NSData *data, NSError *connectionError)
            {
                if (data.length > 0 && connectionError == nil)
                    
                {
                    
//                    NSString *dataString = [[NSString alloc] initWithData:localData encoding:NSUTF8StringEncoding];
                   
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Thank you for your feedback. We will get back to you soon." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [self.view hideToastActivity];
                        CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"] ;
                        UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
                        
                        [self.navigationController presentViewController:navHome animated:YES completion:nil];
                        
                        
                    }];
                    [alert addAction:alertAction];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else
                {
                    
                    [self.view hideToastActivity];
                    CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
                    style.messageFont = [UIFont systemFontOfSize:12];
                    [self.view makeToast:@"Could not connect to server" duration:1.0 position:CSToastPositionBottom style:style];
                    
                   
                }
                
            }];
            
            
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
            [self.view hideToastActivity];
        }
    }
}
@end
