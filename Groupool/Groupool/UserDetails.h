//
//  UserDetails.h
//  
//
//  Created by Parth Pandya on 2/5/16.
//
//

#import <Foundation/Foundation.h>

@interface UserDetails : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *mobileNumber;
@property (nonatomic,strong) NSString *password;
@property (nonatomic,strong) NSString *OTP;
@property (nonatomic,strong) NSString *personalEmail;
@property (nonatomic,strong) NSString *officeEmail;
@property (nonatomic,strong) NSString *companyName;
@property (nonatomic,strong) NSString *appVersion;
@property (nonatomic,strong) NSString *carDetails;
@property (nonatomic,strong) NSString *carNumber;
@property (nonatomic,strong) NSString *carType;
@property (nonatomic,strong) NSString *personalEmailVerified;
@property (nonatomic,strong) NSString *officeEmailVerified;
@property (nonatomic,strong) NSString *smsVerified;

@end
