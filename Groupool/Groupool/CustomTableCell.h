//
//  CustomTableCell.h
//  KivaLoan
//
//  Created by Simon on 5/7/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableCell : UITableViewCell


@property (weak,nonatomic) IBOutlet UILabel *feedMessage;

@property (weak,nonatomic) IBOutlet UILabel *feedTime;
@property (weak,nonatomic) IBOutlet UIImageView *carType;


@end
