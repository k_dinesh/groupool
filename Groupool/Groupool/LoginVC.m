
//  LoginVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "LoginVC.h"
#import "CustomPagerViewController.h"
#import "AppStartVC.h"
#import "SendOTPVC.h"
#import "UIColor+GroupoolColors.h"
#import "UIView+Toast.h"
#import "VerifyOTP.h"

@interface LoginVC ()
{
    CALayer *border;
    CGFloat borderWidth ;
    CALayer *pwdBorder ;
    CGFloat borderWidth1 ;
    NSString *userID;
    UIActivityIndicatorView *indicator;
    
}

@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

CGFloat animatedDistance;
#define ACCEPTABLE_CHARECTERS @"0123456789"

@implementation LoginVC
@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    self.title = @"Login";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    user = [[UserDetails alloc] init];
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.textField.delegate = self;
    self.txtPassword.delegate = self;
    self.txtMobileNumber.delegate = self;
    
    self.btnLogin.layer.cornerRadius= 5;
   
    self.txtMobileNumber.keyboardType = UIKeyboardTypeNumberPad;
    
    // Busy Indicator
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    
    // Mobile Number Textfield Customization
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0,_txtMobileNumber.frame.size.height - borderWidth , _txtMobileNumber.frame.size.width, _txtMobileNumber.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtMobileNumber.layer addSublayer:border];
    _txtMobileNumber.layer.masksToBounds = YES;
    
    // Password Textfield Customization
    pwdBorder = [CALayer layer];
    borderWidth1 = 1.5;
    pwdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    pwdBorder.frame = CGRectMake(0, _txtPassword.frame.size.height - borderWidth1, _txtPassword.frame.size.width, _txtPassword.frame.size.height);
    pwdBorder.borderWidth = borderWidth1;
    [_txtPassword.layer addSublayer:pwdBorder];
    _txtPassword.layer.masksToBounds = YES;
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self userLoginSession];
}

#pragma Mark - Back Button Action
- (void) backButtonAction {
    
    AppStartVC *startView = [self.storyboard instantiateViewControllerWithIdentifier:@"AppStartVC"];
    startView.isUserLoggedOut = YES;
    
    [self.navigationController presentViewController:startView animated:YES completion:nil];
    
}

#pragma mark - Textfield Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if([_txtMobileNumber resignFirstResponder])
        border.borderColor = [UIColor lightGrayColor].CGColor;
    if([_txtPassword resignFirstResponder])
        pwdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == _txtMobileNumber) {
        border.borderColor = [UIColor groupoolMainColor].CGColor;
        pwdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtPassword)
    {
        border.borderColor = [UIColor lightGrayColor].CGColor;
        pwdBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return NO;
}

#pragma mark Login Method
- (IBAction)loginButtonAction:(id)sender {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    
    [self.view endEditing:YES];
    
    if ([_txtMobileNumber.text isEqualToString:@""] || [_txtPassword.text isEqualToString:@""]) {
        
        [self.view endEditing:YES];
        
        [self.view makeToast:@"Please enter Login details" duration:1 position:CSToastPositionBottom style:style];
    }
    else{
        
        NSString *str= @"[789][0-9]{9}";
        
        NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
        if([no evaluateWithObject:_txtMobileNumber.text]==NO)
        {
            
            [self.view makeToast:@"Please enter correct Mobile No." duration:1.5 position:CSToastPositionBottom style:style];
            return;
            
        }
        
        [self.view makeToastActivity:CSToastPositionCenter];
        [self checkUser];
        
    }
    
}
- (void)checkUser {
    
    
    
                                 dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            {
                NSUserDefaults *defaults = [NSUserDefaults  standardUserDefaults];
                NSString *mobile = _txtMobileNumber.text;
                NSString *pwd = _txtPassword.text;
                NSString *fbID = @"0";
                
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@",LOGIN_URL,mobile,pwd,fbID]];
                
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response,
                                                           NSData *data, NSError *connectionError)
                 {
                     [self.view hideToastActivity];
                     if (data.length > 0 && connectionError == nil )
                     {
                         NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
                         
                         if (jsonArray == NULL || jsonArray == nil) {
                             
                             CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                             
                             style.messageFont = [UIFont systemFontOfSize:12];
                             style.messageAlignment = NSTextAlignmentCenter;
                             style.cornerRadius = 12;
                             [self.view makeToast:@"Oops..!! server error occurred. Contact info@groupool.in for help" duration:3 position:CSToastPositionBottom style:style];
                             
                             [self.view endEditing:YES];
                             _txtMobileNumber.text = @"";
                             _txtPassword.text = @"";
                             pwdBorder.borderColor = [UIColor lightGrayColor].CGColor;
                             return ;
                         }
                         
                         if (data.length == 1 || data.length == 0) {
                             
                             [self.view endEditing:YES];
                             [self.view hideToastActivity];
                             
                             CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                             
                             style.messageFont = [UIFont systemFontOfSize:12];
                             style.messageAlignment = NSTextAlignmentCenter;
                             style.cornerRadius = 12;
                             [self.view makeToast:@"Username or Password incorrect" duration:1 position:CSToastPositionBottom style:style];
                             
                         }
                         else{
                             dispatch_async(dispatch_get_main_queue(), ^{

                                 for (NSDictionary *params in jsonArray)
                                 {
                                     user.userID = params[@"id"];
                                     [defaults setValue:user.userID forKey:@"UserID"];
                                     
                                     user.password = params[@"password"];
                                     [defaults setValue:user.password forKey:@"Password"];
                                     
                                     user.name = params[@"name"];
                                     [defaults setValue:user.name forKey:@"UserName"];
                                     
                                     user.personalEmail = params[@"email"];
                                     [defaults setValue:user.personalEmail forKey:@"Email"];
                                     
                                     user.OTP = params[@"otp"];
                                     [defaults setValue:user.OTP forKey:@"OTP"];
                                     
                                     user.smsVerified = params[@"sms_verified"];
                                     [defaults setValue:user.smsVerified forKey:@"MobileVerified"];
                                     
                                     user.officeEmailVerified = params[@"office_email_verified"];
                                     [defaults setValue:user.officeEmailVerified forKey:@"OfficeEmailVerified"];
                                     
                                     user.carDetails = params[@"car_details"];
                                     [defaults setValue:user.carDetails forKey:@"CarDetails"];
                                     
                                     user.carType = params[@"car_type"];
                                     [defaults setValue:user.carType forKey:@"CarType"];
                                     
                                     user.carNumber = params[@"car_number"];
                                     [defaults setValue:user.carNumber forKey:@"CarNumber"];
                                     
                                     
                                     NSString *userImage = params[@"profile_pic"];
                                     
                                     [defaults setValue:userImage forKey:@"ProfilePic"];
                                     

                                     [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"newRootNotAdded"];
                                     
                                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isPasswordChanged"];
                                     
                                      [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"PasswordChanged"];

                                     [defaults synchronize];
                                     
                                 }
                                 
                                 if ([[defaults valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
                                     
                                     [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"isUserNew"];
                                     VerifyOTP *verifyOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTP"];
                                     UINavigationController *navFrogotPwd = [[UINavigationController alloc] initWithRootViewController:verifyOTP];
                                     [self presentViewController:navFrogotPwd animated:YES completion:nil];
                                 }
                                 else
                                 {
                                     CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
                                     
                                     UINavigationController *navMain = [[UINavigationController alloc] initWithRootViewController:homeView];
                                     [self presentViewController:navMain animated:YES completion:nil];
                                     
                                 }
                                 
                                 
                                 
                             });
                         }
                     }
                     else
                     {
                         [self.view hideToastActivity];
                         
                         CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                         style.messageFont = [UIFont systemFontOfSize:12];
                         style.messageAlignment = NSTextAlignmentCenter;
                         style.cornerRadius = 12;
                         [self.view makeToast:@"Connection could not be made to server. Please Try again" duration:1 position:CSToastPositionBottom style:style];
                        
                     }
                 }];
            }
        }
        @catch (NSException *exception) {
            [self.view hideToastActivity];
            NSLog(@"%@",exception.description);
        }
        
    });

    
}

#pragma mark - User Login Session
- (void)userLoginSession {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
//    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isPasswordChanged"]){
    
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"PasswordChanged"] isEqualToString:@"YES"]){
            
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        
        style.messageFont = [UIFont systemFontOfSize:12];
        style.messageAlignment = NSTextAlignmentCenter;
        style.cornerRadius = 12;
        [self.view makeToast:@"Login with new password" duration:1 position:CSToastPositionBottom style:style];
        
        
    }
    
}

#pragma mark - Forgot Passwod Action
- (IBAction)forgotPasswordBtnAction:(id)sender {
    
    SendOTPVC *sendOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"SendOTPVC"];
    UINavigationController *navFrogotPwd = [[UINavigationController alloc] initWithRootViewController:sendOTP];
    [self presentViewController:navFrogotPwd animated:YES completion:nil];
    
}


@end
