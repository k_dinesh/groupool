//
//  SendOTPVC.m
//  Groupool
//
//  Created by HN on 20/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "SendOTPVC.h"
#import "VerifyOTPVC.h"
#import "UIColor+GroupoolColors.h"
#import "UIView+Toast.h"

@interface SendOTPVC ()
{
    CALayer *border;
    CGFloat borderWidth ;
    UIActivityIndicatorView *indicator;
}
@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
CGFloat animatedDistance;

@implementation SendOTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    _textField.delegate = self;
    _txtMobileNumber.delegate= self;
    _txtMobileNumber.keyboardType = UIKeyboardTypeNumberPad;
    _btnSendOTP.layer.cornerRadius = 3;
    
    self.title = @"Forgot Password";
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    // Navigationbar Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    // Busy Indicator
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    
    // Mobile Number TExtField Customization
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, _txtMobileNumber.frame.size.height - borderWidth, _txtMobileNumber.frame.size.width, _txtMobileNumber.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtMobileNumber.layer addSublayer:border];
    _txtMobileNumber.layer.masksToBounds = YES;
}


#pragma mark - Textfield Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([_txtMobileNumber resignFirstResponder])
        border.borderColor = [UIColor lightGrayColor].CGColor;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == _txtMobileNumber)
    {
        border.borderColor = [UIColor groupoolMainColor].CGColor;
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    if (textfield == _txtMobileNumber) {
        border.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    
    }
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return YES;
}

- (void) backButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Send OTP to Mobile Action
- (IBAction)sendOTPAction:(id)sender {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    

    [self.view endEditing:YES];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] isEqualToString:_txtMobileNumber.text]) {
        
    }
    
    {
        if ([_txtMobileNumber.text isEqualToString:@""])
        {
            [self.view makeToast:@"Please enter Mobile No." duration:2.0 position:CSToastPositionBottom style:style];
        }
        else
        {
            
            NSString *str= @"[789][0-9]{9}";
            
            NSPredicate *no=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
            if([no evaluateWithObject:_txtMobileNumber.text]==NO)
            {
                
                [self.view makeToast:@"Please enter correct Mobile No." duration:1.5 position:CSToastPositionBottom style:style];
                return;
                
            }
            [self.view makeToastActivity:CSToastPositionCenter];
            
            @try {
                NSString *mobileNumber = _txtMobileNumber.text;
                [[NSUserDefaults standardUserDefaults] setValue:mobileNumber forKey:@"MobNumber"];
                
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",FORGOT_PASSWORD_SEND_OTP,mobileNumber]];
                
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response,
                                                           NSData *data, NSError *connectionError)
                 {
                     
                     if (data == NULL) {
                         
                         CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                         style.messageFont = [UIFont systemFontOfSize:12];
                         style.messageAlignment = NSTextAlignmentCenter;
                         style.cornerRadius = 12;
                         [self.view makeToast:@"Server connection error" duration:3 position:CSToastPositionBottom style:style]
                         ;
                         return ;
                         
                     }
                     
                     if (data.length > 0 && connectionError == nil)
                     {
                         
                         
                         NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                         if ([dataString isEqualToString:@"0"]) {
                             
                             [self.view hideToastActivity];
                             
                             CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                             
                             style.messageFont = [UIFont systemFontOfSize:12];
                             style.messageAlignment = NSTextAlignmentCenter;
                             
                             [self.view makeToast:@"Oops! Error Receiving data from the server.Please try again or contact info@Groupool.in" duration:5 position:CSToastPositionBottom style:style];
                         }
                         else{

                             [self.view hideToastActivity];
                             VerifyOTPVC *verifyOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTPVC"];
                             UINavigationController *navVerifyOTP = [[UINavigationController alloc] initWithRootViewController:verifyOTP];
                             [self presentViewController:navVerifyOTP animated:YES completion:nil];
                             

                         }
                         
                     }
                     else
                     {
                        
                         [self.view hideToastActivity];
                         
                         CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                         
                         style.messageFont = [UIFont systemFontOfSize:12];
                         style.messageAlignment = NSTextAlignmentCenter;
                        
                         [self.view makeToast:@"Could not connect to server. Please try again" duration:2 position:CSToastPositionBottom style:style];
                         
                     }
                 }];
                
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
                [self.view hideToastActivity];
            }
            
            
        }
        
        
    }
    
    
}

@end
