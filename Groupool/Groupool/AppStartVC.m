//
//  AppStartVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "AppStartVC.h"
#import "VerifyOTP.h"
#import "AddRouteVC.h"

#import "CustomPagerViewController.h"

NSString *userID;

@interface AppStartVC ()

@end

@implementation AppStartVC
@synthesize isUserLoggedOut;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    _btnLogin.layer.cornerRadius = 5;
    _btnSignup.layer.cornerRadius = 5;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"password_changed"];
    if (isUserLoggedOut) {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = YES;
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"addNewRoot"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self checkLoggedinStatus:self];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)checkLoggedinStatus:(id)sender {
 
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    NSLog(@"is Mobile Verified :%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileVerified"]);
    // check user is already logged in
    
    NSString *tmpPass = [defaults objectForKey:@"Password"];
    
    NSString *pass = [defaults valueForKey:@"Password"];
    
    if ([pass isEqualToString:@""] || (pass == nil))
    {
        return;
    }
    if (![tmpPass isEqualToString:@""] && !(tmpPass == nil))
    {
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileVerified"] isEqualToString:@"0"])
        {
            [defaults setValue:@"profile" forKey:@"otp_validation"];
            [defaults setValue:@"No" forKey:@"isUserNew"];
            [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"newRootNotAdded"];
            VerifyOTP *verifyOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTP"];
            UINavigationController *navFrogotPwd = [[UINavigationController alloc] initWithRootViewController:verifyOTP];
            [self presentViewController:navFrogotPwd animated:YES completion:nil];
            
        }
        else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"newRootNotAdded"] isEqualToString:@"No"])
        {
            AddRouteVC *addRoute =[self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
            UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
            [self presentViewController:navRoute animated:YES completion:nil];
            
        }
         else 
        {
            CustomPagerViewController *mainViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
            UINavigationController *navmainViewController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
            [self presentViewController:navmainViewController animated:YES completion:nil];
            
        }
     
    }
    
    
}


- (IBAction)loginAction:(id)sender {
    
    LoginVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    UINavigationController *navLoginVC = [[UINavigationController alloc]initWithRootViewController:loginVC];
    [self presentViewController:navLoginVC animated:YES completion:nil];
    
}

- (IBAction)signUpAction:(id)sender {
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
        [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
    }
    
    SignUpVC *signupView =[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpVC"];
    UINavigationController *navSignUp = [[UINavigationController alloc] initWithRootViewController:signupView];
    
    [self presentViewController:navSignUp animated:YES completion:nil];
    
}

- (IBAction)btnTermsAndCondition:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:TERMS_AND_CONDITIONS_URL]];
}


@end
