//
//  SendOTPVC.h
//  Groupool
//
//  Created by HN on 20/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendOTPVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIButton *btnSendOTP;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
- (IBAction)sendOTPAction:(id)sender;

@end
