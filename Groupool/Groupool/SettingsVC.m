//
//  SettingsVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "SettingsVC.h"
#import "MenuVC.h"
#import "CustomPagerViewController.h"
#import "UIColor+GroupoolColors.h"
#import "UIView+Toast.h"

NSString *userID;
@interface SettingsVC ()
{
    NSString *newUserSwitchState;
    NSString *rideRequestedSwitchState;
    NSString *rideOfferedSwitchState;
    NSString *autoSuggestSwitchState;
    int switchTag;
    
}
@end


@implementation SettingsVC



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    
    // Bartint Colour
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    // Tint Colour
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    // Done Button
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(settingEditDoneAction:)];
    self.navigationItem.rightBarButtonItem = doneBtn;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getUserSettings];
    });
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    isNewUserJoined = NO;
    isRideOffered = NO;
    isRideRequested = NO;
    isSuggestionOrRequest = NO;
    
}


#pragma mark - Back button implementation
- (void) backButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark - Update changed profile implementation
- (void)settingEditDoneAction:(id)sender {
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    userID = [defaults valueForKey:@"UserID"];
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@/%@/%@",SET_USER_SETTINGS_URL,userID,_userNotif,_rideRequestNotif,_rideOfferNotif,@"1"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
    {
        if (data.length > 0 && connectionError == nil)
        {
//            NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
           
            [self.view hideToastActivity];
            CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
            style.messageFont = [UIFont systemFontOfSize:12];
            [self.view makeToast:@"Settings changed" duration:1.0 position:CSToastPositionBottom style:style];
            
            CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
            UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
            [self presentViewController:navHome animated:YES completion:nil];
            
            
        }
        else
        {
            
            [self.view hideToastActivity];
            CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
            style.messageFont = [UIFont systemFontOfSize:12];
            [self.view makeToast:@"Could not connect to server" duration:1.0 position:CSToastPositionBottom style:style];
        }
    }];
    
    
}

#pragma mark - Get user settings implementation
- (void) getUserSettings{
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    userID = [defaults valueForKey:@"UserID"];
    
    
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_SETTINGS_URL,userID]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
    {
        if (data.length > 0 && connectionError == nil)
        {
            NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
            
            for (NSDictionary *dataDict in jsonArray) {
                
                
                _userNotif = dataDict[@"notifs_new_user"];
                _rideRequestNotif = dataDict[@"notifs_ride_request"];
                _rideOfferNotif = dataDict[@"notifs_ride_offer"];
                _autoSuggestNotif = dataDict[@"notifs_auto_suggest"];
                
            }
            [self checkForUpdate];
            [self.view hideToastActivity];
            
            
        }
        else
        {
          
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self.view hideToastActivity];
                [self reconnectToServer];
                
            }];
            [alert addAction:alertAction];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }];
}
- (void)reconnectToServer{
    
    [self getUserSettings];
    
}
- (void)checkForUpdate{
    
    if ([_userNotif  isEqual: @"1"])
    {
        
        isNewUserJoined = YES;
        [self.btnNewUserJoins setImage:[UIImage imageNamed:isNewUserJoined ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
        
    }
    
    
    if ([_rideRequestNotif isEqual: @"1"])
    {
        
        isRideRequested = YES;
        [self.btnRideRequested setImage:[UIImage imageNamed:isRideRequested ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
        
        
    }
    if ([_rideOfferNotif isEqual: @"1"])
    {
        
        isRideOffered = YES;
        [self.btnRideOffered setImage:[UIImage imageNamed:isRideOffered ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
        
    }
    if ([_autoSuggestNotif isEqual: @"1"])
    {
        
        isSuggestionOrRequest = YES;
        [self.btnSuggestOrRequest setImage:[UIImage imageNamed:isSuggestionOrRequest ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
        
    }
    
}


- (IBAction)newUserJoinNotifAction:(id)sender {
    switchTag = 0;
    if(isNewUserJoined)
    {
        
        _userNotif = @"0";
     
    }
    else
    {
        _userNotif = @"1";
      
    }
    [self setSwitches];
}
- (IBAction)rideRequestedNotifAction:(id)sender {
    switchTag = 1;
    if(isRideRequested)
    {
        
        _rideRequestNotif = @"0";
       
    }
    else
    {
        _rideRequestNotif = @"1";
       
    }
    [self setSwitches];
}
- (IBAction)rideOfferedNotifAction:(id)sender {
    switchTag = 2;
    if(isRideOffered)
    {
        
        _rideOfferNotif = @"0";
       
    }
    else
    {
        _rideOfferNotif = @"1";
       
    }
    
    [self setSwitches];
    
}
- (IBAction)suggestionOrRequestNotifAction:(id)sender {
    switchTag = 3;
    if(isSuggestionOrRequest)
    {
        
        _autoSuggestNotif= @"0";
       
    }
    else {
        _autoSuggestNotif = @"1";
       
    }
    
    [self setSwitches];
}

- (void) setSwitches{
    
    switch (switchTag) {
        case 0:
            isNewUserJoined = !isNewUserJoined;
            [self.btnNewUserJoins setImage:[UIImage imageNamed:isNewUserJoined ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
            
            break;
            
        case 1:
            isRideRequested = !isRideRequested;
            [self.btnRideRequested setImage:[UIImage imageNamed:isRideRequested ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
            
            break;
            
        case 2:
            isRideOffered = !isRideOffered;
            [self.btnRideOffered setImage:[UIImage imageNamed:isRideOffered ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
            break;
            
        case 3:
            isSuggestionOrRequest = !isSuggestionOrRequest;
            [self.btnSuggestOrRequest setImage:[UIImage imageNamed:isSuggestionOrRequest ? @"selected.png" :@"un-selected.png"] forState:UIControlStateNormal];
            
            break;
            
        default:
            break;
    }
    
}

@end
