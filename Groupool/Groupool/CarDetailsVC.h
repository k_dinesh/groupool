//
//  CarDetailsVC.h
//  Groupool
//
//  Created by Parth Pandya on 14/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+GroupoolColors.h"

@protocol carDetailDelegate ;


@interface CarDetailsVC : UIViewController<UITextFieldDelegate>
{
    
    NSString *aCarType;
    CGFloat animatedDistance;
    
    CALayer *tcdBorder,
            *tcnBorder;
    
    CGFloat tcdBorderWidth,
            tcnBorderWidth;

    IBOutlet UITextField *txtCarDetails;
    IBOutlet UITextField *txtCarNumber;
    

    IBOutlet UIButton *saveAndOffer;
    IBOutlet UIButton *btnHatchBack;
    IBOutlet UIButton *btnSedan;
    IBOutlet UIButton *btnSUV;
    IBOutlet UIButton *btnOther;
    
    
}
@property (weak, nonatomic) IBOutlet UIView *popUpView;
- (IBAction)saveAndOffer:(id)sender;


- (IBAction)carTypeSelection:(UIButton *)sender;

@property (nonatomic,strong) NSString *name;

@property (assign, nonatomic) id<carDetailDelegate>carDelegate;

@property (nonatomic,strong) NSString *startGrpID;
@property (nonatomic,strong) NSString *womenOnly;
@property (nonatomic,strong) NSString *isActice;
@property (nonatomic,strong) NSString *carNumber;
@property (nonatomic,strong) NSString *editedID;
@property (nonatomic,strong) NSString *carType;
@property (nonatomic,strong) NSString *offeredSeat;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *carDetails;
@property (nonatomic,strong) NSString *endGrpID;
@property (nonatomic,strong) NSString *theDate;
@property (nonatomic,strong) NSString *comment;
@property (nonatomic,strong) NSString *phpID;
@property (nonatomic,strong) NSString *OS;
@property (nonatomic,strong) NSString *rideID;
@property (nonatomic,strong) NSString *dayString;
@property (nonatomic,strong) NSString *tDate;

@end

@protocol carDetailDelegate <NSObject>

- (void) fetchUpdatedData:(CarDetailsVC *)refetchRides;


@end