//
//  AddRouteVC.h
//  Groupool
//
//  Created by HN on 03/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "MVPlaceSearchTextField.h"
#import <MapKit/MapKit.h>

@protocol AddRouteDelegate;

@interface AddRouteVC : UIViewController<UITextFieldDelegate,PlaceSearchTextFieldDelegate,GMSMapViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate>

@property (weak,nonatomic) UITextField *textField;



@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *homeAddressText;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *officeAddressText;
@property (weak, nonatomic) IBOutlet UIButton *showMap;
@property (nonatomic) BOOL isUserNew;
- (IBAction)showMap:(id)sender;
@property (weak, nonatomic) IBOutlet GMSMapView *mapViewBackground;

@property (weak, nonatomic) IBOutlet UIView *routeMap;

@property (assign,nonatomic) id <AddRouteDelegate>delegate;
@property (strong) UIView *customView;
@property (strong,nonatomic) NSMutableArray *rdNameArray;
@property (strong,nonatomic) NSMutableArray *rdLocationArray;
@property (strong,nonatomic) NSMutableArray *rdMemberCountArray;
@property (strong,nonatomic) NSMutableArray *rdRideCountArray;


@end

@protocol AddRouteDelegate <NSObject>

@optional
- (void)refreshTableData:(AddRouteVC *)refreshData;

@end
