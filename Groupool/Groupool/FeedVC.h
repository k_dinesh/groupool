//
//  FeedVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>




@interface FeedVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
  
}

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong,nonatomic) UIRefreshControl *refreshControl;
@property (strong) UIView *customView;

@property (nonatomic) NSMutableArray *messageArray;
@property (nonatomic) NSMutableArray *feedTimeArray;
@property (nonatomic) NSMutableArray *txtColourArray;
@property (nonatomic) NSMutableArray *typesArray;
@property (nonatomic) NSString *userID;





@end
