//
//  ProfileVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "ProfileVC.h"
#import "CustomPagerViewController.h"
#import "MenuVC.h"
#import "ChangePasswordVC.h"
#import "UIColor+GroupoolColors.h"
#import "AppStartVC.h"
#import "UIImage+fixOrientation.h"
#import "NSString+EmailValidation.h"
#import "UIView+Toast.h"
#import "VerifyOTP.h"



@interface ProfileVC ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    
    CALayer *tunBorder,
            *tmnBorder,
            *tpeBorder,
            *toeBorder,
            *tcdBorder,
            *tcnBorder;
    CGFloat tunBorderWidth,
            tmnBorderWidth,
            tpeBorderWidth,
            toeBorderWidth,
            tcdBorderWidth,
            tcnBorderWidth;

    NSString *userID;
    BOOL isCalled;
    
    NSMutableDictionary *userDetailsDict;
    
}


@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;



@implementation ProfileVC
@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

    
    self.title = @"Edit Profile";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    isCalled = NO;
    
    user = [[UserDetails alloc] init];
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    // Done Button
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(profileEditDoneAction:)];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.rightBarButtonItem = doneBtn;
    
    imgUserPic.layer.cornerRadius = 22;
    imgUserPic.layer.masksToBounds = YES;
    imgUserPic.contentMode = UIViewContentModeScaleAspectFill;
    
    [btnVerifyMobileNumber setTag:1];
    [btnVerifyOfcEmail setTag:2];
    self.navigationItem.hidesBackButton = YES;
  
    /* Gender Selection */
    [self.optGender addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.textField.delegate = self;
    _txtUserName.delegate = self;
    _txtMobileNumber.delegate = self;
    _txtPersonamEmail.delegate = self;
    _txtOfficeEmail.delegate = self;
    _txtCarDetails.delegate = self;
    _txtCarNumber.delegate = self;
    
    
    
    [_btnHatchBack setTag:0];
    [_btnSedan setTag:1];
    [_btnSUV setTag:2];
    [_btnOther setTag:3];
    [_btnRequestRide setTag:4];
    
    
    // User Name textfield Customization
    
    tunBorder = [CALayer layer];
    tunBorderWidth = 1.5;
    tunBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tunBorder.frame = CGRectMake(0, _txtUserName.frame.size.height - tunBorderWidth,_txtUserName.frame.size.width,_txtUserName.frame.size.width);
    tunBorder.borderWidth = tunBorderWidth;
    [_txtUserName.layer addSublayer:tunBorder];
    _txtUserName.layer.masksToBounds = YES;
    
    
    // Mobile Number textfield Customization
    tmnBorder = [CALayer layer];
    tmnBorderWidth = 1.5;
    tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tmnBorder.frame = CGRectMake(0, _txtMobileNumber.frame.size.height - tmnBorderWidth,_txtMobileNumber.frame.size.width,_txtMobileNumber.frame.size.width);
    tmnBorder.borderWidth = tmnBorderWidth;
    [_txtMobileNumber.layer addSublayer:tmnBorder];
    _txtMobileNumber.layer.masksToBounds = YES;

    
    // Personal Email textfield Customization
    
    tpeBorder = [CALayer layer];
    tpeBorderWidth = 1.5;
    tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tpeBorder.frame = CGRectMake(0, _txtPersonamEmail.frame.size.height - tpeBorderWidth,_txtPersonamEmail.frame.size.width,_txtPersonamEmail.frame.size.width);
    tpeBorder.borderWidth = tpeBorderWidth;
    [_txtPersonamEmail.layer addSublayer:tpeBorder];
    _txtPersonamEmail.layer.masksToBounds = YES;

    
    // Office Email textfield Customization
    
    toeBorder = [CALayer layer];
    toeBorderWidth = 1.5;
    toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    toeBorder.frame = CGRectMake(0, _txtOfficeEmail.frame.size.height - toeBorderWidth,_txtOfficeEmail.frame.size.width,_txtOfficeEmail.frame.size.width);
    toeBorder.borderWidth = toeBorderWidth;
    [_txtOfficeEmail.layer addSublayer:toeBorder];
    _txtOfficeEmail.layer.masksToBounds = YES;

    
    // Car Details textfield Customization
    
    tcdBorder = [CALayer layer];
    tcdBorderWidth = 1.5;
    tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcdBorder.frame = CGRectMake(0, _txtCarDetails.frame.size.height - tcdBorderWidth,_txtCarDetails.frame.size.width,_txtCarDetails.frame.size.width);
    tcdBorder.borderWidth = tcdBorderWidth;
    [_txtCarDetails.layer addSublayer:tcdBorder];
    _txtCarDetails.layer.masksToBounds = YES;

    // Car Number textfield Customization
    
    tcnBorder = [CALayer layer];
    tcnBorderWidth = 1.5;
    tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcnBorder.frame = CGRectMake(0, _txtCarNumber.frame.size.height - tcnBorderWidth,_txtCarNumber.frame.size.width,_txtCarNumber.frame.size.width);
    tcnBorder.borderWidth = tcnBorderWidth;
    [_txtCarNumber.layer addSublayer:tcnBorder];
    _txtCarNumber.layer.masksToBounds = YES;

    // Assign User Image
        [self assignUserImage];

    // Select Image by tapping on Imageview
    
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [imgUserPic addGestureRecognizer:tapSelectedImage];
    [imgUserPic setUserInteractionEnabled:YES];
    
    [self fetchUserDetails];
}



#pragma mark - Get user details from API Implementation
- (void) fetchUserDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void){
    
        
        userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
        
        if (userID == nil) {
            [self.view hideToastActivity];
            return ;
        }
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_DETAILS_URL,userID]];
    
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             
             
                 if (data.length > 0 && connectionError == nil)
                 {
                     NSError *jsonError;
                     id parsedData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                     
                     if ([parsedData isKindOfClass: [NSArray class]]) {
                         NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             for (NSDictionary *dataDict in dataArray)
                             {
                                 
                                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                 
                                 user.name = dataDict[@"name"];
                               
                                 [defaults setValue:user.name forKey:@"UserName"];
                                 
                                 _txtUserName.text = user.name;
                                 
                                 user.userID = dataDict[@"id"];
                                 
                                 user.gender = dataDict[@"gender"];
                                 
                                 user.personalEmail = dataDict[@"email"];
                                 
                                 _txtPersonamEmail.text = user.personalEmail;
                                 
                                 user.mobileNumber = dataDict[@"mobile"];
                                 
                                 _txtMobileNumber.text = user.mobileNumber;
                                 [defaults setValue:user.mobileNumber forKey:@"MobileNumber"];
                                 
                                 user.password = dataDict[@"password"];
                                 [defaults setValue:user.password forKey:@"Password"];
                                 
                                 user.officeEmail = dataDict[@"office_email"];
                                 
                                 _txtOfficeEmail.text = user.officeEmail;
                                 
                                 user.carType = dataDict[@"car_type"];
                                 [defaults setValue:user.carType forKey:@"CarType"];
                                 
                                 user.carDetails = dataDict[@"car_details"];
                                 
                                 _txtCarDetails.text = user.carDetails;
                                 [defaults setValue:user.carDetails forKey:@"CarDetails"];
                                 user.carNumber = dataDict[@"car_number"];
                                 
                                 _txtCarNumber.text = user.carNumber;
                                 [defaults setValue:user.carNumber forKey:@"CarNumber"];
                                 
                                 user.OTP = dataDict[@"otp"];

                                 user.companyName = dataDict[@"company"];

                                 user.appVersion = dataDict[@"user_app_version"];

                                 user.officeEmailVerified = dataDict[@"office_email_verified"];
                                 
                                 user.smsVerified = dataDict[@"sms_verified"];
                                
                                 
                                 NSString *userImage = dataDict[@"profile_pic"];
                                 
                                 [defaults setValue:userImage forKey:@"ProfilePic"];
                                 
                                 [defaults synchronize];
                                 
                             }
                             [self setData];
                             
                         });
                         
                     }
                     else
                     {
                         
                         NSDictionary *params = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                   
                         [self saveJsonResponse:params];
                         
                     }

                     
                 }
                 else
                 {
                     [self.view hideToastActivity];
                     
                     
                     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                         
                         [self refetchUserDetails];
                     }];
                     [alert addAction:alertAction];
                     [self presentViewController:alert animated:YES completion:nil];
                     
                     
                 }
             
    
         }];
        
    
    });
    
    
    
}

- (void) setData {
    
    if ([user.smsVerified isEqualToString:@"1"]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:user.smsVerified forKey:@"MobileVerified"];
       
        [btnVerifyMobileNumber setTitle:@"Verified" forState:UIControlStateNormal];
        [btnVerifyMobileNumber setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btnVerifyMobileNumber.userInteractionEnabled = NO;
        _txtMobileNumber.userInteractionEnabled = NO;
    
    }
    else{
        [btnVerifyMobileNumber setTitle:@"Verify" forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"MobileVerified"];
    }
    
    // Verify Office Email Button
    if ([user.officeEmailVerified isEqualToString:@"1"]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:user.officeEmailVerified forKey:@"OfficeEmailVerified"];
        
        [btnVerifyOfcEmail setTitle:@"Verified" forState:UIControlStateNormal];
        [btnVerifyOfcEmail setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btnVerifyOfcEmail.userInteractionEnabled = NO;
        _txtOfficeEmail.userInteractionEnabled = NO;
        _txtOfficeEmail.textColor = [UIColor lightGrayColor];
    }
    else{
        [btnVerifyOfcEmail setTitle:@"Verify" forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"OfficeEmailVerified"];
    }
    
    carType = user.carType;
    
    if ([carType isEqualToString:@"HB"]) {
        
        [self.btnHatchBack setImage:[UIImage imageNamed:@"car_two_selected.png"] forState:UIControlStateNormal];
    }
    if ([carType isEqualToString:@"SE"]) {
        
        
        [self.btnSedan setImage:[UIImage imageNamed:@"car_one_selected.png"] forState:UIControlStateNormal];
    }
    if ([carType isEqualToString:@"SU"]) {
        
        [self.btnSUV setImage:[UIImage imageNamed:@"car_three_selected.png"] forState:UIControlStateNormal];
    }
    if ([carType isEqualToString:@"OT"]) {
        
        [self.btnOther setImage:[UIImage imageNamed:@"car_four_selected.png"] forState:UIControlStateNormal];
    }
    

    if ([user.gender isEqualToString:@"M"]) {
        [_optGender setSelectedSegmentIndex:0];
    }else
    {
        [_optGender setSelectedSegmentIndex:1];
    }
    
    [self assignUserImage];
    [self.view hideToastActivity];
}
- (void) refetchUserDetails{
    if (!isCalled) {
        [self fetchUserDetails];
        isCalled = YES;
    }
}
- (void) updateFetchedData {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    _txtUserName.text = [defaults valueForKey:@"UserName"];

    _txtMobileNumber.text = [defaults valueForKey:@"MobileNumber"];
    _txtPersonamEmail.text = [defaults valueForKey:@"Email"];
    _txtOfficeEmail.text = [defaults valueForKey:@"OfcEmail"];
    _txtCarDetails.text = [defaults valueForKey:@"CarDetails"];
    _txtCarNumber.text = [defaults valueForKey:@"CarNumber"];
    
    
    
        gender = [[NSUserDefaults standardUserDefaults] valueForKey:@"Gender"];
        if ([gender isEqualToString:@"M"]) {
            [_optGender setSelectedSegmentIndex:0];
        }else
        {
            [_optGender setSelectedSegmentIndex:1];
        }
    
        carType = [[NSUserDefaults standardUserDefaults] valueForKey:@"CarType"];
        
        if ([carType isEqualToString:@"HB"]) {
            
            [self.btnHatchBack setImage:[UIImage imageNamed:@"car_two_selected.png"] forState:UIControlStateNormal];
        }
        if ([carType isEqualToString:@"SE"]) {
            
            
            [self.btnSedan setImage:[UIImage imageNamed:@"car_one_selected1.png"] forState:UIControlStateNormal];
        }
        if ([carType isEqualToString:@"SU"]) {
            
            [self.btnSUV setImage:[UIImage imageNamed:@"car_three_selected.png"] forState:UIControlStateNormal];
        }
        if ([carType isEqualToString:@"OT"]) {
            
            [self.btnOther setImage:[UIImage imageNamed:@"car_four_selected1.png"] forState:UIControlStateNormal];
        }

        // Set Mobile Email Verified state
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"1"]) {
        
        [btnVerifyMobileNumber setTitle:@"Verified" forState:UIControlStateNormal];
        [btnVerifyMobileNumber setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btnVerifyMobileNumber.userInteractionEnabled = NO;
    }
    else{
        [btnVerifyMobileNumber setTitle:@"Verify" forState:UIControlStateNormal];
    }
    
    // Verify Office Email Button
 
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"1"]) {
        
        [btnVerifyOfcEmail setTitle:@"Verified" forState:UIControlStateNormal];
        [btnVerifyOfcEmail setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        btnVerifyOfcEmail.userInteractionEnabled = NO;
        _txtOfficeEmail.userInteractionEnabled = NO;
        _txtOfficeEmail.textColor = [UIColor lightGrayColor];
    }
    else{
        [btnVerifyOfcEmail setTitle:@"Verify" forState:UIControlStateNormal];
    }
    
        // Verify Mobile Button
    
    
    [self assignUserImage];
    
      [self.view hideToastActivity];
    
}

#pragma mark - Save Json reply from API
- (void)saveJsonResponse:(NSDictionary *)dataDict {
    
   
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        

        fullName = dataDict[@"name"];
        //                             [defaults setValue:name forKey:@"UserName"];
        [defaults setValue:fullName forKey:@"UserName"];
    
    

        userID = dataDict[@"id"];
        [defaults setValue:userID forKey:@"UserID"];
    
        gender = dataDict[@"gender"];
        [defaults setValue:gender forKey:@"Gender"];
    
        personalEmail = dataDict[@"email"];
        [defaults setValue:personalEmail forKey:@"Email"];
    
        mobileNumber = dataDict[@"mobile"];
        [defaults setValue:mobileNumber forKey:@"MobileNumber"];
    
        NSString *aPassword = dataDict[@"password"];
        [defaults setValue:aPassword forKey:@"Password"];
    
        officeEmail = dataDict[@"office_email"];
        [defaults setValue:officeEmail forKey:@"OfcEmail"];
    
        carType = dataDict[@"car_type"];
        [defaults setValue:carType forKey:@"CarType"];
    
        carDetails = dataDict[@"car_details"];
        [defaults setValue:carDetails forKey:@"CarDetails"];
    
        carNumber = dataDict[@"car_number"];
        [defaults setValue:carNumber forKey:@"CarNumber"];
    
        NSString *otp = dataDict[@"otp"];
        [defaults setValue:otp forKey:@"OTP"];
    
        NSString *comapnyName = dataDict[@"company"];
        [defaults setValue:comapnyName forKey:@"CompanyName"];
    
        appVersion = dataDict[@"user_app_version"];
        [defaults setValue:appVersion forKey:@"AppVersion"];
    
        NSString *isOfficeEmailVerified = dataDict[@"office_email_verified"];
        [defaults setValue:isOfficeEmailVerified forKey:@"OfficeEmailVerified"];
    
        NSString *isMobileVerified = dataDict[@"sms_verified"];
        [defaults setValue:isMobileVerified forKey:@"MobileVerified"];
    
        NSString *userImage = dataDict[@"profile_pic"];
    [defaults setValue:userImage forKey:@"ProfilePic"];
    
    NSURL *imageURL = [NSURL URLWithString:userImage];
    NSData *data = [NSData dataWithContentsOfURL: imageURL];
    
    UIImage* image = [[UIImage alloc] initWithData:data];
    imgUserPic.image = image;

    [defaults synchronize];
    
}


#pragma mark - Textfield Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([_txtUserName resignFirstResponder]) {
        tunBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtMobileNumber resignFirstResponder]) {
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtPersonamEmail resignFirstResponder]) {
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtOfficeEmail resignFirstResponder]) {
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtCarDetails resignFirstResponder]) {
        tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtCarNumber resignFirstResponder]) {
        tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == _txtUserName)
    {
        tunBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
    if (textField == _txtMobileNumber) {
        tmnBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
    if (textField == _txtPersonamEmail) {
        tpeBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
    if (textField == _txtOfficeEmail) {
        toeBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
    if (textField == _txtCarDetails) {
        tcdBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
    if (textField == _txtCarNumber) {
        tcnBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
    if (textField == _txtUserName)
    {
        tunBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtMobileNumber) {
        tmnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtPersonamEmail) {
        tpeBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtOfficeEmail) {
        toeBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtCarDetails) {
        tcdBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtCarNumber) {
        tcnBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return NO;
}



#pragma mark - Assign user image implementation
- (void)assignUserImage{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *userImage = [[NSUserDefaults standardUserDefaults] valueForKey:@"ProfilePic"];
        
        NSURL *imageURL = [NSURL URLWithString:userImage];
        NSData *data = [NSData dataWithContentsOfURL: imageURL];
        
        UIImage* image = [[UIImage alloc] initWithData:data];
        
 
    NSData *imageData = UIImagePNGRepresentation(image);
        if (imageData == nil) {
            
            
                        imgUserPic.image = [UIImage imageNamed:@"ic_user_b.png"];
                    }else{
            
                        imgUserPic.image = image;
                    }

    });
    
}

#pragma Mark - Back Button Action & Implementation
- (void) backButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Verify Mobile/Email  Action & Implementation
- (IBAction)verifyMobileNumber:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setValue:@"profile" forKey:@"otp_validation"];
    
   
    [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"isUserNew"];
    VerifyOTP *verifyOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyOTP"];
    UINavigationController *navVerifyOTP = [[UINavigationController alloc] initWithRootViewController:verifyOTP];
    [self presentViewController:navVerifyOTP animated:YES completion:nil];
}

- (IBAction)verifyOfficeEmail:(id)sender {


    [self.view makeToastActivity:CSToastPositionCenter];
    
    @try {
        
        NSDictionary *params = @{
                                 @"user_id":userID,
                                 @"email_verified":@"1",
                                 @"email":_txtOfficeEmail.text,
                                 @"is_office":@"1",
                                 };
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:UPLOAD_DATA_URL]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        //-- Append data into posr url using following method
        NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params ];
        NSString *dataString = [[NSString alloc] initWithData:httpBody encoding:NSUTF8StringEncoding];

        
        request.HTTPBody = httpBody;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            [self.view hideToastActivity];
            
            CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
            
            style.messageFont = [UIFont systemFontOfSize:12];
            [self.view makeToast:@"You will receive verification link shortly" duration:2.0 position:CSToastPositionBottom style:style];
         
        }];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [self.view hideToastActivity];
    }

}

#pragma mark - Change Password Action
- (IBAction)changePasswordAction:(id)sender {
 
    ChangePasswordVC *changePassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
    UINavigationController *navChangePassword = [[UINavigationController alloc] initWithRootViewController:changePassword];
    [self presentViewController:navChangePassword animated:YES completion:nil];
    
    
}
- (IBAction)optGenderAction:(UISegmentedControl *)sender {
    
//    gender = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
}

#pragma mark - Car Selection Action
- (IBAction)carTypeSelectioAction:(UIButton *)btnType {
    switch (btnType.tag) {
        case 0:
            carType = @"HB";
            [_btnHatchBack setImage:[UIImage imageNamed:@"car_two_selected.png"] forState:UIControlStateNormal];
            [_btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [_btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [_btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
             [_btnRequestRide setImage:[UIImage imageNamed:@"thumb.png"] forState:UIControlStateNormal];
            

            break;
        case 1:

            carType = @"SE";
            [_btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [_btnSedan setImage:[UIImage imageNamed:@"car_one_selected.png"] forState:UIControlStateNormal];
            [_btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [_btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
             [_btnRequestRide setImage:[UIImage imageNamed:@"thumb.png"] forState:UIControlStateNormal];


            break;
        case 2:

            carType = @"SU";
            [_btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [_btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [_btnSUV setImage:[UIImage imageNamed:@"car_three_selected.png"] forState:UIControlStateNormal];
            [_btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
             [_btnRequestRide setImage:[UIImage imageNamed:@"thumb.png"] forState:UIControlStateNormal];
            break;
        case 3:

            carType = @"OT";
            [_btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [_btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [_btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [_btnOther setImage:[UIImage imageNamed:@"car_four_selected.png"] forState:UIControlStateNormal];
            [_btnRequestRide setImage:[UIImage imageNamed:@"thumb.png"] forState:UIControlStateNormal];
            break;
            
        case 4:
            
            rideRequested = @"OT" ;
            [_btnHatchBack setImage:[UIImage imageNamed:@"car_two_unselected.png"] forState:UIControlStateNormal];
            [_btnSedan setImage:[UIImage imageNamed:@"car_one_unselected.png"] forState:UIControlStateNormal];
            [_btnSUV setImage:[UIImage imageNamed:@"car_three_unselected.png"] forState:UIControlStateNormal];
            [_btnOther setImage:[UIImage imageNamed:@"car_four_unselected.png"] forState:UIControlStateNormal];
            [_btnRequestRide setImage:[UIImage imageNamed:@"thumb_press.png"] forState:UIControlStateNormal];
            break;

        default:
            break;
    }
    
    
}

#pragma Mark - Logout Action
- (IBAction)logOutAction:(id)sender {
    // Clearing Stored Credentials
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults removeObjectForKey:@"MobileNumber"];
    [defaults removeObjectForKey:@"Password"];
    [defaults setValue:@"" forKey:@"Password"];
    [defaults setValue:nil forKey:@"UserName"];
    [defaults setValue:nil forKey:@"MobileNumber"];
    [defaults setValue:nil forKey:@"Email"];
    [defaults setValue:nil forKey:@"OfcEmail"];
    [defaults setValue:nil forKey:@"CarDetails"];
    [defaults setValue:nil forKey:@"CarNumber"];
    [defaults setValue:nil forKey:@"Gender"];
    [defaults setValue:nil forKey:@"CarType"];
    [defaults setValue:nil forKey:@"OfficeEmailVerified"];
    [defaults setValue:nil forKey:@"MobileVerified"];
    [defaults setValue:nil forKey:@"UserID"];
    
    [defaults synchronize];
    
    
    
    AppStartVC *startView = [self.storyboard instantiateViewControllerWithIdentifier:@"AppStartVC"];
    startView.isUserLoggedOut = YES;
    
    [self.navigationController presentViewController:startView animated:YES completion:nil];
}

#pragma mark - Image Tap selection Action & Implementation
- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    [self.view makeToastActivity:CSToastPositionCenter];
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = imgUserPic.frame.size.height;
        CGRect imageViewFrame = CGRectInset(imgUserPic.bounds, 0.0, (CGRectGetHeight(imgUserPic.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:imgUserPic]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            
        }
       
    }
    
    
}
- (void)selectPhotos {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;

    // show device picture library
    picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self.view hideToastActivity];
    [self presentViewController:picker animated:YES completion:nil];
    
}
// Image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    imgUserPic.image = originalImage;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"newUserImage.png"];
    UIImage *image = originalImage; // imageView is my image from camera
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Gender Selection Implementation
- (IBAction)segmentAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        gender = @"M";
    }else{
        gender = @"F";
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:gender forKey:@"Gender" ];
    
    
}

#pragma mark - Edit Done Button Action & Implementation
- (void)profileEditDoneAction:(id)sender {
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    
    
    [self.view endEditing:YES];
    
    if ([_txtUserName.text isEqualToString:@""] || [_txtOfficeEmail.text isEqualToString:@""] || [_txtPersonamEmail.text isEqualToString:@""])
    {
       
        [self.view makeToast:@"Please enter information" duration:1 position:CSToastPositionBottom style:style];
        
    }
    else{
        if (![self.txtPersonamEmail.text isValidEmail])
        {
            [self.view makeToast:@"Please enter correct Personal Email ID." duration:1.5 position:CSToastPositionBottom style:style];
            return;
            
        }
        if (![self.txtOfficeEmail.text isValidEmail])
        {
            [self.view makeToast:@"Please enter correct Office Email ID." duration:1.5 position:CSToastPositionBottom style:style];
            return;
        }
        
        
        [self uploadEditedData];
    }
    
    
}
- (void) uploadEditedData {
    
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = [self buildParamDataDict:@"upload"];

    @try {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        //-- Convert string into URL

        NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
        
        [request setURL:[NSURL URLWithString:UPLOAD_DATA_URL]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        //-- Append data into posr url using following method
        NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params ];
        
        request.HTTPBody = httpBody;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
        {
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@",dataString);
            
            if (data.length > 0 && !connectionError)
            {
                
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                if (!(jsonArray.count == 0)) {
                    
                    for (NSDictionary *params in jsonArray)
                    {
                        [self saveJsonResponse:params];
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self updateFetchedData];
                        
                    });
                    
                    [self.view hideToastActivity];
                   
                    
                    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
                    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
                    [self presentViewController:navHome animated:YES completion:nil];
                }
                else{
                    
                        [self.view endEditing:YES];
                    [self.view hideToastActivity];
                    
                    CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
                    style.messageFont = [UIFont systemFontOfSize:12];
                    [self.view makeToast:@"There was an error while updating Profile" duration:1.0 position:CSToastPositionBottom style:style];
                }
            }
            else {
               
                [self.view hideToastActivity];
                
                CSToastStyle *style = [[CSToastStyle alloc]initWithDefaultStyle];
                style.messageFont = [UIFont systemFontOfSize:12];
                [self.view makeToast:@"Could not connect to server. Please try again" duration:1.0 position:CSToastPositionBottom style:style];
                
            }
            
        }];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [self.view hideToastActivity];
    }
    
    
}

- (NSDictionary *)buildParamDataDict:(NSString *)sender {
    
    if ([sender isEqualToString:@"verify"]) {
        officeEmailVerified = @"1";
    }else{
        officeEmailVerified = @"0";
    }
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"newUserImage.png"];
    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    NSData *imageData = UIImagePNGRepresentation(img);
    
    NSString *data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if (data == nil) {
        data = @"";
    }
    
    if (gender == nil) {
     
        if (self.optGender.selectedSegmentIndex == 0) {
            gender = @"M";
        }
        else{
            gender = @"F";
        }
        
        
    }
    
    
    fullName = _txtUserName.text;
    mobileNumber = _txtMobileNumber.text;
    personalEmail = _txtPersonamEmail.text;
    password = [[NSUserDefaults standardUserDefaults] valueForKey:@"Password"];
    officeEmail  = _txtOfficeEmail.text;
    loginType = @"C";
    faceBookID = @"0";
    fbFriends = @"0";
    appVersion = @"32";
    country = @"in";
    gcmRegID = @"";
    carDetails = _txtCarDetails.text;
    carNumber = _txtCarNumber.text;
    OS = @"I";
    
    
    NSDictionary *params = @{
                             @"image_url": data,
                             @"id":userID,
                             @"name":fullName,
                             @"gender":gender,
                             @"mobile":mobileNumber,
                             @"password":password,
                             @"email":personalEmail,
                             @"office_email":officeEmail,
                             @"login_type":loginType,
                             @"facebook_id":faceBookID,
                             @"user_app_version":appVersion,
                             @"fb_friends":fbFriends,
                             @"country":country,
                             @"gcm_reg_id":gcmRegID,
                             @"car_type":carType,
                             @"car_details":carDetails,
                             @"car_number":carNumber,
                             @"office_email_verified":officeEmailVerified,
                             @"os":OS
                             
                             };
    
    return params;
    
    
}
/***** Building API post call param body Method  *******/
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}



@end
