//
//  FindTableViewCellVC.h
//  Groupool
//
//  Created by HN on 04/04/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindTableViewCellVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *rideTime;
@property (strong, nonatomic) IBOutlet UILabel *rideGroupName;
@property (strong, nonatomic) IBOutlet UILabel *ridePrice;
@property (strong, nonatomic) IBOutlet UILabel *rideUserName;



@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnFull;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnSMS;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;





@property (strong, nonatomic) IBOutlet UIImageView *rideUserImage;



@end
