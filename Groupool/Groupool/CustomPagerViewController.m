//
//  CustomPageViewController.m
//  PageViewController
//
//  Created by Tom Fewster on 11/01/2012.
//

#import "CustomPagerViewController.h"
#import "MenuVC.h"
#import "UIColor+GroupoolColors.h"

@interface CustomPagerViewController ()<UINavigationBarDelegate>
{
    BOOL toggleMenu;
    BOOL toggleInfoView;
    
}

@end

@implementation CustomPagerViewController

- (void)viewDidLoad {
    // Do any additional setup after loading the view, typically from a nib.
    [super viewDidLoad];
    
    toggleInfoView = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [menuButton setImage:[UIImage imageNamed:@"ic_drawer.png"] forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake(3, 4, 3, 4);
    [menuButton addTarget:self action:@selector(navMenuAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"findRideView"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"offerRideView"]];
    [self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"feedView"]];
    
    self.title = @"Groupool";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationItem.leftBarButtonItem.title = @"";
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"toggleMenu"];
}

- (void) navMenuAction {
    
    MenuVC *menuVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"toggleMenu"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"toggleMenu"];
        
        UINavigationController *navSecondView = [[UINavigationController alloc] initWithRootViewController:menuVC];
        [self presentViewController:navSecondView animated:YES completion:nil];
        
    }
}
@end
