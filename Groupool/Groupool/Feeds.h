//
//  Feeds.h
//  
//
//  Created by Parth Pandya on 2/5/16.
//
//

#import <Foundation/Foundation.h>

@interface Feeds : NSObject
@property (strong, nonatomic) NSString *feedMessage;
@property (strong, nonatomic) NSString *feedTime;
@property (strong, nonatomic) NSString *feedColor;
@property (strong, nonatomic) NSString *feedType;
@property (strong, nonatomic) NSString *feedStartGrpID;
@property (strong, nonatomic) NSString *feedEndGrpID;
@property (nonatomic) NSInteger feedID;


@end
