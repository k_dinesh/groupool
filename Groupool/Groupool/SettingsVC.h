//
//  SettingsVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController

{
    BOOL isNewUserJoined;
    BOOL isRideRequested;
    BOOL isRideOffered;
    BOOL isSuggestionOrRequest;
}

@property (weak, nonatomic) IBOutlet UIButton *btnNewUserJoins;
@property (weak, nonatomic) IBOutlet UIButton *btnRideRequested;
@property (weak, nonatomic) IBOutlet UIButton *btnRideOffered;
@property (weak, nonatomic) IBOutlet UIButton *btnSuggestOrRequest;
@property (strong, nonatomic) NSString *userNotif;
@property (strong, nonatomic) NSString *rideRequestNotif;
@property (strong, nonatomic) NSString *rideOfferNotif;
@property (strong, nonatomic) NSString *autoSuggestNotif;


- (IBAction)newUserJoinNotifAction:(id)sender;
- (IBAction)rideRequestedNotifAction:(id)sender;
- (IBAction)rideOfferedNotifAction:(id)sender;
- (IBAction)suggestionOrRequestNotifAction:(id)sender;


@end
