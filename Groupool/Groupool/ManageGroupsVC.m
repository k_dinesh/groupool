//
//  ManageGroupsVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "ManageGroupsVC.h"
#import "CustomPagerViewController.h"
#import "UIColor+GroupoolColors.h"
#import "UIView+Toast.h"


@interface ManageGroupsVC ()<AddRouteDelegate>
{
    
    NSString *userID;

    
    UILabel *popupTitle ;
    UIView *separatorBar;
    UILabel *fromRouteName;
    UIImageView *locationIcon;
    UILabel *fromLocation;
    UIImageView *groupIcon;
    UILabel *numberOfMembers;
    UIImageView *rideIcon;
    UILabel *numberOfRides;
    UIView *separatorBar1;
    UILabel *toRouteName;
    UIImageView *locationIcon1;
    UILabel *toLocation;
    UIImageView *groupIcon1;
    UILabel *numberOfMembers1;
    UIImageView *rideIcon1;
    UILabel *numberOfRides1;
    
}
@end

@implementation ManageGroupsVC
@synthesize mDelegate,fromGroupsName,toGroupsName,routeIDArray,rdNameArray,rdLocationArray,rdMemberCountArray,rdRideCountArray,homeToOfficeArray,officeToHomeArray,repeatRouteIDArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Manage Routes";
 
    
    [self fetchData];
    
    self.myTableView.dataSource = self;
    self.myTableView.delegate  = self;
    self.myTableView.tableFooterView = [[UIView alloc] init];
    self.myTableView.showsVerticalScrollIndicator = NO;
    
    self.view.backgroundColor = [UIColor tableBackgroundColor];
    
    self.myTableView.backgroundColor = [UIColor tableBackgroundColor];
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
}


#pragma mark - Table View delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.myTableView])
    {
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:self.myTableView])
    {
        return [fromGroupsName count];
    }
    return 0; }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    [cell setBackgroundColor:[UIColor clearColor]];
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    @try {
        static NSString *cellIdentifier = @"Cell";
        //   /* Dynamic cell addition
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        UILabel *groupName = (UILabel *)[cell viewWithTag:100];
        UIButton *btnDelete = (UIButton *) [cell viewWithTag:200];
        
        
        groupName.text = [NSString stringWithFormat:@"%@ <-> %@",[fromGroupsName objectAtIndex:indexPath.row],[toGroupsName objectAtIndex:indexPath.row]];
        
        
        // Delete button cuatomization
        [btnDelete setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
        [btnDelete addTarget:self action:@selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [btnDelete setTag:indexPath.row];
        return cell;
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@",exception.description);
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Popup view

    [self.view makeToastActivity:CSToastPositionCenter];
    
    CGFloat viewHeight;
    CGFloat viewWidth;
    CGFloat viewX;
    CGFloat viewY;
    // popup view dimensions
    viewHeight = (self.view.frame.size.height/2) + (self.view.frame.size.height/6);
    viewWidth  = (self.view.frame.size.width/2) + (self.view.frame.size.width/3);
    viewX = (self.view.frame.size.width/12);
    viewY = (self.view.frame.size.height/6);
    
    self.customView = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
    
    self.customView.backgroundColor = [UIColor whiteColor];
    
    // popup View Shadow
    self.customView.layer.cornerRadius = 3;
    self.customView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.customView.layer.shadowOpacity = 0.8;
    self.customView.layer.shadowRadius = 12;
    self.customView.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    self.customView.tag = 100;
    
    //    CGFloat allControlYPos = (self.customView.frame.size.height) - (self.customView.frame.size.height);
    
    
    // Popup title
    
    popupTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 10, 220, 55)];
    popupTitle.textColor = [UIColor groupoolMainColor];
    popupTitle.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupTitle.numberOfLines = 3;
    popupTitle.lineBreakMode = NSLineBreakByWordWrapping;
    popupTitle.text = @"You will see rides from your following Groups and other nearby Groups.";
    
    // separator Bar 1
    
    separatorBar = [[UIView alloc] initWithFrame:CGRectMake(40, 68, 220, 1)];
    separatorBar.backgroundColor = [UIColor lightGrayColor];
    
    
    // From Route Name Label
    
    fromRouteName = [[UILabel alloc] initWithFrame:CGRectMake(40, 74, 170, 21)];
    fromRouteName.textColor = [UIColor grayColor];
    fromRouteName.font = [UIFont systemFontOfSize:13 weight:bold];
    
    fromRouteName.lineBreakMode = NSLineBreakByTruncatingTail;
    
    
    
    // Location Icon
    
    locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 100, 21, 21)];
    locationIcon.image = [UIImage imageNamed:@"ic_location.png"];
    
    // From Location
    
    fromLocation = [[UILabel alloc] initWithFrame:CGRectMake(40, 104, 170, 16)];
    fromLocation.font = [UIFont systemFontOfSize:12];
    fromLocation.textColor = [UIColor grayTextColor];
    fromLocation.lineBreakMode = NSLineBreakByTruncatingTail;
    fromLocation.numberOfLines = 1;
    
    // Group Icon
    
    groupIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 125, 21, 21)];
    groupIcon.image = [UIImage imageNamed:@"ic_group.png"];
    
    
    // Number of Group Members
    
    numberOfMembers = [[UILabel alloc] initWithFrame:CGRectMake(40, 129, 90, 16)];
    numberOfMembers.font = [UIFont systemFontOfSize:12];
    numberOfMembers.textColor = [UIColor grayTextColor];
    numberOfMembers.numberOfLines = 1;
    
    // Ride Icon
    
    rideIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 150, 24, 24)];
    rideIcon.image = [UIImage imageNamed:@"ic_cartype.png"];
    
    
    // Number of Rides
    
    numberOfRides = [[UILabel alloc] initWithFrame:CGRectMake(40, 155, 90, 16)];
    numberOfRides.font = [UIFont systemFontOfSize:12];
    numberOfRides.textColor = [UIColor grayTextColor];
    numberOfRides.numberOfLines = 1;
    
    
    // Separator Bar
    
    separatorBar1 = [[UIView alloc] initWithFrame:CGRectMake(40, 185, 200, 1)];
    separatorBar1.backgroundColor = [UIColor lightGrayColor];
    
    
    // To Route Name
    
    toRouteName = [[UILabel alloc] initWithFrame:CGRectMake(40, 200, 170, 21)];
    toRouteName.textColor = [UIColor grayColor];
    toRouteName.font = [UIFont systemFontOfSize:13 weight:bold];
    
    toRouteName.lineBreakMode = NSLineBreakByTruncatingTail;
    // Location Icon 1
    
    locationIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 225, 21, 21)];
    locationIcon1.image = [UIImage imageNamed:@"ic_location.png"];
    
    // To Location
    
    toLocation = [[UILabel alloc] initWithFrame:CGRectMake(40, 229, 170, 16)];
    toLocation.font = [UIFont systemFontOfSize:12];
    toLocation.textColor = [UIColor grayTextColor];
    toLocation.lineBreakMode = NSLineBreakByTruncatingTail;
    toLocation.numberOfLines = 1;
    
    // Group Icon1
    
    groupIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 250, 21, 21)];
    groupIcon1.image = [UIImage imageNamed:@"ic_group.png"];
    
    // Number of Members
    
    numberOfMembers1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 255, 90, 16)];
    numberOfMembers1.font = [UIFont systemFontOfSize:12];
    numberOfMembers1.textColor = [UIColor grayTextColor];
    numberOfMembers1.numberOfLines = 1;
    
    
    // Ride Icon 1
    
    rideIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 275, 24, 24)];
    rideIcon1.image = [UIImage imageNamed:@"ic_cartype.png"];
    
    // Number Of Rides 1
    
    numberOfRides1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 280, 90, 16)];
    numberOfRides1.font = [UIFont systemFontOfSize:12];
    numberOfRides1.textColor = [UIColor grayTextColor];
    numberOfRides1.numberOfLines = 1;
    
    
    // OK Button
    CGFloat okButtonX = (self.customView.frame.size.width) - (self.customView.frame.size.width/3) ;
    CGFloat okButtonY = (self.customView.frame.size.height) - (self.customView.frame.size.height / 9);
    
    
    UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(okButtonX, okButtonY, 70, 30)];
    
    btnOK.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnOK setTitle:@"OK" forState:UIControlStateNormal];
    [btnOK setTitleColor:[UIColor colorWithRed:81.00/255.00 green:165.00/255.00 blue:243.00/255.00 alpha:1] forState:UIControlStateNormal];
    
    
    [btnOK addTarget:self action:@selector(closePopup:)  forControlEvents:UIControlEventTouchUpInside];
    
    [self.customView addSubview:popupTitle];
    [self.customView addSubview:separatorBar];
    [self.customView addSubview:fromRouteName];
    [self.customView addSubview:locationIcon];
    [self.customView addSubview:fromLocation];
    [self.customView addSubview:groupIcon];
    [self.customView addSubview:numberOfMembers];
    [self.customView addSubview:rideIcon];
    [self.customView addSubview:numberOfRides];
    [self.customView addSubview:separatorBar1];
    [self.customView addSubview:toRouteName];
    [self.customView addSubview:locationIcon1];
    [self.customView addSubview:toLocation];
    [self.customView addSubview:groupIcon1];
    [self.customView addSubview:numberOfMembers1];
    [self.customView addSubview:rideIcon1];
    [self.customView addSubview:numberOfRides1];
    
    [self.customView addSubview:btnOK];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_GROUP_DETAILS_URL,[routeIDArray objectAtIndex:indexPath.row]]]
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    // handle response
                    
                    if (data.length > 0 && !error)
                    {
                        rdNameArray = [[NSMutableArray alloc] init];
                        rdLocationArray = [[NSMutableArray alloc] init];
                        rdMemberCountArray = [[NSMutableArray alloc] init];
                        rdRideCountArray = [[NSMutableArray alloc] init];
                        
                        NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                        
                        
                        
                        for (NSDictionary *routes in jsonArray) {
                            
                            [rdNameArray addObject:routes[@"name"]];
                            [rdLocationArray addObject:routes[@"location"]];
                            [rdMemberCountArray addObject:routes[@"group_member_count"]];
                            [rdRideCountArray addObject:routes[@"group_ride_count"]];
                            
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            fromRouteName.text = [NSString stringWithFormat:@"%@",rdNameArray[0]];
                            toRouteName.text = [NSString stringWithFormat:@"%@",rdNameArray[1]];
                            fromLocation.text = [NSString stringWithFormat:@"%@",rdLocationArray[0]];
                            toLocation.text = [NSString stringWithFormat:@"%@",rdLocationArray[1]];
                            numberOfRides.text = [NSString stringWithFormat:@"%@ Rides",rdRideCountArray[0]];
                            numberOfRides1.text = [NSString stringWithFormat:@"%@ Rides",rdRideCountArray[1]];
                            numberOfMembers.text = [NSString stringWithFormat:@"%@ Members",rdMemberCountArray[0]];
                            numberOfMembers1.text = [NSString stringWithFormat:@"%@ Members",rdMemberCountArray[1]];
                            

                            [self.view hideToastActivity];
                            
                            // Show custom view animated
                            self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                            [self.view addSubview:self.customView];
                            [UIView animateWithDuration:0.1
                                             animations:
                             ^{
                                 self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                                 self.myTableView.userInteractionEnabled = NO;
                                 self.navigationController.navigationBar.userInteractionEnabled = NO;
                                 self.addNewRouteButton.userInteractionEnabled = NO;
                                 
                             }];
                            
                        });
                        
                    }
                    else
                    {
                        NSLog(@"Connection could not be made");
                        
                    }
                    
                }] resume];
        
    });
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - Implementation
/************ Route Details popup close Implementation **************/
- (void) closePopup:(id)sender {
    
    
    
    self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.1
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
         self.myTableView.userInteractionEnabled = YES;
         self.navigationController.navigationBar.userInteractionEnabled = YES;
         self.addNewRouteButton.userInteractionEnabled = YES;
         
     }
     ];
    
    [self.customView removeFromSuperview];
    
}

//************* Navigation bar back button action method ***************
- (void) backButtonAction {
    [self.mDelegate refreshDropdown:self];
    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
    [self.navigationController presentViewController:navHome animated:YES completion:nil];
    
}
//************* Get data from API method ********************
- (void)fetchData {
    
   
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    @try {
        {
            userID = [defaults valueForKey:@"UserID"];
            
            if (userID == nil || [userID isEqualToString:@""]) {
                return;
                
            }
            
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_ROUTE_URL,userID]];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response,
                                                       NSData *data, NSError *connectionError)
             {
                 if (data.length > 0 && connectionError == nil)
                 {
                     
                     
                     NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                     
                     fromGroupsName = [[NSMutableArray alloc] init];
                     toGroupsName = [[NSMutableArray alloc] init];
                     routeIDArray = [[NSMutableArray alloc]init];
                     
                     
                     for (NSDictionary *routes in jsonArray) {
                         
                         [fromGroupsName addObject:routes[@"from_group_name"]];
                         [toGroupsName addObject:routes[@"to_group_name"]];
                         [routeIDArray addObject:routes[@"id"]];
                         
                         self.myTableView.hidden = FALSE;
                     }
                     homeToOfficeArray = [[NSMutableArray alloc] init];
                     officeToHomeArray = [[NSMutableArray alloc] init];
                     repeatRouteIDArray = [[NSMutableArray alloc] init];
                     
                     homeToOfficeArray = [NSMutableArray arrayWithArray:fromGroupsName];
                     [homeToOfficeArray addObjectsFromArray:toGroupsName];
                     
                     officeToHomeArray = [NSMutableArray arrayWithArray:toGroupsName];
                     [officeToHomeArray addObjectsFromArray:fromGroupsName];
                     
                     repeatRouteIDArray = [NSMutableArray arrayWithArray:routeIDArray];
                     [repeatRouteIDArray addObjectsFromArray:routeIDArray];

                     [self.myTableView reloadData];
                     [self.view hideToastActivity];
                 }
                 else
                 {
                     
                     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                         
                         [self reconnectToServer];
                         
                     }];
                     [alert addAction:alertAction];
                     [self.view hideToastActivity];
                     [self presentViewController:alert animated:YES completion:nil];
                     
                 }
             }];
        }
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"Manage Route Exception:%@",exception);
    }
    
    
}

- (void)reconnectToServer{
    [self fetchData];
}
//************* Table cell Delete button action method
- (void)deletePressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    
    if ((fromGroupsName.count == 1) && (toGroupsName.count == 1) )
    {
        // Atleast one Route required.
        UIAlertController *alert =  [UIAlertController alertControllerWithTitle:@"" message:@"At least one Route required." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        UIAlertController *alert =  [UIAlertController alertControllerWithTitle:@"" message:@"Sure want to Delete the Route?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [self.view makeToastActivity:CSToastPositionCenter];
            
            
            NSString *fullAddressURL = [NSString stringWithFormat:@"%@/%@",DELETE_USER_ROUTE_URL,[routeIDArray objectAtIndex:button.tag]];
            fullAddressURL = [fullAddressURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSURL *url = [NSURL URLWithString:fullAddressURL];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response,
                                                       NSData *data, NSError *connectionError)
             {
                 if ((data.length > 0) && (connectionError == nil))
                 {
                     //                     NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                     
                     [fromGroupsName removeObjectAtIndex:button.tag];
                     [toGroupsName removeObjectAtIndex:button.tag];
                     [routeIDArray removeObjectAtIndex:button.tag];
                     [self refreshTable];
                     
                     [self.view hideToastActivity];
                 }
                 else
                 {
                     
                     [self.view hideToastActivity];
                     
                 }
             }];
            
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)refreshTable {
    
    [self.myTableView reloadData];
    NSIndexSet * sections = [NSIndexSet indexSetWithIndex:0];
    [self.myTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    
    BOOL isRideDataEmpty = ([fromGroupsName count] == 0);
    
    
    if (isRideDataEmpty) {
        
        self.myTableView.hidden = YES;
    }
    else{
        self.myTableView.hidden = NO;
    }
    
}



//************* Refresh Table Data Delegate  method
- (void)refreshTableData:(AddRouteVC *)sender {
    [fromGroupsName removeAllObjects];
    [toGroupsName removeAllObjects];
    [routeIDArray removeAllObjects];
    [self.myTableView reloadData];
    NSIndexSet * sections = [NSIndexSet indexSetWithIndex:0];
    [self.myTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    
    [self fetchData];
    
    
}


#pragma mark- Actions
//************* Add New Route button action
- (IBAction)addNewRouteAction:(id)sender {
    
    
    AddRouteVC *addRoute =[self.storyboard instantiateViewControllerWithIdentifier:@"AddRouteVC"];
    addRoute.delegate = self;
    UINavigationController *navRoute = [[UINavigationController alloc] initWithRootViewController:addRoute];
    //    [self.navigationController pushViewController: navRoute animated:YES];
    [self.navigationController presentViewController:navRoute animated:YES completion:nil];
    
    
    
}
- (IBAction)backToLanding:(id)sender {
    
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
    [self.navigationController pushViewController:homeView animated:YES];
}
@end
