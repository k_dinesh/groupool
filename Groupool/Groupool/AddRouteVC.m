//
//  AddRouteVC.m
//  Groupool
//
//  Created by HN on 03/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "AddRouteVC.h"
#import "MyRideVC.h"
#import "UIColor+GroupoolColors.h"
#import "CustomPagerViewController.h"
#import "ManageGroupsVC.h"
#import "UIView+Toast.h"

#define kGoogleAPIKey @"AIzaSyBMnrz-MCJH-UC7HY4T3SL4W-iz7XS920A"

@interface AddRouteVC ()
{
    CLLocationCoordinate2D fromLocationCoord;
    CLLocationCoordinate2D toLocationCoord;
    NSString *fromLocationString;
    NSString *toLocationString;
    NSString *fromLocationString1;
    NSString *toLocationString1;
    
    bool isMapVisible;
    BOOL toggleOn;
    GMSMapView *googelMap;
    NSString *userID;
    NSString *address;
    
}
@end
@implementation AddRouteVC
@synthesize mapViewBackground = _mapViewBackground;
@synthesize delegate;
@synthesize isUserNew,rdLocationArray,rdMemberCountArray,rdNameArray,rdRideCountArray,routeMap;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textField.delegate = self;
    _homeAddressText.placeSearchDelegate = self;
    _officeAddressText.placeSearchDelegate = self;
    _homeAddressText.strApiKey = kGoogleAPIKey;
    _officeAddressText.strApiKey = kGoogleAPIKey;
    _homeAddressText.superViewOfList = self.view;
    _officeAddressText.superViewOfList = self.view;
    _homeAddressText.autoCompleteShouldHideOnSelection = YES;
    _officeAddressText.autoCompleteShouldHideOnSelection = YES;
    _homeAddressText.maximumNumberOfAutoCompleteRows  = 10;
    _officeAddressText.maximumNumberOfAutoCompleteRows = 10;
    
    toggleOn  = NO;
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    
     {
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
        [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        
    }
    
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    self.title = @"New Route";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(newRouteAdded)];
    
    self.navigationItem.rightBarButtonItem = doneButton;
    
    isMapVisible = false;
    
   

}



#pragma mark - Place search Textfield Delegates


-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.view endEditing:YES];
//    NSLog(@"SELECTED ADDRESS :%@",responseDict);
//    NSLog(@"Co-ordinate:(%f,%f)",responseDict.coordinate.latitude,responseDict.coordinate.longitude);
//    NSLog(@"Address:%@",responseDict.formattedAddress);
    if (textField == _homeAddressText) {
        fromLocationCoord.latitude = responseDict.coordinate.latitude;
        fromLocationCoord.longitude = responseDict.coordinate.longitude;
//        fromLocationString = responseDict.formattedAddress;
        fromLocationString1 = _homeAddressText.text;
    
    }
    if (textField == _officeAddressText) {
        toLocationCoord.latitude = responseDict.coordinate.latitude;
        toLocationCoord.longitude = responseDict.coordinate.longitude;
//        toLocationString = responseDict.formattedAddress;
        toLocationString1  = _officeAddressText.text;
    }
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - UITextfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [_homeAddressText resignFirstResponder];
    [_officeAddressText resignFirstResponder];
    
}

#pragma mark - Implementation
//***************** Navigation bar New Route Added action method ************
- (void) newRouteAdded {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    fromLocationString = _homeAddressText.text;
    toLocationString = _officeAddressText.text;
    
    
    if (([fromLocationString isEqualToString:@""] || !(toLocationString))||(!(fromLocationString) || [toLocationString isEqualToString:@""])
        ||(!([fromLocationString isEqualToString:fromLocationString1])) || (!([toLocationString isEqualToString:toLocationString1])))
    {
        
        [self.view hideToastActivity];
        
        
        [self.view makeToast:@"Please enter locations" duration:2 position:CSToastPositionBottom style:style];

        [googelMap removeFromSuperview];
        
        
    }else
    {
        if (![self validateAddress:_homeAddressText.text])
        {
            [self.view hideToastActivity];
            [self.view makeToast:@"Please enter correct Home Address" duration:2 position:CSToastPositionBottom style:style];
            return;
            
        }
        if (![self validateAddress:_officeAddressText.text])
        {
            [self.view hideToastActivity];
            [self.view makeToast:@"Please enter correct Office Address" duration:2 position:CSToastPositionBottom style:style];
            return;
            
        }
        
        
//        toLocationString = @"Gururaj 1st Society Rd, Gururaj Society, Kothrud, Pune, Maharashtra, India";
//        fromLocationString = @"Accenture Hinjewadi, Pune, Maharashtra, India";
//        NSString *fullAddressURL = [NSString stringWithFormat:@"http://www.groupool.in/service/index.php?/UploadDownload_groupool/createUsersRouteViaLoc/%@/%@/%@",fromLocationString,toLocationString,userID];
        
        fromLocationString = [fromLocationString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        toLocationString = [toLocationString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        userID = [userID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        
        NSString *dataString = [NSString stringWithFormat:@"%@/%@/%@",fromLocationString,toLocationString,userID];
       
        NSString *fullAddressURL = [NSString stringWithFormat:@"%@/%@",ADD_NEW_ROUTE_URL,dataString];
        
        
        NSURL *url = [NSURL URLWithString:fullAddressURL];
        
        NSLog(@"%@",url);
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if ((data.length > 0) && (data != nil) && (connectionError == nil))
             {
                 NSString *dataString  = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          
                 
                 if ([dataString isEqualToString:@"2"]) {
                     
                     for (UIView *view in [self.view subviews])
                     {
                         if ([view isKindOfClass:[UITextField class]])
                         {
                             UITextField *textField = (UITextField *)view;
                             textField.text = @"";
                         }
                     }
                     
                     [self.view hideToastActivity];
                     
                     [self.view makeToast:@"Route already exists" duration:2 position:CSToastPositionBottom style:style];
                     
                     
                 }
                 else
                 {
                     
                     rdNameArray = [[NSMutableArray alloc] init];
                     rdLocationArray = [[NSMutableArray alloc] init];
                     rdMemberCountArray = [[NSMutableArray alloc] init];
                     rdRideCountArray = [[NSMutableArray alloc] init];
                     
                     NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                     if (!(jsonArray.count == 0))
                     {
                        
                         for (NSDictionary *routes in jsonArray) {
                             
                             [rdNameArray addObject:routes[@"name"]];
                             [rdLocationArray addObject:routes[@"location"]];
                             [rdMemberCountArray addObject:routes[@"group_member_count"]];
                             [rdRideCountArray addObject:routes[@"group_ride_count"]];
                             
                         }
                         
                         UILabel *popupTitle ;
                         UIView *separatorBar;
                         UILabel *fromRouteName;
                         UIImageView *locationIcon;
                         UILabel *fromLocation;
                         UIImageView *groupIcon;
                         UILabel *numberOfMembers;
                         UIImageView *rideIcon;
                         UILabel *numberOfRides;
                         UIView *separatorBar1;
                         UILabel *toRouteName;
                         UIImageView *locationIcon1;
                         UILabel *toLocation;
                         UIImageView *groupIcon1;
                         UILabel *numberOfMembers1;
                         UIImageView *rideIcon1;
                         UILabel *numberOfRides1;
                         CGFloat viewHeight;
                         CGFloat viewWidth;
                         CGFloat viewX;
                         CGFloat viewY;
                         
                         // popup view dimensions
                         viewHeight = (self.view.frame.size.height/2) + (self.view.frame.size.height/6);
                         viewWidth  = (self.view.frame.size.width/2) + (self.view.frame.size.width/3);
                         viewX = (self.view.frame.size.width/12);
                         viewY = (self.view.frame.size.height/6);
                         
                         self.customView = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
                         
                         self.customView.backgroundColor = [UIColor whiteColor];
                         
                         // popup View Shadow
                         self.customView.layer.cornerRadius = 3;
                         self.customView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
                         self.customView.layer.shadowOpacity = 0.8;
                         self.customView.layer.shadowRadius = 12;
                         self.customView.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
                         self.customView.tag = 100;
                         
                         //    CGFloat allControlYPos = (self.customView.frame.size.height) - (self.customView.frame.size.height);
                         
                         
                         // Popup title
                         
                         popupTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 25, 220, 40)];
                         popupTitle.textColor = [UIColor groupoolMainColor];
                         popupTitle.font = [UIFont systemFontOfSize:12];
                         popupTitle.numberOfLines = 4;
                         popupTitle.lineBreakMode = NSLineBreakByWordWrapping;
                         popupTitle.text = @"You will see rides from your Groups and other nearby Groups in ~2 km vicinity. Happy Carpooling!";
                         
                         // separator Bar 1
                         
                         separatorBar = [[UIView alloc] initWithFrame:CGRectMake(25, 68, 220, 1)];
                         separatorBar.backgroundColor = [UIColor lightGrayColor];
                         
                         
                         // From Route Name Label
                         
                         fromRouteName = [[UILabel alloc] initWithFrame:CGRectMake(40, 74, 170, 21)];
                         fromRouteName.textColor = [UIColor grayTextColor];
                         fromRouteName.font = [UIFont systemFontOfSize:13 weight:bold];
                         
                         fromRouteName.lineBreakMode = NSLineBreakByTruncatingTail;
                         
                         
                         
                         // Location Icon
                         
                         locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 100, 21, 21)];
                         locationIcon.image = [UIImage imageNamed:@"ic_location.png"];
                         
                         // From Location
                         
                         fromLocation = [[UILabel alloc] initWithFrame:CGRectMake(40, 104, 170, 16)];
                         fromLocation.font = [UIFont systemFontOfSize:12];
                         fromLocation.textColor = [UIColor grayTextColor];
                         fromLocation.lineBreakMode = NSLineBreakByTruncatingTail;
                         fromLocation.numberOfLines = 1;
                         
                         // Group Icon
                         
                         groupIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 125, 21, 21)];
                         groupIcon.image = [UIImage imageNamed:@"ic_group.png"];
                         
                         
                         // Number of Group Members
                         
                         numberOfMembers = [[UILabel alloc] initWithFrame:CGRectMake(40, 129, 90, 16)];
                         numberOfMembers.font = [UIFont systemFontOfSize:12];
                         numberOfMembers.textColor = [UIColor grayTextColor];
                         numberOfMembers.numberOfLines = 1;
                         
                         // Ride Icon
                         
                         rideIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 150, 24, 24)];
                         rideIcon.image = [UIImage imageNamed:@"ic_cartype.png"];
                         
                         
                         // Number of Rides
                         
                         numberOfRides = [[UILabel alloc] initWithFrame:CGRectMake(40, 155, 90, 16)];
                         numberOfRides.font = [UIFont systemFontOfSize:12];
                         numberOfRides.textColor = [UIColor grayTextColor];
                         numberOfRides.numberOfLines = 1;
                         
                         
                         // Separator Bar
                         
                         separatorBar1 = [[UIView alloc] initWithFrame:CGRectMake(40, 185, 200, 1)];
                         separatorBar1.backgroundColor = [UIColor lightGrayColor];
                         
                         /*
                          UILabel *toRouteName;
                          UIImageView *locationIcon1;
                          UILabel *toLocation;
                          UIImageView *groupIcon1;
                          UILabel *numberOfMembers1;
                          UIImageView *rideIcon1;
                          UILabel *numberOfRides1;
                          */
                         
                         // To Route Name
                         
                         toRouteName = [[UILabel alloc] initWithFrame:CGRectMake(40, 200, 170, 21)];
                         toRouteName.textColor = [UIColor grayColor];
                         toRouteName.font = [UIFont systemFontOfSize:13 weight:bold];
                         
                         toRouteName.lineBreakMode = NSLineBreakByTruncatingTail;
                         // Location Icon 1
                         
                         locationIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 225, 21, 21)];
                         locationIcon1.image = [UIImage imageNamed:@"ic_location.png"];
                         
                         // To Location
                         
                         toLocation = [[UILabel alloc] initWithFrame:CGRectMake(40, 229, 170, 16)];
                         toLocation.font = [UIFont systemFontOfSize:12];
                         toLocation.textColor = [UIColor grayTextColor];
                         toLocation.lineBreakMode = NSLineBreakByTruncatingTail;
                         toLocation.numberOfLines = 1;
                         
                         // Group Icon1
                         
                         groupIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 250, 21, 21)];
                         groupIcon1.image = [UIImage imageNamed:@"ic_group.png"];
                         
                         // Number of Members
                         
                         numberOfMembers1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 255, 90, 16)];
                         numberOfMembers1.font = [UIFont systemFontOfSize:12];
                         numberOfMembers1.textColor = [UIColor grayTextColor];
                         numberOfMembers1.numberOfLines = 1;
                         
                         
                         // Ride Icon 1
                         
                         rideIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 275, 24, 24)];
                         rideIcon1.image = [UIImage imageNamed:@"ic_cartype.png"];
                         
                         // Number Of Rides 1
                         
                         numberOfRides1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 280, 90, 16)];
                         numberOfRides1.font = [UIFont systemFontOfSize:12];
                         numberOfRides1.textColor = [UIColor grayTextColor];
                         numberOfRides1.numberOfLines = 1;
                         
                         
                         // OK Button
                         CGFloat okButtonX = (self.customView.frame.size.width) - (self.customView.frame.size.width/3) ;
                         CGFloat okButtonY = (self.customView.frame.size.height) - (self.customView.frame.size.height / 9);
                         
                         
                         UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(okButtonX, okButtonY, 70, 30)];
                         
                         btnOK.titleLabel.font = [UIFont systemFontOfSize:15];
                         [btnOK setTitle:@"OK" forState:UIControlStateNormal];
                         [btnOK setTitleColor:[UIColor colorWithRed:81.00/255.00 green:165.00/255.00 blue:243.00/255.00 alpha:1] forState:UIControlStateNormal];
                         
                         
                         [btnOK addTarget:self action:@selector(closePopup:)  forControlEvents:UIControlEventTouchUpInside];
                         
                         [self.customView addSubview:popupTitle];
                         [self.customView addSubview:separatorBar];
                         [self.customView addSubview:fromRouteName];
                         [self.customView addSubview:locationIcon];
                         [self.customView addSubview:fromLocation];
                         [self.customView addSubview:groupIcon];
                         [self.customView addSubview:numberOfMembers];
                         [self.customView addSubview:rideIcon];
                         [self.customView addSubview:numberOfRides];
                         [self.customView addSubview:separatorBar1];
                         [self.customView addSubview:toRouteName];
                         [self.customView addSubview:locationIcon1];
                         [self.customView addSubview:toLocation];
                         [self.customView addSubview:groupIcon1];
                         [self.customView addSubview:numberOfMembers1];
                         [self.customView addSubview:rideIcon1];
                         [self.customView addSubview:numberOfRides1];
                         
                         [self.customView addSubview:btnOK];
                         
                         
                         fromRouteName.text = [NSString stringWithFormat:@"%@",rdNameArray[0]];
                         toRouteName.text = [NSString stringWithFormat:@"%@",rdNameArray[1]];
                         fromLocation.text = [NSString stringWithFormat:@"%@",rdLocationArray[0]];
                         toLocation.text = [NSString stringWithFormat:@"%@",rdLocationArray[1]];
                         numberOfRides.text = [NSString stringWithFormat:@"%@ Rides",rdRideCountArray[0]];
                         numberOfRides1.text = [NSString stringWithFormat:@"%@ Rides",rdRideCountArray[1]];
                         numberOfMembers.text = [NSString stringWithFormat:@"%@ Members",rdMemberCountArray[0]];
                         numberOfMembers1.text = [NSString stringWithFormat:@"%@ Members",rdMemberCountArray[1]];
                         
                         
                         [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"newRootNotAdded"];
                         
                         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                         [self.view addSubview:self.customView];
                         [UIView animateWithDuration:0.1
                                          animations:
                          ^{
                              self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                              
                              
                          }];
                         
                         
                     }
                     else{
                         
                         for (UIView *view in [self.view subviews])
                         {
                             if ([view isKindOfClass:[UITextField class]])
                             {
                                 UITextField *textField = (UITextField *)view;
                                 textField.text = @"";
                             }
                         }
                         
                         [self.view hideToastActivity];
                         
                         [self.view makeToast:@"There was a problem connecting to the server" duration:2 position:CSToastPositionBottom style:style];
                         
                     }
                     
                 }
                 
             }
             else
             {
                 [self.view hideToastActivity];
                 
                 [self.view makeToast:@"Connection could not be made" duration:2 position:CSToastPositionBottom style:style];
                 
                 
             }
         }];
        
        
        
    }
    
}

- (BOOL)validateAddress:(NSString*)addressString {
    
    
    @try {
       
        
        NSString *esc_addr =  [addressString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        NSError *jsonError;
        NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:objectData options:-1 error:nil];
        
        NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
        if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
        {
            //        NSString *state;
            NSString *city;
            
            for (NSDictionary *dictAddress in addressArray)
            {
                //            if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"administrative_area_level_1"])
                //                {
                //                    state = [dictAddress objectForKey:@"long_name"];
                //                     NSLog(@"state :%@",state);
                //
                //                }
                
                if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"country"]) {
                    city = [dictAddress objectForKey:@"long_name"];
               
                    
                    if ([city isEqualToString:@"India"] ) {
                        return YES;
                    }
                    else{
                        return NO;
                    }
                }
                
            }
        }
        
        return NO;
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
   
}

//***************** Navigation bar Back Button action method
- (void) backButtonAction {
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"newRootNotAdded"] isEqualToString:@"Yes"]) {
        ManageGroupsVC *manageRoute = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageGroupsVC"];
        [self.navigationController pushViewController:manageRoute animated:YES];
    }
    else{
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        
        [self.view makeToast:@"Minimum one route must be added." duration:2 position:CSToastPositionBottom style:style];
    }
    
}

//***************** Show and Hide map button action
- (IBAction)showMap:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([_homeAddressText.text isEqualToString:@""] && [_officeAddressText.text isEqualToString:@""]) {
        [googelMap clear];
        
    }
    
        if (([fromLocationString1 isEqualToString:@""] || !(toLocationString1))||(!(fromLocationString1) || [toLocationString1 isEqualToString:@""] ))
        {
                           [googelMap removeFromSuperview];
            
            CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];

            style.messageFont = [UIFont systemFontOfSize:12];
            
            [self.view makeToast:@"Please enter locations" duration:2 position:CSToastPositionBottom style:style];
        }
        else{
            if (!toggleOn) {
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:fromLocationCoord.latitude
                                                                        longitude:fromLocationCoord.longitude
                                                                             zoom:12];
                //    CGFloat xPos = (((self.view.frame.size.width)-((self.view.frame.size.width)-10)));
                CGFloat xPos = 0;
                CGFloat yPos = ((self.view.frame.size.height/2)+(self.view.frame.size.height/30));
                //    CGFloat mapWidth = ((self.view.frame.size.width)-(self.view.frame.size.width/8));
                CGFloat mapWidth = self.view.frame.size.width;
                CGFloat mapHeight = ((self.view.frame.size.height/2)-(self.view.frame.size.height/16));
                
                CGRect mapFrame = CGRectMake(xPos,yPos, mapWidth , mapHeight);
                
                googelMap = [GMSMapView mapWithFrame:mapFrame camera:camera];
                googelMap.delegate = self;
                googelMap.camera = camera;
                googelMap.myLocationEnabled = YES;
                
                [self.view addSubview:googelMap];
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                
                marker.position=CLLocationCoordinate2DMake(toLocationCoord.latitude, toLocationCoord.longitude);
                marker.title=@"Destination"; marker.snippet=toLocationString;
                marker.map = googelMap;
                
                GMSMarker *marker2 = [[GMSMarker alloc] init];
                marker2.position=CLLocationCoordinate2DMake(fromLocationCoord.latitude, fromLocationCoord.longitude);
                marker2.title=@"Source"; marker2.snippet=fromLocationString;
                marker2.map = googelMap;
                _mapViewBackground.hidden= NO;
                [self.view addSubview:_mapViewBackground];
                
                CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake(fromLocationCoord.latitude, fromLocationCoord.longitude);
                CLLocationCoordinate2D coord2 = CLLocationCoordinate2DMake(toLocationCoord.latitude, toLocationCoord.longitude);
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord1 coordinate:coord2];
                
                [googelMap moveCamera:[GMSCameraUpdate fitBounds:bounds]];
                
            }else
            {
                [googelMap removeFromSuperview];
            }
            toggleOn = !toggleOn;
            [_showMap setTitle:toggleOn ? @"Hide Map":@"Show Map" forState:UIControlStateNormal];
        }
    
    
    
}

- (void) closePopup:(id)sender {
    
    
    
    self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.1
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
         
     }
     ];
    
    [self.customView removeFromSuperview];
    
    ManageGroupsVC *manageRoute = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageGroupsVC"];
    
    [self.delegate refreshTableData:self];
    [self.navigationController pushViewController:manageRoute animated:YES];
}

@end
