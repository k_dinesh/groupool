//
//  SignUpVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetails.h"



@interface SignUpVC : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtFullName;
@property (strong, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtPersonalEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UISegmentedControl *optGender;
@property (strong, nonatomic) IBOutlet UITextField *txtOfficeEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;


@property (strong,nonatomic) UserDetails *user;

-(IBAction)signuUpAction:(id)sender;




@end
