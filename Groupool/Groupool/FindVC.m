//
//  FindVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "FindVC.h"
#import <QuartzCore/QuartzCore.h>
#import "RequestRideVC.h"
#import "ActionSheetStringPicker.h"
#import "UIColor+GroupoolColors.h"
#import "ActionSheetDatePicker.h"
#import "ProfileVC.h"
#import "UIView+Toast.h"

static NSString *TableViewCellIdentifier = @"FindCells";

@interface FindVC ()
{
    NSInteger cellID;
    NSString *deleteOfferedRideURL;
    NSDateFormatter *dateFormatter;
    
}

@end

@implementation FindVC
@synthesize OfferedRideData,rideDate,startGroupID,endGroupID;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [self fetchData]; // Get Data from API
    
    self.title = @"Find";
    
    self.myTableView.hidden = YES;
    isCalled = NO;
    
    _requestRide.layer.cornerRadius= 5;
    
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.showsVerticalScrollIndicator = NO;
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self
                            action:@selector(fetchData)
                  forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.refreshControl];
    
//    NSString *trimmed = [self.selectDay.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *trimmed = [self.lblDaySelection.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([trimmed isEqualToString:@"Today"]) {
        rideDay = @"0";
    }else{
        rideDay = @"1";
    }
    
    UIButton *btnRequest = [[UIButton alloc] initWithFrame:CGRectMake(14,(self.view.frame.size.height/2) + (self.view.frame.size.height/6),70,42)];
    
    btnRequest.tintColor = [UIColor whiteColor];
    btnRequest.backgroundColor = [UIColor groupoolMainColor];
    btnRequest.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnRequest.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnRequest setTitle:@"REQUEST RIDE" forState:UIControlStateNormal];
    btnRequest.layer.cornerRadius = 5;
    btnRequest.titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
    //    btnRequest.titleLabel.font = [UIFont fontWithName:nil size:12];
    btnRequest.layer.masksToBounds = NO;
    
    
    
    btnRequest.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    btnRequest.layer.shadowOpacity = 0.8;
    btnRequest.layer.shadowRadius = 12;
    btnRequest.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    
    [self.view addSubview:btnRequest];
    
    [btnRequest addTarget:self
                   action:@selector(rideRequested)forControlEvents:UIControlEventTouchUpInside];
    
    
    rideDayArray = [NSArray arrayWithObjects:@"Today",@"Tomorrow", nil];
    [self getOfferRidesAll:rideDay]; // Get Offered Rides Data from API
  
    self.myTableView.tableFooterView = [[UIView alloc] init];
    
    
    dateFormatter = [[NSDateFormatter alloc] init];
    
    
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isRideTimeChanged"] isEqualToString:@"YES"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isRideTimeChanged"];
        [self fetchData];
    }
    
}



#pragma mark - Load/Reload Data from API
-(void)fetchData {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void){ @try {
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_ROUTE_URL,userID]];
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
        {
            if (data == NULL) {
                
                CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                style.messageFont = [UIFont systemFontOfSize:12];
                style.messageAlignment = NSTextAlignmentCenter;
                style.cornerRadius = 12;
                [self.view makeToast:@"Server connection error" duration:3 position:CSToastPositionBottom style:style]
                ;
                
                
            }
            
            if (data.length > 0 && connectionError == nil)
            {
                NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                fromGroupsNameArray = [[NSMutableArray alloc] init];
                toGroupsNameArray = [[NSMutableArray alloc] init];
                fromGroupIDArray = [[NSMutableArray alloc] init];
                toGroupIDArray = [[NSMutableArray alloc] init];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    for (NSDictionary *routes in jsonArray) {
                        
                        [fromGroupsNameArray addObject:routes[@"from_group_name"]];
                        [toGroupsNameArray addObject:routes[@"to_group_name"]];
                        [fromGroupIDArray addObject:routes[@"from_group_id"]];
                        [toGroupIDArray addObject:routes[@"to_group_id"]];
                        
                        
                    }
                    
                    [self checkforUpdate];
                    
                    
                    if (self.refreshControl) {
                        [self.refreshControl endRefreshing];
                    }
                    [self.view hideToastActivity];
                });
                
            }
            else
            {
                
                [self.view hideToastActivity];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    
                    [self reFetchData];
                    
                }];
                [alert addAction:alertAction];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }];
        
    }
        @catch (NSException *exception) {
            [self.view hideToastActivity];
            NSLog(@"%@",exception.description);
            
            
        }
    });
    
    
}
- (void)reFetchData {
    
    if (!isCalled) {
        [self fetchData];
        isCalled = YES;
    }
    
}
-(void)checkforUpdate {
    
    BOOL isRideDataEmpty = ([OfferedRideData count] == 0);
    
    if (!isRideDataEmpty) {
        self.InviteFriendsBtn.hidden = YES;
        self.NoRideLabel.hidden = YES;
        self.noRideLabel1.hidden = YES;
        self.myTableView.hidden = NO;
        
        [self.myTableView reloadData];
        
       
        
    }else{
        self.myTableView.hidden = YES;
        self.InviteFriendsBtn.hidden = NO;
        self.NoRideLabel.hidden = NO;
        self.noRideLabel1.hidden = NO;
    }
    
}

#pragma mark - TableView delegate methods and Implementation
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.myTableView])
    {
        return 1;
    }
    return 0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [OfferedRideData count];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:indexPath.row];
    
    if ([userID isEqualToString:obj.userID])
    {
        return 124;
    }
    return 95;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:indexPath.row];
    FindTableViewCellVC *Cell = [[FindTableViewCellVC alloc] init];
    
    if ([userID isEqualToString:obj.userID])
    {
        Cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
    }
    else
    {
        Cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
        
    }
    
    // Group Names
    Cell.rideGroupName.text = [NSString stringWithFormat:@"%@ > %@",obj.startGroupName,obj.endGroupName];
    
    // User Name
    Cell.rideUserName.text = obj.name;
    NSArray *nameArray = [obj.name componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    Cell.rideUserName.text = nameArray[0];
    if ([Cell.rideUserName.text isEqualToString:@""]) {
        Cell.rideUserName.text = [NSString stringWithFormat:@"%@ (%@)",obj.name,obj.company];
    }
    else
    {
        Cell.rideUserName.text = [NSString stringWithFormat:@"%@ (%@)",Cell.rideUserName.text,obj.company];
    }
    
    // Ride Time
    NSString *timeString = obj.time;
    
    NSScanner *timescanner = [NSScanner scannerWithString:timeString];
    int hour, minutes;
    [timescanner scanInt:&hour];
    [timescanner scanString:@":" intoString:nil];
    [timescanner scanInt:&minutes];
    timeString = [NSString stringWithFormat:@"%02d:%02d",hour,minutes];
    Cell.rideTime.text = timeString;
    
    if ([obj.isFull isEqualToString:@"1"]) {
        Cell.rideTime.text = @"FULL";
        Cell.rideTime.textColor = [UIColor lightGrayColor];
        Cell.btnEdit.hidden = YES;
        Cell.btnFull.hidden = YES;
        
    }
    else{
        Cell.rideTime.text = timeString;
        Cell.rideTime.textColor = [UIColor groupoolMainColor];
        Cell.btnEdit.hidden = NO;
        Cell.btnFull.hidden = NO;
    }
    
    
    // Ride Price
    Cell.ridePrice.text = [NSString stringWithFormat:@"Rs.%@",obj.price];
    
    // User Image
    
    Cell.rideUserImage.layer.cornerRadius = 24;
    Cell.rideUserImage.layer.masksToBounds = YES;
    Cell.rideUserImage.contentMode = UIViewContentModeScaleAspectFill;
    
    Cell.tag = indexPath.row;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSURL *imageURL = [NSURL URLWithString:obj.profilePic];
        NSData *imageData = [NSData dataWithContentsOfURL: imageURL];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (Cell.tag == indexPath.row) {
                    
                    Cell.rideUserImage.image = image;
                    [Cell setNeedsLayout];
                }
            });
        }
        else{
            if (Cell.tag == indexPath.row) {
                Cell.rideUserImage.image = [UIImage imageNamed:@"ic_user_b.png"];
                [Cell setNeedsLayout];
            }
            
            
        }
    });
    
    
    Cell.btnEdit.tag = indexPath.row;
    Cell.btnFull.tag = indexPath.row;
    Cell.btnDelete.tag = indexPath.row;
    Cell.btnCall.tag = indexPath.row;
    Cell.btnSMS.tag = indexPath.row;
    
    
    
    [Cell.btnEdit addTarget:self action:@selector(rideEditPressed:)  forControlEvents:UIControlEventTouchUpInside];
    
    [Cell.btnFull addTarget:self action:@selector(rideFullPressed:)  forControlEvents:UIControlEventTouchUpInside];
    
    [Cell.btnDelete addTarget:self action:@selector(rideDeletePressed:)  forControlEvents:UIControlEventTouchUpInside];
    [Cell.btnCall addTarget:self action:@selector(callUserPressed:)  forControlEvents:UIControlEventTouchUpInside];
    [Cell.btnSMS addTarget:self action:@selector(smsUserPressed:)  forControlEvents:UIControlEventTouchUpInside];
    
    return Cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:indexPath.row];
    
    // Popup view
    
    CGFloat viewHeight;
    CGFloat viewWidth;
    CGFloat viewX;
    CGFloat viewY;
    // popup view dimensions
    viewHeight = (self.view.frame.size.height/2) + (self.view.frame.size.height/2.5);
    viewWidth  = (self.view.frame.size.width/2) + (self.view.frame.size.width/3);
    viewX = (self.view.frame.size.width/12);
    viewY = (self.view.frame.size.height/18);
    
    self.customView = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
    
    self.customView.backgroundColor = [UIColor whiteColor];
    // popup View Shadow
    self.customView.layer.cornerRadius = 3;
    self.customView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.customView.layer.shadowOpacity = 0.8;
    self.customView.layer.shadowRadius = 12;
    self.customView.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    self.customView.tag = 100;
    
    CGFloat allControlYPos = (self.customView.frame.size.height) - (self.customView.frame.size.height);
    
    CGFloat allControlXPos = (self.customView.frame.size.width) - (self.customView.frame.size.width);
    
    
    // Ride Time Label
    UILabel *popupRideTime = [[UILabel alloc]initWithFrame:CGRectMake(5, allControlYPos+5, 160, 70)];
    popupRideTime.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    popupRideTime.textColor = [UIColor groupoolMainColor];
    popupRideTime.numberOfLines = 3;
    popupRideTime.lineBreakMode = NSLineBreakByWordWrapping;
    NSString *timeString = obj.time;
    NSScanner *timescanner = [NSScanner scannerWithString:timeString];
    int hour, minutes;
    [timescanner scanInt:&hour];
    [timescanner scanString:@":" intoString:nil];
    [timescanner scanInt:&minutes];
    timeString = [NSString stringWithFormat:@"%02d:%02d",hour,minutes];
    popupRideTime.text = timeString;
    
    NSString *tDate,*tmDate;
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [[NSDate alloc] init];
    // Today
    tDate = [dateFormatter stringFromDate:now];
    
    int daysToAdd = 1;
    NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
    // Tomorrwo
    tmDate = [dateFormatter stringFromDate:tomorrowsDate];
    
    if ([obj.date isEqualToString:@"0000-00-00"])
    {
        
        NSDate *todaysDate;
        NSString *dayString;
        
        
        [dateFormatter setDateFormat:@"EE"];
        todaysDate = [[NSDate alloc] init];
        
        int daysToAdd = 1;
        
        if ([self.lblDaySelection.text isEqualToString:@"Today"]) {
            
            dayString = [dateFormatter stringFromDate:todaysDate];
            
            if ([obj.days containsString:dayString])
            {
                dayString = @"Today";
            }
            
            
        }else{
            
            dayString = [dateFormatter stringFromDate:[todaysDate dateByAddingTimeInterval:86400]];
            if ([obj.days containsString:dayString]) {
                dayString = @"Tomorrow";
            }
            
        }
        popupRideTime.text = [NSString stringWithFormat:@"%@ %@",timeString,dayString];
        
    }
    else
    {
        
        if ([obj.date isEqualToString:tDate]) {
            popupRideTime.text = [NSString stringWithFormat:@"%@ Today",timeString];
        }
        else if ([obj.date isEqualToString:tmDate])
        {
            popupRideTime.text = [NSString stringWithFormat:@"%@ Tomorrow",timeString];
        }
        
    }
    
    // Ride price label
    UILabel *popupRidePrice = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 95, allControlYPos+15, 90,36)];
    popupRidePrice.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
    popupRidePrice.textAlignment = NSTextAlignmentRight;
    popupRidePrice.textColor = [UIColor groupoolMainColor];
    popupRidePrice.text = [NSString stringWithFormat:@"Rs.%@",obj.price];
    
    
    // Ride Locations(From and To) Label
    UILabel *popupFromRideLocations = [[UILabel alloc] initWithFrame:CGRectMake(5, allControlYPos+81, self.customView.frame.size.width - 10, 94)];
    
    popupFromRideLocations.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupFromRideLocations.textColor = [UIColor grayTextColor];
    
    popupFromRideLocations.lineBreakMode = NSLineBreakByWordWrapping;
    popupFromRideLocations.numberOfLines = 6;
    popupFromRideLocations.text = [NSString stringWithFormat:@"%@ \n>>\n%@",obj.startGroupLocation,obj.endGroupLocation];
    
    // UI Bar
    UIView  *popupBar = [[UIView alloc] initWithFrame:CGRectMake(5, allControlYPos+180, self.customView.frame.size.width-10, 2)];
    popupBar.backgroundColor = [UIColor colorWithRed:242.00/255.00 green:242.00/255.00 blue:242.00/255.00 alpha:1];
    
    UIView  *popupBar1 = [[UIView alloc] initWithFrame:CGRectMake(5, allControlYPos+224, self.customView.frame.size.width-10, 2)];
    popupBar1.backgroundColor = [UIColor colorWithRed:242.00/255.00 green:242.00/255.00 blue:242.00/255.00 alpha:1];
    
    
    // Car Pic Imageview
    UIImageView *popupCarPic = [[UIImageView alloc]initWithFrame:CGRectMake(5, allControlYPos+188, 32, 32)];
    
    if ([obj.carType isEqualToString:@"HB"]) {
        popupCarPic.image = [UIImage imageNamed:@"car_two_selected.png"];
    }
    if ([obj.carType isEqualToString:@"SE"]) {
        
        popupCarPic.image = [UIImage imageNamed:@"car_one_selected.png"];
        
    }
    if ([obj.carType isEqualToString:@"SU"]) {
        popupCarPic.image = [UIImage imageNamed:@"car_three_selected.png"];
    }
    if ([obj.carType isEqualToString:@"OT"]) {
        
        popupCarPic.image = [UIImage imageNamed:@"car_four_selected.png"];
    }
    
    
    // Car Detail Label
    UILabel *popupcarDetails =[[UILabel alloc] initWithFrame:CGRectMake(40, allControlYPos+188, 80, 32)];
    popupcarDetails.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupcarDetails.numberOfLines = 2;
    popupcarDetails.lineBreakMode = NSLineBreakByWordWrapping;
    popupcarDetails.textAlignment = NSTextAlignmentLeft;
    popupcarDetails.textColor = [UIColor grayTextColor];
    popupcarDetails.textAlignment = NSTextAlignmentCenter;
    popupcarDetails.text = obj.carDetails;
    //    popupcarType.text = @"HB";
    
    // Car No. Label
    UILabel *popupCarNumber = [[UILabel alloc] initWithFrame:CGRectMake((self.customView.frame.size.width - 145), allControlYPos+195, 135, 16)];
    
    popupCarNumber.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupCarNumber.textColor = [UIColor grayTextColor];
    popupCarNumber.textAlignment = NSTextAlignmentRight;
    if ([obj.carNumber isEqualToString:@""]) {
        
    }
    else{
        popupCarNumber.text = [NSString stringWithFormat:@"Car No.:%@",obj.carNumber];
        
    }
    
    
    // User Pic Imageview
    UIImageView *popupUserImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, allControlYPos+230, 42, 42)];
    popupUserImage.layer.cornerRadius = 21;
    popupUserImage.layer.masksToBounds = YES;
    popupUserImage.contentMode = UIViewContentModeScaleAspectFill;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSURL *imageURL = [NSURL URLWithString:obj.profilePic];
        NSData *imageData = [NSData dataWithContentsOfURL: imageURL];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                popupUserImage.image = image;
                
                
            });
        }
    });
    
    // User Name Label
    UILabel *popupUserName = [[UILabel alloc]initWithFrame:CGRectMake(5, allControlYPos+274, 100, 36)];
    popupUserName.font = [UIFont systemFontOfSize:15];
    popupUserName.numberOfLines = 2;
    popupUserName.lineBreakMode = NSLineBreakByWordWrapping;
    popupUserName.textColor = [UIColor groupoolMainColor];
    popupUserName.textAlignment = NSTextAlignmentLeft;
    NSArray *nameArray = [obj.name componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    popupUserName.text = nameArray[0];
    if ([popupUserName.text isEqualToString:@""]) {
        popupUserName.text = [NSString stringWithFormat:@"%@ (%@)",obj.name,obj.gender];
    }else{
        //        popupUserName.text = [NSString stringWithFormat:@"%@ (%@)",popupUserName.text,obj.gender];
        popupUserName.text = [NSString stringWithFormat:@"%@ (%@)",obj.name,obj.gender];
        
    }
    
    //Works At Label
    UILabel *popupCompanyName = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 185, allControlYPos+240, 180, 35)];
    popupCompanyName.numberOfLines = 2;
    popupCompanyName.lineBreakMode = NSLineBreakByCharWrapping;
    popupCompanyName.textColor = [UIColor grayTextColor];
    popupCompanyName.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupCompanyName.textAlignment = NSTextAlignmentRight;
    popupCompanyName.text = [NSString stringWithFormat:@"Works at: %@",obj.company];
    
    //Mobile Verified Label
    UILabel *popupMobileVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 160, allControlYPos+276, 155, 15)];
    popupMobileVerified.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupMobileVerified.textColor = [UIColor grayTextColor];
    popupMobileVerified.textAlignment = NSTextAlignmentRight;
    if ([obj.smsVerified isEqualToString:@"1"]) {
        popupMobileVerified.text = @"Mobile Verified: Yes";
    }else{
        popupMobileVerified.text = @"Mobile Verified: No";
    }
    
    
    //Office Email Verified
    UILabel *popupOfficeEmailVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 158, allControlYPos+295, 155, 15)];
    popupOfficeEmailVerified.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupOfficeEmailVerified.textColor = [UIColor grayTextColor];
    popupOfficeEmailVerified.textAlignment = NSTextAlignmentRight;
    if ([obj.officeEmailVerified isEqualToString:@"1"]) {
        popupOfficeEmailVerified.text = @"Office Email Verified:Yes";
    }else{
        popupOfficeEmailVerified.text = @"Office Email Verified:No";
    }
    
    //Comment From car owner Label
    UILabel *commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, allControlYPos+320, self.customView.frame.size.width - 20, 13)];
    commentLabel.textColor = [UIColor grayTextColor];
    commentLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    commentLabel.text = @"Comment from Car owner:";
    
    
    UITextView *popupCarOwnerComment = [[UITextView alloc] initWithFrame:CGRectMake(5, allControlYPos+335, self.customView.frame.size.width - 10, 38)];
    popupCarOwnerComment.editable = NO;
    popupCarOwnerComment.selectable = NO;
    popupCarOwnerComment.showsVerticalScrollIndicator = YES;
    popupCarOwnerComment.textContainer.maximumNumberOfLines = 6;
    popupCarOwnerComment.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    
    popupCarOwnerComment.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    popupCarOwnerComment.textColor = [UIColor grayTextColor];
    
    
    if ([obj.comment isEqualToString:@""]) {
        popupCarOwnerComment.text = @"[No comments]";
    }else {
        popupCarOwnerComment.text = obj.comment;
    }
    
    // OK Button
    CGFloat okButtonX = (self.customView.frame.size.width) - (self.customView.frame.size.width/3.5) ;
    CGFloat okButtonY = (self.customView.frame.size.height) - (self.customView.frame.size.height / 10 - 8);
    
    
    UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(okButtonX, okButtonY, 70, 30)];
    
    btnOK.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
    
    [btnOK setTitle:@"DONE" forState:UIControlStateNormal];
    [btnOK setTitleColor:[UIColor colorWithRed:81.00/255.00 green:165.00/255.00 blue:243.00/255.00 alpha:1] forState:UIControlStateNormal];
    
    
    [btnOK addTarget:self action:@selector(closePopup:)  forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *btnCALL = [[UIButton alloc] initWithFrame:CGRectMake(allControlXPos+5, okButtonY, 40, 30)];
    btnCALL.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
    [btnCALL setTitle:@"CALL" forState:UIControlStateNormal];
    [btnCALL setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [btnCALL setTag:indexPath.row];
    [btnCALL addTarget:self action:@selector(callUserPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnSMS = [[UIButton alloc] initWithFrame:CGRectMake(allControlXPos+50, okButtonY, 40, 30)];
    btnSMS.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
    [btnSMS setTitle:@"SMS" forState:UIControlStateNormal];
    [btnSMS setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [btnSMS setTag:indexPath.row];
    [btnSMS addTarget:self action:@selector(smsUserPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnCHAT = [[UIButton alloc] initWithFrame:CGRectMake(allControlXPos+95, okButtonY, 45, 30)];
    btnCHAT.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
    [btnCHAT setTitle:@"CHAT" forState:UIControlStateNormal];
    [btnCHAT setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [btnCHAT setTag:indexPath.row];
    [btnCHAT addTarget:self action:@selector(closePopup:) forControlEvents:UIControlEventTouchUpInside];
    
    
    // Add popup controls to popupview
    [self.customView addSubview:popupRideTime];
    [self.customView addSubview:popupFromRideLocations];
    [self.customView addSubview:popupRidePrice];
    [self.customView addSubview:popupBar];
    [self.customView addSubview:popupcarDetails];
    [self.customView addSubview:popupCarPic];
    [self.customView addSubview:popupCarNumber];
    [self.customView addSubview:popupBar1];
    [self.customView addSubview:popupUserImage];
    [self.customView addSubview:popupUserName];
    [self.customView addSubview:popupCompanyName];
    [self.customView addSubview:popupMobileVerified];
    [self.customView addSubview:popupOfficeEmailVerified];
    [self.customView addSubview:commentLabel];
    [self.customView addSubview:popupCarOwnerComment];
    
    if ([userID isEqualToString:obj.userID])
    {
        [self.customView addSubview:btnOK];
        
        
    }else{
        [self.customView addSubview:btnOK];
        [self.customView addSubview:btnCALL];
        [self.customView addSubview:btnSMS];
        //        [self.customView addSubview:btnCHAT];
    }
    
    self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
    
    [self.view addSubview:self.customView];
    
    [UIView animateWithDuration:0.5
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
         self.myTableView.userInteractionEnabled = NO;
         
     }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
/************ Ride Details popup close Implementation **************/
- (void) closePopup:(id)sender {
    
    
    
    self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.5
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
         self.myTableView.userInteractionEnabled = YES;
         
     }
     ];
    
    [self.customView removeFromSuperview];
    
}

#pragma mark - Cell Buttons Action Implementations
-(void)callUserPressed:(id)sender {
    
    
    
    UIButton *button = (UIButton*)sender;
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:button.tag];
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:12];
    
    
    if ([obj.isFull isEqualToString:@"1"]) {
        
        [self.view makeToast:@"Ride is marked as Full" duration:2.0 position:CSToastPositionBottom style:style];
        return;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"OfficeEmailVerified"] isEqualToString:@"1"]
        && [[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileVerified"] isEqualToString:@"1"])
    {
        
        
        NSString *userMobileNumber = obj.mobile;
        userMobileNumber = [NSString stringWithFormat:@"+91%@",userMobileNumber];
        //    NSString *phNo = @"+919876543210";
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",userMobileNumber]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            NSLog(@"Call facility not available");
        }
    }
    else
    {
        
        
        NSString *alertMessage;
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}
-(void)smsUserPressed:(id)sender {
    
    
    UIButton *button = (UIButton*)sender;
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:button.tag];
    
    if ([obj.isFull isEqualToString:@"1"]) {
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Ride is marked as Full" duration:2.0 position:CSToastPositionBottom style:style];
        return;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"OfficeEmailVerified"] isEqualToString:@"1"]
        && [[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileVerified"] isEqualToString:@"1"])
    {
        
        NSString *userMobileNumber = obj.mobile;
        userMobileNumber = [NSString stringWithFormat:@"sms:+91%@",userMobileNumber];
  
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:userMobileNumber]];
    }
    else
    {
        
        NSString *alertMessage;
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}
-(void)rideDeletePressed:(id)sender {
    
    UIButton *button = (UIButton*)sender;
 
    FindDataModel *obj = [OfferedRideData objectAtIndex:button.tag];
    
    NSString *alertMessage;
    if ([obj.date isEqualToString:@"0000-00-00"] && (obj.days != nil))
    {
        rideDate = [self getRideDate];
        
        alertMessage = @"To Delete Repeat ride, go to Menu > My Ride. \r Sure Want to delete this ride for selected day?";
   
        
    }
    else
    {
        rideDate = @"0000-00-00";
        alertMessage = @"Sure want to Delete the Ride?";
    }
    
    UIAlertController *alert =  [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self.view makeToastActivity:CSToastPositionCenter];
        NSIndexPath *indexPath = [self.myTableView indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
        
        //    deleteOfferedRide
        deleteOfferedRideURL = [NSString stringWithFormat:@"%@/%@/%@",DELETE_ONE_INSTANCE_OFFERED_RIDE_URL,obj.rideID,rideDate];
        deleteOfferedRideURL = [deleteOfferedRideURL  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:deleteOfferedRideURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
        {
            
//            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
            
            [self.myTableView beginUpdates];
            [self.myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            [OfferedRideData removeObjectAtIndex:button.tag];
            [self.myTableView endUpdates];
            
            [self.view hideToastActivity];
            
        }];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
}
-(void)rideFullPressed:(id)sender {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@"Sure want to mark this Ride as full?"
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                             UIButton *button = (UIButton*)sender;
                             
                          
                             
                             FindDataModel *obj = [OfferedRideData objectAtIndex:button.tag];
                             
                             // Sure want to mark this ride as full?
                             if ([obj.date isEqualToString:@"0000-00-00"] && (obj.days != nil))
                             {
                                 rideDate = [self getRideDate];
                                 
                             }
                             else
                             {
                                 rideDate = obj.date;
                                 
                             }
                             
                             NSString *tmpDate = @"0000-00-00";
                             
                             @try {
                                 
                                 [self.view makeToastActivity:CSToastPositionCenter];
                                 
                                 NSString *addressString = [NSString stringWithFormat:@"%@/%@/%@",MARK_OFFERED_RIDE_FULL,obj.rideID,rideDate];
                                 addressString = [addressString  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                 
                                 NSURL *url = [NSURL URLWithString:addressString];
                                 
                                 NSURLRequest *request = [NSURLRequest requestWithURL:url];
                                 [NSURLConnection sendAsynchronousRequest:request
                                                                    queue:[NSOperationQueue mainQueue]
                                                        completionHandler:^(NSURLResponse *response,
                                                                            NSData *data, NSError *connectionError)
                                 {
                                     [self.view hideToastActivity];
                                     
                                     CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                                     style.messageFont = [UIFont systemFontOfSize:12];
                                     [self.view makeToast:@"Ride Marked as Full" duration:2.0 position:CSToastPositionBottom style:style];
                                     [self getOfferRidesAll:rideDay];
                                     
                                 }];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }
                             
                             @catch (NSException *exception) {
                                 [self.view hideToastActivity];
                                 NSLog(@"Error:%@",exception);
                             }
                             
                         }];
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                                 [self.view hideToastActivity];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [alert addAction:OK];
    [alert addAction:Cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)rideEditPressed:(id)sender {
    
    NSIndexPath *indexPath = [self.myTableView indexPathForCell:(UITableViewCell *)[[sender superview] superview]];
    NSInteger row = indexPath.row;
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:row];
    cellID = row;
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    self.selectedTime = [dateFormatter dateFromString:obj.time];
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
    
    
    ActionSheetDatePicker *aDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Time" datePickerMode:UIDatePickerModeTime  selectedDate:self.selectedTime target:self action:@selector(timeWasSelected:element:) origin:sender cancelAction:@selector(cancelTimeSelection:)];
    
    aDatePicker.minuteInterval = minuteInterval;
    
    //    [datePicker showActionSheetPicker];
    //    [datePicker setLocale:[NSLocale systemLocale]];
    [aDatePicker showActionSheetPicker];
    [aDatePicker setLocale:[NSLocale systemLocale]];
    
    
}

/************ Time Selection Picker Implementation **************/
- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    self.selectedTime = selectedTime;
    
    UIButton *button = (UIButton *)element;
  
    
    NSString *createdTime;
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    createdTime = [dateFormatter stringFromDate:selectedTime];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    if(formStTime < nowTime) {
   
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionCenter style:style];
        
    }
    else
    {
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    
        [self editOfferedRide:[dateFormatter stringFromDate:selectedTime] index:button.tag];
    }
}
/************ Update changed Ride time to API ****************/
-(void)editOfferedRide:(NSString *)changedTime index:(NSInteger)index {
    
    NSString *alertMessage;
    
    FindDataModel *obj = [OfferedRideData objectAtIndex:cellID];
    
    if ([obj.date isEqualToString:@"0000-00-00"] && (obj.days != nil))
    {
        rideDate = [self getRideDate];
        
        alertMessage = @"Eidt Time only for Selected day.\r To change Repeat ride, go to Menu > My Rides";
      
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [self updateEditedRide:obj.rideID date:rideDate time:changedTime];
            
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        rideDate = @"0000-00-00";
        
        [self updateEditedRide:obj.rideID date:rideDate time:changedTime];
    }
    
}

- (void)updateEditedRide:(NSString *)aRideID date:(NSString *)aRideDate time:(NSString *)aRideTime{
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@",EDIT_OFFERED_RIDE_URL,aRideID,aRideDate,aRideTime]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
    {
        
        if (data.length > 0 && connectionError == nil) {
            
            NSString *dataString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
          
            if ([dataString isEqualToString:@"1"])
            {
                [self getOfferRidesAll:rideDay];
                
            }
        }
        
    }];
    
}


/************ Cancel Time Selection Picker Implementation **************/
- (void)cancelTimeSelection:(id)sender {
    NSLog(@"Time selection canceled..");
}

#pragma mark - Request Ride implementation
-(void) rideRequested {
    
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"1"]
        && [[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"1"])
    {
        
        RequestRideVC *rideRequest = [[self storyboard] instantiateViewControllerWithIdentifier:@"requestrideVC"];
        UINavigationController *navRequestRide = [[UINavigationController alloc]initWithRootViewController:rideRequest];
        [self.navigationController presentViewController:navRequestRide animated:YES completion:nil];
        
    }
    else{
        
        
        NSString *alertMessage;
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
    
}

#pragma mark - Refresh table data offerview delegate
- (void) refreshTableData:(OfferVC *)refreshTableData {
    
    [self fetchData];
}

#pragma mark - Invite Friends Action
-(IBAction)InviteFriendsAction:(id)sender {
  
    
    
    NSString *message = @"I'm using Groupool App for trusted carpooling.It's simply an awesome app to share a ride. Give it a Try!(* it's only for corporate employees)"; //this is your text string to share
    //    NSString *shareLink = @"https://play.google.com/sore/apps/details?id=in.groupool";
    NSURL *shareURl = [NSURL URLWithString:GROUPOOL_PLAY_STORE_URL];
    
    NSArray *activityItems = @[message,shareURl];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:TRUE completion:nil];
    
}


#pragma mark - Select Group dropdown Action & Implementation
-(IBAction)selectGroupPressed:(id)sender {
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    NSString *tmpObject,*tmpObject1;
    reverseRouteArray = [[NSMutableArray alloc] init];
    reverseRouteFromGroupID = [[NSMutableArray alloc] init];
    reverseRouteToGroupID = [[NSMutableArray alloc] init];
    
    for (int i=0;i<([fromGroupsNameArray count]+1);i++)
    {
        if (i == 0)
        {
            tmpObject = @"All Rides(You and nearby Groups)";
            [newArray addObject:tmpObject];
            
        }
        else
        {
            // Reverse Route List
            tmpObject = [NSString stringWithFormat:@"%@ > %@",
                         [fromGroupsNameArray objectAtIndex:i-1],
                         [toGroupsNameArray objectAtIndex:i-1]];
            tmpObject1  = [NSString stringWithFormat:@"%@ > %@",
                           [toGroupsNameArray objectAtIndex:i-1],
                           [fromGroupsNameArray objectAtIndex:i-1]];
            
            [newArray addObject:tmpObject];
            [newArray addObject:tmpObject1];
            [reverseRouteArray addObject:tmpObject];
            [reverseRouteArray addObject:tmpObject1];
            tmpObject = nil;
            tmpObject1 = nil;
            
            // Reverse Route From Group ID
            tmpObject = [fromGroupIDArray objectAtIndex:i-1];
            tmpObject1 = [toGroupIDArray objectAtIndex:i-1];
            
            [reverseRouteFromGroupID addObject:tmpObject];
            [reverseRouteFromGroupID addObject:tmpObject1];
            tmpObject = nil;
            tmpObject1 = nil;
            
            // Reverse Route To Group ID
            tmpObject =  [toGroupIDArray objectAtIndex:i-1];
            tmpObject1  = [fromGroupIDArray objectAtIndex:i-1];
            
            [reverseRouteToGroupID addObject:tmpObject];
            [reverseRouteToGroupID addObject:tmpObject1];
            tmpObject = nil;
            tmpObject1 = nil;
            
        }
        
    }
    
    if (!([_userSelectedGroup isEqualToString:@""]) && (_userSelectedGroup != nil)) {

        self.lblRouteSelection.text = _userSelectedGroup;
        
    }
    
    NSArray *tmpArray = [newArray mutableCopy];
    
    //    NSArray *tmpArray = [fromGroupsName copy];
    [ActionSheetStringPicker showPickerWithTitle:@"Select Route" rows:tmpArray initialSelection:self.selectedIndex target:self successAction:@selector(groupSelected:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
}
-(void)groupSelected:(NSNumber *)selectedIndex element:(id)element {
    self.selectedIndex = [selectedIndex intValue];
   
    
    if (self.selectedIndex == 0)
    {
        self.lblRouteSelection.text = @"All Rides(You and nearby Groups)";
        _userSelectedGroup = @"All Rides(You and nearby Groups)";
        startGroupID = nil;
        endGroupID = nil;
        [self getOfferRidesAll:rideDay];
        
    }
    else
    {
        self.lblRouteSelection.text = (reverseRouteArray)[(NSUInteger) (self.selectedIndex)-1];
        
        _userSelectedGroup = (reverseRouteArray)[(NSUInteger) (self.selectedIndex)-1];
        
        startGroupID = (reverseRouteFromGroupID)[(NSInteger) (self.selectedIndex)-1];
        endGroupID = (reverseRouteToGroupID)[(NSInteger) (self.selectedIndex)-1];
        
        [self getOfferedRide:rideDay
                startGroupID:(reverseRouteFromGroupID)[(NSInteger) (self.selectedIndex)-1]
                  endGroupID:(reverseRouteToGroupID)[(NSInteger) (self.selectedIndex)-1]];
        
    }
    
}

/************* Get Offered Ride Filter Implementation ***********/
-(void)getOfferedRide:(NSString *)ridesDay startGroupID:(NSString *)startID endGroupID:(NSString *)endID {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    @try {
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@/%@",GET_OFFERED_RIDE_URL,startID,endID,rideDay,userID]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
        {
            
            if (data.length > 0 && connectionError == nil) {
                
                NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                OfferedRideData = [[NSMutableArray alloc] init];
                
                for (NSDictionary *rides in jsonArray)
                {
                    
                    FindDataModel *obj = [[FindDataModel alloc] init];
                    
                    obj.name = rides[@"name"];
                    obj.gender = rides[@"gender"];
                    obj.mobile = rides[@"mobile"];
                    obj.profilePic = rides[@"profile_pic"];
                    obj.carType = rides[@"car_type"];
                    obj.carNumber = rides[@"car_number"];
                    obj.carDetails = rides[@"car_details"];
                    obj.smsVerified = rides[@"sms_verified"];
                    obj.emailVerified = rides[@"email_verified"];
                    obj.officeEmailVerified = rides[@"office_email_verified"];
                    obj.company = rides[@"company"];
                    obj.startGroupName = rides[@"start_group_name"];
                    obj.startGroupLocation = rides[@"start_group_location"];
                    obj.startGroupLatitude = rides[@"start_group_latitude"];
                    obj.startGroupLongitude = rides[@"start_group_longitude"];
                    obj.endGroupName = rides[@"end_group_name"];
                    obj.endGroupLocation = rides[@"end_group_location"];
                    obj.endGroupLatitude = rides[@"end_group_latitude"];
                    obj.endGroupLongitude = rides[@"end_group_longitude"];
                    obj.rideID = rides[@"id"];
                    obj.userID = rides[@"user_id"];
                    obj.startGroupID = rides[@"start_group_id"];
                    obj.endGroupID = rides[@"end_group_id"];
                    obj.date = rides[@"date"];
                    obj.time = rides[@"time"];
                    obj.days = rides [@"days"];
                    obj.offeredSeats = rides[@"offered_seats"];
                    obj.price = rides[@"price"];
                    obj.womenOnly = rides[@"women_only"];
                    obj.editedID = rides[@"edited_id"];
                    obj.comment = rides[@"comment"];
                    obj.createdBy = rides[@"created_by"];
                    obj.createdTime = rides[@"created_time"];
                    obj.isFull = rides[@"is_full"];
                    
                    [OfferedRideData addObject:obj];
                }
                
                [self checkforUpdate];
                [self.view hideToastActivity];
                
            }
            
        }];
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@",exception.description);
        
    }
    
    
    
}
-(void)refreshTable {
    
    [self.myTableView reloadData];
    NSIndexSet * sections = [NSIndexSet indexSetWithIndex:0];
    [self.myTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    
    BOOL isRideDataEmpty = ([OfferedRideData count] == 0);
    
    
    if (isRideDataEmpty) {
        
        self.myTableView.hidden = YES;
    }
    else{
        self.myTableView.hidden = NO;
    }
    
}

#pragma mark - Select Day dropdown Action & Implementation
-(IBAction)selectDayPressed:(id)sender {
    
    if (!([_userSelectedTime isEqualToString:@""]) && (_userSelectedTime != nil))
    {
        
//        [self.selectDay setTitle:_userSelectedTime forState:UIControlStateNormal];
       
        self.lblDaySelection.text = _userSelectedTime;
        
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Day" rows:rideDayArray initialSelection:self.selectedIndexForDay target:self successAction:@selector(daySelected:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
}
-(void)daySelected:(NSNumber *)selectedIndex element:(id)element {
    
    self.selectedIndexForDay = [selectedIndex integerValue];
    
    if ((long)self.selectedIndexForDay == 0)
    {
        _userSelectedTime = @"Today";
    }
    else
    {
        _userSelectedTime = @"Tomorrow";
        
    }
//    [self.selectDay setTitle:[rideDayArray objectAtIndex:self.selectedIndexForDay] forState:UIControlStateNormal];
    self.lblDaySelection.text = [rideDayArray objectAtIndex:self.selectedIndexForDay];
    
    [[NSUserDefaults standardUserDefaults] setValue:(rideDayArray)[(NSInteger) self.selectedIndexForDay] forKey:@"PopupRideDay"];
    rideDay = [NSString stringWithFormat:@"%ld",(long)self.selectedIndexForDay];
    
    
    if (startGroupID == nil && endGroupID == nil)
    {
        [self getOfferRidesAll:rideDay];
    }
    else
    {
        [self getOfferedRide:rideDay
                startGroupID:(reverseRouteFromGroupID)[(NSInteger) (self.selectedIndex)-1]
                  endGroupID:(reverseRouteToGroupID)[(NSInteger) (self.selectedIndex)-1]];
        
    }
    
}

- (NSString *)getRideDate {
    
    NSString *tDate;
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *now = [[NSDate alloc] init];
    
//    NSString *dayStr = self.selectDay.titleLabel.text;
    NSString *dayStr = self.lblDaySelection.text;
    
    NSString *trimmedString = [dayStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if ([trimmedString isEqualToString:@"Today"]) {
        
        tDate = [dateFormatter stringFromDate:now];
        
    }
    else{
        
        int daysToAdd = 1;
        NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
        tDate = [dateFormatter stringFromDate:tomorrowsDate];
    }
    
    [[NSUserDefaults standardUserDefaults]setValue:tDate forKey:@"RideDate"];
    return tDate;
}
/************ Get all offered rides from API ****************/
-(void)getOfferRidesAll:(NSString *)rideDaySelected {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString *company = @"#";
    
    @try {
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@",GET_OFFERED_RIDE_ALL_URL,rideDaySelected,userID,company]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
        {
            
            if (data.length > 0 && connectionError == nil)
                
            {
                
                NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                
                OfferedRideData = [[NSMutableArray alloc] init];
                
                for (NSDictionary *rides in jsonArray)
                {
                    
                    FindDataModel *obj = [[FindDataModel alloc] init];
                    
                    obj.name = rides[@"name"];
                    obj.gender = rides[@"gender"];
                    obj.mobile = rides[@"mobile"];
                    obj.profilePic = rides[@"profile_pic"];
                    obj.carType = rides[@"car_type"];
                    obj.carNumber = rides[@"car_number"];
                    obj.carDetails = rides[@"car_details"];
                    obj.smsVerified = rides[@"sms_verified"];
                    obj.emailVerified = rides[@"email_verified"];
                    obj.officeEmailVerified = rides[@"office_email_verified"];
                    obj.company = rides[@"company"];
                    obj.startGroupName = rides[@"start_group_name"];
                    obj.startGroupLocation = rides[@"start_group_location"];
                    obj.startGroupLatitude = rides[@"start_group_latitude"];
                    obj.startGroupLongitude = rides[@"start_group_longitude"];
                    obj.endGroupName = rides[@"end_group_name"];
                    obj.endGroupLocation = rides[@"end_group_location"];
                    obj.endGroupLatitude = rides[@"end_group_latitude"];
                    obj.endGroupLongitude = rides[@"end_group_longitude"];
                    obj.rideID = rides[@"id"];
                    obj.userID = rides[@"user_id"];
                    obj.startGroupID = rides[@"start_group_id"];
                    obj.endGroupID = rides[@"end_group_id"];
                    obj.date = rides[@"date"];
                    obj.time = rides[@"time"];
                    obj.days = rides [@"days"];
                    obj.offeredSeats = rides[@"offered_seats"];
                    obj.price = rides[@"price"];
                    obj.womenOnly = rides[@"women_only"];
                    obj.editedID = rides[@"edited_id"];
                    obj.comment = rides[@"comment"];
                    obj.createdBy = rides[@"created_by"];
                    obj.createdTime = rides[@"created_time"];
                    obj.isFull = rides[@"is_full"];
                    
                    [OfferedRideData addObject:obj];
                }
                
                [self checkforUpdate];
                
                [self.view hideToastActivity];
                
            }
            
        }];
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@",exception.description);
        
    }
}
-(void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
    
}

- (void) fetchUpdatedData:(CarDetailsVC *)refetchRides{
    [self fetchData];
}

@end
