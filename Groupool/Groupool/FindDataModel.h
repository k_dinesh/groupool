//
//  FindDataModel.h
//  Groupool
//
//  Created by Parth Pandya on 23/03/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FindDataModel : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *profilePic;
@property (nonatomic,strong) NSString *carType;
@property (nonatomic,strong) NSString *carNumber;
@property (nonatomic,strong) NSString *carDetails;
@property (nonatomic,strong) NSString *smsVerified;
@property (nonatomic,strong) NSString *emailVerified;
@property (nonatomic,strong) NSString *officeEmailVerified;
@property (nonatomic,strong) NSString *company;
@property (nonatomic,strong) NSString *startGroupName;
@property (nonatomic,strong) NSString *startGroupLocation;
@property (nonatomic,strong) NSString *startGroupLatitude;
@property (nonatomic,strong) NSString *startGroupLongitude;
@property (nonatomic,strong) NSString *endGroupName;
@property (nonatomic,strong) NSString *endGroupLocation;
@property (nonatomic,strong) NSString *endGroupLatitude;
@property (nonatomic,strong) NSString *endGroupLongitude;
@property (nonatomic,strong) NSString *rideID;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *startGroupID;
@property (nonatomic,strong) NSString *endGroupID;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *days;
@property (nonatomic,strong) NSString *offeredSeats;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *womenOnly;
@property (nonatomic,strong) NSString *editedID;
@property (nonatomic,strong) NSString *comment;
@property (nonatomic,strong) NSString *createdBy;
@property (nonatomic,strong) NSString *createdTime;
@property (nonatomic,strong) NSString *isFull;

@end
