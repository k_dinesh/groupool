//
//  ChangePasswordVC.m
//  Groupool
//
//  Created by HN on 21/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "ChangePasswordVC.h"

#import "UIColor+GroupoolColors.h"
#import "AppStartVC.h"
#import "UIView+Toast.h"


@interface ChangePasswordVC ()
{
    CALayer *tcpBorder;
    CGFloat tcpBorderWidth;
    CALayer *tnpBorder;
    CGFloat tnpBorderWidth;
    CALayer *tcnpBorder;
    CGFloat tcnpBorderWidth;
    CGFloat animatedDistance;
    NSString *userID;
}
@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@implementation ChangePasswordVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtCurrentPassword.delegate = self;
    _txtConfirmNewPassword.delegate = self;
    _txtNewPassword.delegate = self;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    // Done Button
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(passwordChanged)];
    
    self.navigationItem.rightBarButtonItem = doneBtn;
    self.title = @"Change Password";
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    [CSToastManager setTapToDismissEnabled:YES];
    
    
    //     Current Password textfield customization
    
    tcpBorder = [CALayer layer];
    tcpBorderWidth = 1.5;
    tcpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcpBorder.frame = CGRectMake(0, _txtCurrentPassword.frame.size.height - tcpBorderWidth,_txtCurrentPassword.frame.size.width,_txtCurrentPassword.frame.size.height);
    tcpBorder.borderWidth = tcpBorderWidth;
    [_txtCurrentPassword.layer addSublayer:tcpBorder];
    _txtCurrentPassword.layer.masksToBounds = YES ;
    
    //     New Password textfield customization
    
    tnpBorder = [CALayer layer];
    tnpBorderWidth = 1.5;
    tnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tnpBorder.frame = CGRectMake(0, _txtNewPassword.frame.size.height - tnpBorderWidth,_txtNewPassword.frame.size.width,_txtNewPassword.frame.size.height);
    tnpBorder.borderWidth = tnpBorderWidth;
    [_txtNewPassword.layer addSublayer:tnpBorder];
    _txtNewPassword.layer.masksToBounds = YES ;
    
    
    //    Confirm New Password Textfield Customization
    
    tcnpBorder = [CALayer layer];
    tcnpBorderWidth = 1.5;
    tcnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    tcnpBorder.frame = CGRectMake(0, _txtConfirmNewPassword.frame.size.height - tcnpBorderWidth, _txtConfirmNewPassword.frame.size.width, _txtConfirmNewPassword.frame.size.height);
    tcnpBorder.borderWidth = tcnpBorderWidth;
    [_txtConfirmNewPassword.layer addSublayer:tcnpBorder];
    _txtConfirmNewPassword.layer.masksToBounds = YES ;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backButtonAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Password Change Implementation
-(void)passwordChanged {
    
    [self.view endEditing:YES];
    
    if ([_txtCurrentPassword.text isEqualToString:@""] || [_txtNewPassword.text isEqualToString:@""] || [_txtConfirmNewPassword.text isEqualToString:@""]) {
        
        [self.view endEditing:YES];
        
        for (UIView *view in [self.view subviews]) {
            if ([view isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField *)view;
                textField.text = @"";
            }
        }
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Please enter password" duration:1.0 position:CSToastPositionBottom style:style];
        
    }
    else{
        
        [self.view makeToastActivity:CSToastPositionCenter];
        
        userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
        
        
        NSString *addressString = [NSString stringWithFormat:@"%@/%@/%@/%@",CHANGE_PASSWORD_URL,userID,self.txtCurrentPassword.text,self.txtNewPassword.text];
        addressString = [addressString  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
       
        NSURL *url = [NSURL URLWithString:addressString];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             
             if (data.length > 0 && !connectionError) {
                 
                 NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 
                 if ([dataString isEqualToString:@"1"]) {
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults removeObjectForKey:@"MobileNumber"];
                     [defaults removeObjectForKey:@"Password"];

                     [defaults synchronize];
                     
                     [self.view hideToastActivity];
                     [self.view endEditing:YES];
                     
                     for (UIView *view in [self.view subviews]) {
                         if ([view isKindOfClass:[UITextField class]]) {
                             UITextField *textField = (UITextField *)view;
                             textField.text = @"";
                         }
                     }
                     
                     CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                     style.messageFont = [UIFont systemFontOfSize:12];
                     [self.view makeToast:@"Password changed" duration:2.0 position:CSToastPositionBottom style:style];
                     
                     [self dismissViewControllerAnimated:YES completion:nil];
                     
                 }
                 if ([dataString isEqualToString:@"0"]) {
                     
                     [self.view endEditing:YES];
                     
                     for (UIView *view in [self.view subviews]) {
                         if ([view isKindOfClass:[UITextField class]]) {
                             UITextField *textField = (UITextField *)view;
                             textField.text = @"";
                         }
                     }
                     [self.view hideToastActivity];
                     
                     CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                     style.messageFont = [UIFont systemFontOfSize:12];
                     [self.view makeToast:@"Incorrect password" duration:2.0 position:CSToastPositionBottom style:style];
                     
                 }
                 
             }
             else {
                 
                 [self.view hideToastActivity];
                 CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                 style.messageFont = [UIFont systemFontOfSize:12];
                 [self.view makeToast:@"Could not connect to server. Please try again" duration:1.0 position:CSToastPositionBottom style:style];
                 
             }
         }];
        
    }
}

#pragma mark - Textfield Delegates

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([_txtCurrentPassword resignFirstResponder]) {
        tcpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if ([_txtNewPassword resignFirstResponder]) {
        tnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtConfirmNewPassword resignFirstResponder]) {
        tcnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    if (textField == _txtCurrentPassword)
    {
        tcpBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tcnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    if (textField == _txtNewPassword) {
        tcpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tnpBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        tcnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtConfirmNewPassword) {
        tcpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tnpBorder.borderColor = [UIColor lightGrayColor].CGColor;
        tcnpBorder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return YES;
}

@end
