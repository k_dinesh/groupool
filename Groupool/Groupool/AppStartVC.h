//
//  AppStartVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "SignUpVC.h"

@interface AppStartVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (nonatomic) BOOL isUserLoggedOut;


- (IBAction)loginAction:(id)sender;
- (IBAction)signUpAction:(id)sender;
- (IBAction)btnTermsAndCondition:(id)sender;

@end
