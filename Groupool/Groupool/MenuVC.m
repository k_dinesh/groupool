//
//  MenuVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "MenuVC.h"


@interface MenuVC ()

@end

@implementation MenuVC


- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    
    self.menuTable.delegate = self;
    self.menuTable.dataSource = self;
    
    self.title = @"Groupool";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [menuButton setImage:[UIImage imageNamed:@"ic_drawer.png"] forState:UIControlStateNormal];
    menuButton.imageEdgeInsets = UIEdgeInsetsMake(3, 4, 3, 4);
    [menuButton addTarget:self action:@selector(backToHomeView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    _imageArray = [[NSMutableArray alloc] initWithObjects:@"ic_cartype.png",@"ic_group.png",@"ic_user_b.png",@"ic_feed.png",@"ic_share.png",@"ic_settings.png", nil];
    
    _titleArray = [[NSMutableArray alloc] initWithObjects:@"My Rides",@"Manage Routes",@"Profile",@"Feedback",@"Invite Friends",@"Settings", nil];
    
    
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"toggleMenu"];
    
    self.menuTable.tableFooterView = [[UIView alloc] init];
    
}


#pragma mark - Navigation bar back button implementation
- (void) backToHomeView {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
    [self.navigationController pushViewController:homeView animated:YES];
    
}

- (IBAction)btnGoToMyChats:(id)sender {
    
    MyChatsVC *myChats = [self.storyboard instantiateViewControllerWithIdentifier:@"MyChatsVC"];
    UINavigationController *navChat = [[UINavigationController alloc] initWithRootViewController:myChats];
    [self.navigationController presentViewController:navChat animated:YES completion:nil];
    
}

# pragma mark - TableviewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _imageArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 51;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *menuCellImage = (UIImageView *)[cell viewWithTag:11];
    UILabel *menuCellTitle = (UILabel *) [cell viewWithTag:1];
    
    
    [menuCellImage.layer setMasksToBounds:YES];
    menuCellImage.image = [UIImage imageNamed:[_imageArray objectAtIndex:indexPath.row]];
    
    

    
    
    menuCellTitle.text = [_titleArray objectAtIndex:indexPath.row];
    
        return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 133.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    UIImageView *userImage = (UIImageView *)[headerView viewWithTag:100];
    UILabel *userName = (UILabel *)[headerView viewWithTag:1];
    UILabel *userEmail = (UILabel *)[headerView viewWithTag:2];
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"Email"] || ![[[NSUserDefaults standardUserDefaults]valueForKey:@"Email"] isEqualToString:@""] || ![[NSUserDefaults standardUserDefaults] valueForKey:@"UserName"] || ![[[NSUserDefaults standardUserDefaults] valueForKey:@"UserName"] isEqualToString:@""])
    {
        
        userEmail.text  = [[NSUserDefaults standardUserDefaults] valueForKey:@"Email"] ;
        userName.text =  [[NSUserDefaults standardUserDefaults] valueForKey:@"UserName"] ;
        
        
    }
    
    
    userImage.layer.cornerRadius = 26 ;
    userImage.layer.masksToBounds = YES;
    userImage.contentMode = UIViewContentModeScaleAspectFill;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSString *strUserImage = [[NSUserDefaults standardUserDefaults] valueForKey:@"ProfilePic"];
        NSURL *imageURL = [NSURL URLWithString:strUserImage];
        NSData *imageData = [NSData dataWithContentsOfURL: imageURL];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    
                    userImage.image = image;
                [userImage setNeedsLayout];
                
            });
        }
        else{
            
                userImage.image = [UIImage imageNamed:@"ic_user_b.png"];
            [userImage setNeedsLayout];
     }
    });
  return headerView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == 0)    {
        MyRideVC *ride = [self.storyboard instantiateViewControllerWithIdentifier:@"MyRideVC"];
        
        UINavigationController *navSetting = [[UINavigationController alloc] initWithRootViewController:ride];
        [self.navigationController presentViewController:navSetting animated:YES completion:nil];
        
    }
    
    if (indexPath.row == 1)    {
        ManageGroupsVC  *routes = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageGroupsVC"];
        
        UINavigationController *navRoutes = [[UINavigationController alloc]initWithRootViewController:routes];
        
        [self.navigationController presentViewController:navRoutes animated:YES completion:nil];
        
    }
    
    if (indexPath.row == 2)     {
        ProfileVC *profileView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
        UINavigationController *navProfile = [[UINavigationController alloc] initWithRootViewController:profileView];
        [self.navigationController presentViewController:navProfile animated:YES completion:nil];
        
    }
    
    if (indexPath.row == 3) {
        FeedbackVC *feedback = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
        UINavigationController *navFeedback = [[UINavigationController alloc] initWithRootViewController:feedback];
        [self.navigationController presentViewController:navFeedback animated:YES completion:nil];
    }
    
    if (indexPath.row == 4) {
        NSString *message = @"I'm using Groupool App for trusted carpooling.It's simply an awesome app to share a ride. Give it a Try!(* it's only for corporate employees)"; //this is your text string to share
        //    NSString *shareLink = @"https://play.google.com/sore/apps/details?id=in.groupool";
        NSURL *shareURl = [NSURL URLWithString:@"https://play.google.com/sore/apps/details?id=in.groupool"];
        NSArray *activityItems = @[message,shareURl];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        [self presentViewController:activityVC animated:TRUE completion:nil];
        
        
        NSString *URLString=@"https://play.google.com/sore/apps/details?id=in.groupool";
        
        
    }
    
    if (indexPath.row == 5) {
        SettingsVC *setting = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
        UINavigationController *navSetting = [[UINavigationController alloc] initWithRootViewController:setting];
        [self.navigationController presentViewController:navSetting animated:YES completion:nil];
    }
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
