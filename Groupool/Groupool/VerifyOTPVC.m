//
//  VerifyOTPVC.m
//  Groupool
//
//  Created by HN on 20/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "VerifyOTPVC.h"
#import "LoginVC.h"
#import "UIColor+GroupoolColors.h"
#import "UIView+Toast.h"

@interface VerifyOTPVC ()
{
    
    CALayer *txtOTPborder;
    CGFloat txtOTPborderWidth ;
    CALayer *txtNewPasswordborder;
    CGFloat txtNewPasswordborderWidth ;
    CALayer *txtMobileNumberdborder;
    CGFloat txtMobileNumberborderWidth ;
    UIActivityIndicatorView *indicator;
}

@end
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

CGFloat animatedDistance;
@implementation VerifyOTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _txtMobileNumber.delegate  = self;
    _txtOTP.delegate = self;
    _txtNewPassword.delegate = self;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    
    self.title = @"Forgot Password";
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    _btnSubmit.layer.cornerRadius = 5;
    
    _txtMobileNumber.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"MobNumber"];
    
    _txtMobileNumber.userInteractionEnabled = NO;
    
    
    // Navigationbar Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    // Busy Indicator
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    
    // OTP TextField Customization
    txtMobileNumberdborder = [CALayer layer];
    txtMobileNumberborderWidth = 1.5;
    txtMobileNumberdborder.borderColor = [UIColor lightGrayColor].CGColor;
    txtMobileNumberdborder.frame = CGRectMake(0, _txtMobileNumber.frame.size.height - txtMobileNumberborderWidth, _txtMobileNumber.frame.size.width, _txtMobileNumber.frame.size.height);
    txtMobileNumberdborder.borderWidth = txtMobileNumberborderWidth;
    [_txtMobileNumber.layer addSublayer:txtMobileNumberdborder];
    _txtMobileNumber.layer.masksToBounds = YES;
    
    
    
    // OTP TextField Customization
    txtOTPborder = [CALayer layer];
    txtOTPborderWidth = 1.5;
    txtOTPborder.borderColor = [UIColor lightGrayColor].CGColor;
    txtOTPborder.frame = CGRectMake(0, _txtOTP.frame.size.height - txtOTPborderWidth, _txtOTP.frame.size.width, _txtOTP.frame.size.height);
    txtOTPborder.borderWidth = txtOTPborderWidth;
    [_txtOTP.layer addSublayer:txtOTPborder];
    _txtOTP.layer.masksToBounds = YES;
    
    // New Password TextField Customization
    
    txtNewPasswordborder = [CALayer layer];
    txtNewPasswordborderWidth = 1.5;
    txtNewPasswordborder.borderColor = [UIColor lightGrayColor].CGColor;
    txtNewPasswordborder.frame = CGRectMake(0, _txtNewPassword.frame.size.height - txtNewPasswordborderWidth, _txtNewPassword.frame.size.width, _txtNewPassword.frame.size.height);
    txtNewPasswordborder.borderWidth = txtNewPasswordborderWidth;
    [_txtNewPassword.layer addSublayer:txtNewPasswordborder];
    _txtNewPassword.layer.masksToBounds = YES;
    
}
- (void)backButtonAction {
    
    LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    UINavigationController *navLogin = [[UINavigationController alloc]initWithRootViewController:login];
    [self presentViewController:navLogin animated:YES completion:nil];
}

#pragma mark - Textfield Delegates
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_txtMobileNumber resignFirstResponder];
    if([_txtNewPassword resignFirstResponder])
    {
        txtNewPasswordborder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if ([_txtOTP resignFirstResponder])
    {
        txtOTPborder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == _txtOTP)
    {
        txtOTPborder.borderColor = [UIColor groupoolMainColor].CGColor;
        txtNewPasswordborder.borderColor = [UIColor lightGrayColor].CGColor;
    }
    if (textField == _txtNewPassword)
    {
        txtOTPborder.borderColor = [UIColor lightGrayColor].CGColor;
        txtNewPasswordborder.borderColor = [UIColor groupoolMainColor].CGColor;
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Submit OTP Action
- (IBAction)btnSubmitAction:(id)sender {
    [self.view endEditing:YES];
    
    if ([_txtOTP.text isEqualToString:@""] || [_txtNewPassword.text isEqualToString:@""]) {
        
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        style.messageAlignment = NSTextAlignmentCenter;
        style.cornerRadius = 12;
        [self.view makeToast:@"Please enter information" duration:2 position:CSToastPositionBottom style:style];
        
    
    }
    else{
        [self.view makeToastActivity:CSToastPositionCenter];
        
        @try {
            
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@",FORGOT_PASSWORD_VERIFY_OTP,_txtMobileNumber.text,_txtOTP.text,_txtNewPassword.text]];
           
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response,
                                                       NSData *data, NSError *connectionError)
             {
                 if (data.length > 0 && connectionError == nil)
                 {
                  
                     NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                     if (data == NULL) {
                         
                         CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                         style.messageFont = [UIFont systemFontOfSize:12];
                         style.messageAlignment = NSTextAlignmentCenter;
                         style.cornerRadius = 12;
                         [self.view makeToast:@"Server connection error" duration:3 position:CSToastPositionBottom style:style]
                         ;
                         return ;
                         
                     }
                     
                     if ([dataString isEqualToString:@"0"]) {
                     
                         
                         _txtNewPassword.text = @"";
                         _txtOTP.text = @"";
                         [self.view hideToastActivity];
                         
                         CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                         style.messageFont = [UIFont systemFontOfSize:12];
                         [self.view makeToast:@"Invalid OTP" duration:2 position:CSToastPositionBottom style:style];
                         
                     }
                     else{
                         
                         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isPasswordChanged"];
                         [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"PasswordChanged"];
                         
                         
                         [self.view hideToastActivity];
                         [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"Password"];
                         
                         LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginView"];
                         UINavigationController *navLogin = [[UINavigationController alloc]initWithRootViewController:login];
                         [self presentViewController:navLogin animated:YES completion:nil];
                         
                     }
                     
                 }
                 else
                 {
                     
                     [self.view hideToastActivity];
                     CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
                     style.messageFont = [UIFont systemFontOfSize:12];
                     [self.view makeToast:@"Could not connect to server. Please try again" duration:2 position:CSToastPositionBottom style:style];
                     
                 }
             }];
        }
        @catch (NSException *exception) {
            NSLog(@"%@",exception);
            [self.view hideToastActivity];
        }
    }
    
    }

@end
