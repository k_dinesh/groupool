//
//  MenuTableCell.m
//  Groupool
//
//  Created by Parth Pandya on 02/05/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "MenuTableCell.h"

@implementation MenuTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
