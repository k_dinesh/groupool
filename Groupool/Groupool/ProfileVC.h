//
//  ProfileVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetails.h"

@interface ProfileVC : UIViewController <UITextFieldDelegate>
{
    
    CGFloat animatedDistance;
    
    
    IBOutlet UIButton *btnVerifyMobileNumber;
    IBOutlet UIButton *btnVerifyOfcEmail;
    IBOutlet UIImageView *imgUserPic;
    
    NSString *gender;
    NSString *carType;
    NSString *rideRequested;
    NSString *fullName;
    NSString *mobileNumber;
    NSString *personalEmail;
    NSString *password;
    NSString *officeEmail;
    NSString *loginType;
    NSString *faceBookID;
    NSString *fbFriends;
    NSString *appVersion;
    NSString *country;
    NSString *OS;
    
    NSString *gcmRegID;
    NSString *carDetails;
    NSString *carNumber;
    NSString *officeEmailVerified;
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *optGender;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPersonamEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtOfficeEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtCarDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtCarNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak,nonatomic) IBOutlet UIButton *btnHatchBack;
@property (weak,nonatomic) IBOutlet UIButton *btnSedan;
@property (weak,nonatomic) IBOutlet UIButton *btnSUV;
@property (weak,nonatomic) IBOutlet UIButton *btnOther;

@property (weak, nonatomic) IBOutlet UIButton *btnRequestRide;

@property (strong,nonatomic) UserDetails *user;

- (IBAction)verifyMobileNumber:(id)sender;

- (IBAction)verifyOfficeEmail:(id)sender;



- (IBAction)changePasswordAction:(id)sender;
- (IBAction)logOutAction:(id)sender;

- (IBAction)optGenderAction:(UISegmentedControl *)sender;


- (IBAction)carTypeSelectioAction:(UIButton *)btnType;

@end
