//
//  OfferVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManageGroupsVC.h"
#import "RideDetails.h"
#import "OfferDataModel.h"

@protocol OfferRideDelegate;
@protocol OfferedRideFeedDelegate;

@interface OfferVC : UIViewController<ManageRouteDelegate,UITextFieldDelegate>
{
 
    
    BOOL toggleOn;
    BOOL repeatDays;
    BOOL isDaySelected;
    BOOL isMultipleDays;
    BOOL isMonday;
    BOOL isTuesday;
    BOOL isWednesday;
    BOOL isThursday;
    BOOL isFriday;
    BOOL isSaturday;
    BOOL isSunday;
    BOOL isCalled;
    
   
    __weak IBOutlet UIButton *btnRepeatDays;
    __weak IBOutlet UIButton *moday;
    __weak IBOutlet UIButton *tuesday;
    __weak IBOutlet UIButton *wednesday;
    __weak IBOutlet UIButton *thursday;
    __weak IBOutlet UIButton *friday;
    __weak IBOutlet UIButton *saturday;
    __weak IBOutlet UIButton *sunday;
    __weak IBOutlet UILabel *recommendedPrice;
    
    __weak IBOutlet UISegmentedControl *daySelection;
    
    __weak IBOutlet UIView *dayRepeatView;
    
    
    NSMutableArray *fromGroupsName;
    NSMutableArray *toGroupsName;
    NSMutableArray *fromGroupID;
    NSMutableArray *toGroupID;
    NSMutableArray *defaultRidePrice;
    NSMutableArray *startTimeArray;
    NSMutableArray *routeIDArray;
    NSMutableArray *maxRidePriceArray;
    NSMutableArray *userRidePriceArray;
    NSMutableArray *rideTimeArray;

    // reverse route array objects
    NSMutableArray *reverseRouteArray;
    NSMutableArray *reverseRouteRidePriceArray;
    NSMutableArray *reverseRouteDefaultRidePriceArray;
    NSMutableArray *reverseRouteMaximumRidePriceArray;
    NSMutableArray *reverseRouteFromGroupIDArray;
    NSMutableArray *reverseRouteToGroupIDArray;
    
    NSMutableArray *rideDetailsArray;
    
    CALayer *commentBorder;
    CGFloat commentBorderWidth;
    CGFloat animatedDistance;
    
    NSString *strStartGroupID;
    NSString *strEndGroupID;
    NSString *userID;
    NSString *theDayString;
    NSString *mondayString;
    NSString *tuesdayString;
    NSString *wednesdayString;
    NSString *thursdayString;
    NSString *fridayString;
    NSString *saturdayString;
    NSString *sundayString;
    NSString *tDate;
    NSInteger ridePriceDefault;
    NSInteger ridePriceMaximum;
    NSInteger groupArrayIndex;
  
}

// Integer for string selecter array index
@property (nonatomic, assign) NSInteger selectedIndex;
// Date for timeselector
@property (nonatomic, strong) NSDate *selectedTime;
@property (strong, nonatomic) IBOutlet UIButton *offerRide;
@property (weak, nonatomic) IBOutlet UILabel *lblRidePrice;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectTime;
@property (weak, nonatomic) IBOutlet UITextField *commentText;
@property (nonatomic) BOOL shouldRefresh;
@property RideDetails *ride;

@property (strong,nonatomic) NSString *userSelectedGroup;


@property (assign,nonatomic) id <OfferRideDelegate>oDelegate;


- (IBAction)manageRouteAction:(id)sender;
- (IBAction)selectClicked:(id)sender;

- (IBAction)btnRepeatDays:(id)sender;
- (IBAction)offerRideAction:(id)sender;
- (IBAction)timeSelectorPressed:(id)sender;
- (IBAction)selectDay:(id)sender;
- (IBAction)decreaseRidePrice:(id)sender;
- (IBAction)increaseRidePrice:(id)sender;

@end


@protocol OfferRideDelegate <NSObject>
-(void) refreshTableData:(OfferVC *)refreshTableData;
@end

@protocol OfferedRideFeedDelegate <NSObject>

- (void) refreshFeedTable:(OfferVC *)refreshFeedTable;

@end

