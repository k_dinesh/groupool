//
//  MenuTableCell.h
//  Groupool
//
//  Created by Parth Pandya on 02/05/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuCellImage;
@property (strong, nonatomic) IBOutlet UILabel *menuCellTitle;


@end
