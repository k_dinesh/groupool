//
//  MyRideVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "MyRideVC.h"
#import "AddRouteVC.h"
#import "CustomPagerViewController.h"
#import "UIColor+GroupoolColors.h"
#import "ActionSheetDatePicker.h"
#import "UIView+Toast.h"


@interface MyRideVC ()
{
    
    NSString *userID;
    BOOL isCalled;
    
}

@end

@implementation MyRideVC
@synthesize rideDetails;

- (void)viewDidLoad {
    self.title = @"My Rides";
   
    [super viewDidLoad];
    [self fetchData];
    isCalled = NO;
    
    self.myTableView.dataSource = self;
    self.myTableView.delegate  = self;
    self.myTableView.tableFooterView = [[UIView alloc] init];
    self.myTableView.showsVerticalScrollIndicator = NO;
    self.myTableView.backgroundColor = [UIColor tableBackgroundColor];
    
    self.myTableView.contentInset = UIEdgeInsetsZero;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
}




- (void) backButtonAction {
    CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"];
    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
    [self.navigationController presentViewController:navHome animated:YES completion:nil];
    
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyRidesDataModel *obj = [rideDetails objectAtIndex:indexPath.row];
    
    if ([obj.mySort isEqualToString:@"1"])
    {
        return 117;
    }
    return 86;
}
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath  {
    cell.backgroundColor = [UIColor clearColor];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [rideDetails count];
    
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:indexPath.row];
    
    @try {
        
        MyRideTableViewCellVC *rideCell = [[MyRideTableViewCellVC alloc] init];
        
        
        if ([obj.mySort isEqualToString:@"1"])
           
        {
            
            rideCell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
            
        }
        else
        {
            rideCell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
        }
        
        
        // Ride Date Time Label Customization
        
        NSString *timeString = obj.time;
        NSScanner *timescanner = [NSScanner scannerWithString:timeString];
        int  hour, minutes;
        [timescanner scanInt:&hour];
        [timescanner scanString:@":" intoString:nil];
        [timescanner scanInt:&minutes];

        timeString = [NSString stringWithFormat:@"%02d:%02d ",hour,minutes];
        
        if ([obj.date isEqualToString:@"0"] || (obj.date == nil) || [obj.date isEqualToString:@""] )
        {
            
            rideCell.rideDayAndDate.text = [NSString stringWithFormat:@"%@ %@",obj.days,timeString];
        }
        else{
            
            rideCell.rideDayAndDate.text = [NSString stringWithFormat:@"%@ %@",obj.date,timeString];
        }
        
        // Groupname Label Customization
        
        rideCell.rideGroupName.text = [NSString stringWithFormat:@"%@ > %@",obj.fromGroupLocation,obj.toGroupLocation];

        // Ride Price Label Customization
        rideCell.ridePrice.text = [NSString stringWithFormat:@"Rs. %@",obj.price];
        
        
        // Ride Offered user Name Labe customization
        
        
        rideCell.rideUserName.text = obj.name;
        
        
        
        // Delete button cuatomization
        //        rideCell.btnDeleteRide.tag = [[rideIDArray objectAtIndex:indexPath.row] intValue];
        rideCell.btnDeleteRide.tag = indexPath.row;
        [rideCell.btnDeleteRide addTarget:self action:@selector(deletePressed:) forControlEvents:UIControlEventTouchUpInside];
        
        // Edit Ride Time Button customization
        
        rideCell.btnEditTime.tag = indexPath.row;
        [rideCell.btnEditTime addTarget:self action:@selector(editRideTime:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return rideCell;
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@",exception.description);
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Popup view
    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:indexPath.row];
    
    if ([obj.mySort isEqualToString:@"2"])
    {
        CGFloat viewHeight;
        CGFloat viewWidth;
        CGFloat viewX;
        CGFloat viewY;
        // popup view dimensions
        viewHeight = (self.view.frame.size.height/2) + (self.view.frame.size.height/7);
        viewWidth  = (self.view.frame.size.width/2) + (self.view.frame.size.width/3);
        viewX = (self.view.frame.size.width/12);
        viewY = (self.view.frame.size.height/10 + 20);
        
        self.customView = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
        
        self.customView.backgroundColor = [UIColor whiteColor];
        // popup View Shadow
        self.customView.layer.cornerRadius = 3;
        self.customView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.customView.layer.shadowOpacity = 0.9;
        self.customView.layer.shadowRadius = 12;
        self.customView.layer.shadowOffset = CGSizeMake(18.0f, 18.0f);
        self.customView.tag = 100;
        
        CGFloat allControlYPos = (self.customView.frame.size.height) - (self.customView.frame.size.height);
        
        CGFloat allControlXPos = (self.customView.frame.size.width) - (self.customView.frame.size.width);
        
        
        // Ride Time Label
        NSString *timeString = obj.time;
        NSScanner *timescanner = [NSScanner scannerWithString:timeString];
        int  hour, minutes;
        [timescanner scanInt:&hour];
        [timescanner scanString:@":" intoString:nil];
        [timescanner scanInt:&minutes];
        timeString = [NSString stringWithFormat:@"%02d:%02d ",hour,minutes];
        
        UILabel *rideTime = [[UILabel alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+46, self.customView.frame.size.width - 100, 44)];
        rideTime.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
        rideTime.textColor = [UIColor groupoolMainColor];
        rideTime.numberOfLines = 2;
        rideTime.lineBreakMode = NSLineBreakByWordWrapping;
        
        rideTime.text = [NSString stringWithFormat:@"%@ %@",obj.date,timeString];
        
        // Ride price label
        UILabel *ridePrice = [[UILabel alloc]initWithFrame:CGRectMake(self.customView.frame.size.width - 80, allControlYPos+57, 70, 20)];
        ridePrice.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
        ridePrice.textColor = [UIColor groupoolMainColor];
        ridePrice.numberOfLines = 2;
        ridePrice.lineBreakMode = NSLineBreakByWordWrapping;
        ridePrice.text = [NSString stringWithFormat:@"Rs. %@",obj.price];
        
        
        // Ride Locations(From and To) Label
        UILabel *popupFromRideLocations = [[UILabel alloc] initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+96, self.customView.frame.size.width - 20, 100)];
        
        popupFromRideLocations.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        popupFromRideLocations.textColor = [UIColor grayTextColor];
        popupFromRideLocations.lineBreakMode = NSLineBreakByWordWrapping;
        popupFromRideLocations.numberOfLines = 0;
        
        // User Pic Imageview
        UIImageView *popupUserImage = [[UIImageView alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+198, 52, 52)];
        popupUserImage.layer.cornerRadius = 26;
        popupUserImage.layer.masksToBounds = YES;
        
        
        // User Name Label
        UILabel *popupUserName = [[UILabel alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+252, 95, 60)];
        popupUserName.font = [UIFont systemFontOfSize:17];
        popupUserName.numberOfLines = 3;
        popupUserName.lineBreakMode = NSLineBreakByWordWrapping;
        popupUserName.textColor = [UIColor groupoolMainColor];
        popupUserName.textAlignment = NSTextAlignmentLeft;
        
        //Works At Label
        UILabel *popupCompanyName = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 205, allControlYPos+215, 198, 35)];
        popupCompanyName.numberOfLines = 2;
        popupCompanyName.lineBreakMode = NSLineBreakByCharWrapping;
        popupCompanyName.textColor = [UIColor grayTextColor];
        popupCompanyName.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        popupCompanyName.textAlignment = NSTextAlignmentRight;
        
        //Mobile Verified Label
        UILabel *popupMobileVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 142, allControlYPos+252, 135, 15)];
        popupMobileVerified.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        popupMobileVerified.textColor = [UIColor grayTextColor];
        popupMobileVerified.textAlignment = NSTextAlignmentRight;
        
        
        //Office Email Verified
        UILabel *popupOfficeEmailVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 162, allControlYPos+269, 155, 15)];
        popupOfficeEmailVerified.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        popupOfficeEmailVerified.textColor = [UIColor grayTextColor];
        popupOfficeEmailVerified.textAlignment = NSTextAlignmentRight;
       
        
        //Comment From car owner Label
        
        // OK Button
        
        
        UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(25, self.customView.frame.size.height - 40, 50, 30)];
        
        btnOK.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        btnOK.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnOK setTitle:@"DONE" forState:UIControlStateNormal];
        [btnOK setTitleColor:[UIColor groupoolMainColor] forState:UIControlStateNormal];
        
        [btnOK addTarget:self action:@selector(closePopup:)  forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *btnCALL = [[UIButton alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - self.customView.frame.size.width/2, self.customView.frame.size.height - 40, 50, 30)];
        btnCALL.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnCALL setTitle:@"CALL" forState:UIControlStateNormal];
        [btnCALL setTitleColor:[UIColor groupoolMainColor] forState:UIControlStateNormal];
        [btnCALL setTag:indexPath.row];
        [btnCALL addTarget:self action:@selector(callUserPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *btnSMS = [[UIButton alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - self.customView.frame.size.width/4, self.customView.frame.size.height - 40, 50, 30)];
        btnSMS.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnSMS setTitle:@"SMS" forState:UIControlStateNormal];
        [btnSMS setTitleColor:[UIColor groupoolMainColor] forState:UIControlStateNormal];
        [btnSMS setTag:indexPath.row];
        [btnSMS addTarget:self action:@selector(smsUserPressed:) forControlEvents:UIControlEventTouchUpInside];

        
        // Add popup controls to popupview
        [self.customView addSubview:rideTime];
        [self.customView addSubview:ridePrice];
        [self.customView addSubview:popupFromRideLocations];
        
        [self.customView addSubview:popupUserImage];
        [self.customView addSubview:popupUserName];
        [self.customView addSubview:popupCompanyName];
        [self.customView addSubview:popupMobileVerified];
        [self.customView addSubview:popupOfficeEmailVerified];
        
        [self.customView addSubview:btnOK];
        [self.customView addSubview:btnCALL];
        [self.customView addSubview:btnSMS];
        
        
        popupFromRideLocations.text = [NSString stringWithFormat:@"%@ \n>>\n%@",obj.fromGroupLocation,obj.toGroupLocation];
        
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSURL *imageURL = [NSURL URLWithString:obj.profilePic];
            NSData *imageData = [NSData dataWithContentsOfURL: imageURL];
            
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    popupUserImage.image = image;
                    
                    
                });
            }
        });

        
        popupUserName.text = [NSString stringWithFormat:@"%@ (%@)",obj.name,obj.gender];
        popupCompanyName.text = [NSString stringWithFormat:@"Works at: %@",obj.company];
        if ([obj.smsVerified isEqualToString:@"1"]) {
            popupMobileVerified.text = @"Mobile Verified: Yes";
        }else{
            popupMobileVerified.text = @"Mobile Verified: No";
        }
        if ([obj.officeEmailVerified isEqualToString:@"1"]) {
            popupOfficeEmailVerified.text = @"Office Email Verified:Yes";
        }else{
            popupOfficeEmailVerified.text = @"Office Email Verified:No";
        }
        
        // Show custom view animated
        self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        [self.view addSubview:self.customView];
        [UIView animateWithDuration:0.1
                         animations:
         ^{
             self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
             self.myTableView.userInteractionEnabled = NO;
             self.navigationController.navigationBar.userInteractionEnabled = NO;
         }];
        
    }
    else {
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"You have Offered this ride. Click Edit to make changes" duration:2.0 position:CSToastPositionBottom style:style];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void) closePopup:(id)sender {
    
    
    
    self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.5
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
         self.myTableView.userInteractionEnabled = YES;
         self.navigationController.navigationBar.userInteractionEnabled = YES;
     }
     ];
    
    [self.customView removeFromSuperview];
    
}

#pragma mark - Popup button action implementation
- (void)callUserPressed:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    
  
    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:button.tag];
    
    if ([obj.officeEmailVerified isEqualToString:@"1"]
        && [obj.smsVerified isEqualToString:@"1"])
    {
        
        NSString *userMobileNumber = obj.mobile;
        userMobileNumber = [NSString stringWithFormat:@"+91%@",userMobileNumber];
        //    NSString *phNo = @"+919876543210";
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",userMobileNumber]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            NSLog(@"Call facility not available");
        }
    }
    else
    {
        
        
        NSString *alertMessage;
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}
- (void)smsUserPressed:(id)sender {
    
    
    UIButton *button = (UIButton*)sender;
    
  
    MyRidesDataModel *obj = [rideDetails objectAtIndex:button.tag];
    
    
    if ([obj.officeEmailVerified isEqualToString:@"1"]
        && [obj.smsVerified isEqualToString:@"1"])
    {
        NSString *userMobileNumber = obj.mobile;
        userMobileNumber = [NSString stringWithFormat:@"sms:+91%@",userMobileNumber];
        
  
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:userMobileNumber]];
    }
    else
    {
        
        
        NSString *alertMessage;
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
        }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
            alertMessage = @"For the Security reason, please complete Mobile verification.";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
            UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
            [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
            
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:actionGoTo];
        [alert addAction:actionCancel];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}
- (void)deletePressed:(id)sender {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@""
                                message:@"Sure want to delete this ride ?"
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                             @try {
                                 UIButton *button = (UIButton *)sender;
                                 [self.view makeToastActivity:CSToastPositionCenter];
                                 MyRidesDataModel *obj = [rideDetails objectAtIndex:button.tag];
                                 NSString *rideID = obj.offeredRideID;
                                 NSURL *url;
                                 url =[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",MY_RIDES_DELETE_OFFERED_RIDE,rideID]];
                                 NSURLRequest *request = [NSURLRequest requestWithURL:url];
                                 [NSURLConnection sendAsynchronousRequest:request
                                                                    queue:[NSOperationQueue mainQueue]
                                                        completionHandler:^(NSURLResponse *response,
                                                                            NSData *data, NSError *connectionError)
                                  {
                                      if ((data.length > 0) && (connectionError == nil))
                                      {
                                          
                                          [rideDetails removeObjectAtIndex:button.tag];
                                          
                                          [self checkforUpdate];
                                          [self.view hideToastActivity];
                                          
                                      }
                                      else
                                      {
                                          
                                          [self.view hideToastActivity];
                                          
                                          CSToastStyle *style =[[CSToastStyle alloc] initWithDefaultStyle];
                                          
                                          style.messageFont = [UIFont systemFontOfSize:12];
                                          [self.view makeToast:@"Please try again." duration:1.0 position:CSToastPositionBottom style:style];
                                          
                                          
                                      }
                                  }];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }
                             @catch (NSException *exception) {
                                 [self.view hideToastActivity];
                                 NSLog(@"Exception:%@",exception);
                             }
                         }];
    
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                                 [self.view hideToastActivity];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:Cancel];
    [alert addAction:OK];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)editRideTime: (id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:button.tag];
    
    [[NSUserDefaults standardUserDefaults] setValue:obj.offeredRideID forKey:@"OfferedRideID"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    
    self.selectedTime = [dateFormatter dateFromString:obj.time];
  
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
    
    
    ActionSheetDatePicker *aDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Time" datePickerMode:UIDatePickerModeTime  selectedDate:self.selectedTime target:self action:@selector(timeWasSelected:element:) origin:sender cancelAction:@selector(cancelTimeSelection:)];
    aDatePicker.minuteInterval = minuteInterval;
    
    [aDatePicker showActionSheetPicker];
    [aDatePicker setLocale:[NSLocale systemLocale]];
    
    
}

- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    self.selectedTime = selectedTime;
    UIButton *button = (UIButton *)element;

    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:button.tag];
    
    NSString *createdTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSDateFormatter *rideDateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [rideDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"HH:mm:ss"];

    [rideDateFormatter setDateFormat:@"dd MM yyyy"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *tDate = [rideDateFormatter stringFromDate:now];

    NSDate *rideDate = [rideDateFormatter dateFromString:obj.date];
    NSString *rDate = [rideDateFormatter stringFromDate:rideDate];

    createdTime = [dateFormatter stringFromDate:selectedTime];
 
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    
    
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    
    if(formStTime < nowTime && [tDate isEqualToString:rDate]) {
  
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionBottom style:style];
        
    }
    else
    {
     
    [self editOfferedRide:createdTime index:button.tag];
    }
    
    
}
- (void)editOfferedRide:(NSString *)changedTime index:(NSInteger)index {
    
    
    NSString *alertMessage;
    NSString *rideDate;
    
    MyRidesDataModel *obj = [rideDetails objectAtIndex:index];
   
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Sure want to Edit Ride?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //    editOfferedRide
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@",MY_RIDES_EDIT_OFFERED_RIDE,obj.offeredRideID,changedTime]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             
             if (data.length > 0 && connectionError == nil) {
                 
                 NSString *dataString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                 if ([dataString isEqualToString:@"1"])
                 {
                     [self fetchData];
                     
                 }
             }
             
         }];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    
    
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
}
- (void)cancelTimeSelection:(id)sender {
    NSLog(@"Time selection canceled..");
}

#pragma mark - Get Data from server
- (void)fetchData {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    {
        userID = [defaults valueForKey:@"UserID"];
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",MY_RIDES_GETMYRIDES,userID]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
  
                 
                 if (jsonArray != nil) {
                     
                     rideDetails = [[NSMutableArray alloc] init];
                     
                     for (NSDictionary *rides in jsonArray)
                     {
                         
                         MyRidesDataModel *obj = [[MyRidesDataModel alloc] init];
                         
                         
                         obj.mySort = rides[@"mysort"];
                         obj.offeredRideID = rides[@"offered_ride_id"];
                         obj.days = rides[@"days"];
                         obj.date = rides[@"date"];
                         obj.time = rides[@"time"];
                         obj.startGroupID = rides[@"start_group_id"];
                         obj.endGroupID = rides[@"end_group_id"];
                         obj.price = rides[@"price"];
                         obj.joinedRideID = rides[@"joined_ride_id"];
                         obj.userID = rides[@"user_id"];
                         obj.fromGroupLocation = rides[@"from_group_location"];
                         obj.fromGroupName = rides[@"from_group_name"];
                         obj.toGroupLocation = rides[@"to_group_location"];
                         obj.toGroupName = rides[@"to_group_name"];
                         obj.name = rides[@"name"];
                         obj.gender = rides[@"gender"];
                         obj.mobile = rides[@"mobile"];
                         obj.profilePic = rides[@"profile_pic"];
                         obj.smsVerified = rides[@"sms_verified"];
                         obj.emailVerified = rides[@"email_verified"];
                         obj.officeEmailVerified = rides[@"office_email_verified"];
                         obj.facebookID = rides[@"facebook_id"];
                         obj.fbFriends = rides[@"fb_friends"];
                         obj.company = rides[@"company"];
                         
                         [rideDetails addObject:obj];
                         
                     }
                     [self.myTableView reloadData];
                     
                 }
                 [self checkforUpdate];
                 [self.view hideToastActivity];
                 
             }
             else
             {
       
                 
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self.view hideToastActivity];
                     [self reconnectToServer];
                     
                 }];
                 [alert addAction:alertAction];
                 
                 [self presentViewController:alert animated:YES completion:nil];
                 
                 
             }
         }];
    }
}
- (void)reconnectToServer {
    
    if (isCalled) {
        [self fetchData];
        isCalled = YES;
    }
    
}
- (void)checkforUpdate {
    
    
    
    BOOL arrayIsEmpty = ([rideDetails count] == 0);
    
    if (!arrayIsEmpty) {
        
        self.myTableView.hidden = NO;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_myTableView reloadData];
        });
        
    }else{
        self.myTableView.hidden = YES;
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height/2, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"You don’t have any Offered or Joined rides yet.";
        messageLabel.textColor = [UIColor grayTextColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        
        [messageLabel sizeToFit];
        [self.view addSubview:messageLabel];
    }
    
}


@end
