//
//  MenuVC.h
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPagerViewController.h"
#import "ProfileVC.h"
#import "MyRideVC.h"
#import "FeedbackVC.h"
#import "SettingsVC.h"
#import "UIColor+GroupoolColors.h"
#import "ManageGroupsVC.h"
#import "MyChatsVC.h"
#import "UIView+Toast.h"
#import "MenuTableCell.h"

@interface MenuVC : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
   
   
}
@property (strong, nonatomic) IBOutlet UITableView *menuTable;

@property (strong,nonatomic) NSMutableArray *imageArray;
@property (strong,nonatomic) NSMutableArray *titleArray;
@property (strong,nonatomic) UIActivityViewController *activityViewController;



@end
