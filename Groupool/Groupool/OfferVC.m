//
//  OfferVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "OfferVC.h"
#import "MyRideVC.h"
#import "CustomPagerViewController.h"
#import "ManageGroupsVC.h"
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"
#import "UIColor+GroupoolColors.h"
#import "ProfileVC.h"
#import "UIView+Toast.h"
#import "CarDetailsVC.h"



@interface OfferVC ()

@end

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@implementation OfferVC
@synthesize btnSelect;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
    [self fetchData];
    toggleOn = YES;
    repeatDays = YES;
    
    _ride = [[RideDetails alloc] init];
    isMonday = YES;
    isTuesday = YES;
    isThursday = YES;
    isWednesday = YES;
    isThursday = YES;
    isFriday = YES;
    isSaturday = YES;
    isSunday = YES;
    
    isCalled = NO;
    
   
    
    _offerRide.layer.cornerRadius= 5;
    self.commentText.delegate = self;
    
    dayRepeatView.layer.cornerRadius = 3;
    dayRepeatView.hidden = TRUE;
    
    [daySelection addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    btnSelect.layer.cornerRadius = 2;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    commentBorder = [CALayer layer];
    commentBorderWidth = 1.5;
    commentBorder.borderColor = [UIColor lightGrayColor].CGColor;
    
    commentBorder.frame = CGRectMake(0, _commentText.frame.size.height - commentBorderWidth,_commentText.frame.size.width,_commentText.frame.size.height);
    commentBorder.borderWidth = commentBorderWidth;
    [_commentText.layer addSublayer:commentBorder];
    _commentText.layer.masksToBounds =  YES;
    
    
    self.selectedTime = [[NSDate new] dateByAddingTimeInterval:2700];
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
    
    
    NSDateFormatter *formatter;
    NSString *createdTime;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    createdTime = [formatter stringFromDate:self.selectedTime];
    
    [self.btnSelectTime setTitle:createdTime forState:UIControlStateNormal];
    
    self.lblRidePrice.text = @"Rs.00";
    [self checkForUpdatedData];
    
}

#pragma mark - Load/Refresh Data from API
- (void)fetchData {
    [self.view makeToastActivity:CSToastPositionCenter];
    @try {
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_USER_ROUTE_URL,userID]];
        
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                 fromGroupsName = [[NSMutableArray alloc] init];
                 toGroupsName = [[NSMutableArray alloc] init];
                 fromGroupID = [[NSMutableArray alloc] init];
                 toGroupID = [[NSMutableArray alloc] init];
                 defaultRidePrice = [[NSMutableArray alloc] init];
                 startTimeArray = [[NSMutableArray alloc] init];
                 routeIDArray = [[NSMutableArray alloc]init];
                 maxRidePriceArray = [[NSMutableArray alloc]init];
                 userRidePriceArray = [[NSMutableArray alloc]init];
                 rideTimeArray = [[NSMutableArray alloc] init];
                 
                 rideDetailsArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *routes in jsonArray)
                 {
                  
                     OfferDataModel *obj = [OfferDataModel getInstance];
                     
                     [fromGroupsName addObject:routes[@"from_group_name"]];
                     [toGroupsName addObject:routes[@"to_group_name"]];
                     [routeIDArray addObject:routes[@"id"]];
                     [defaultRidePrice addObject:routes[@"default_price"]];
                     [fromGroupID addObject:routes[@"from_group_id"]];
                     [toGroupID addObject:routes[@"to_group_id"]];
                     [userRidePriceArray addObject:routes[@"user_price"]];
                     [maxRidePriceArray addObject:routes[@"max_price"]];
                     [rideTimeArray addObject:routes[@"modified_time"]];
                     
                     obj.fromGroupName = routes[@"from_group_name"];
                     obj.toGroupName = routes[@"to_group_name"];
                     obj.routeID = routes[@"id"];
                     obj.defaultRidePrice = routes[@"default_price"];
                     obj.fromGroupID = routes[@"from_group_id"];
                     obj.toGroupID = routes[@"to_group_id"];
                     obj.userRidePrice = routes[@"user_price"];
                     obj.maxRidePrice = routes[@"max_price"];
                     obj.rideTime = routes[@"modified_time"];
                    
                     [rideDetailsArray addObject:obj];
                     
                 }
                 
                 [self checkForUpdatedData];
                 [self.view hideToastActivity];
             }
             else
             {
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not connect to server" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self.view hideToastActivity];
                     [self reFetchData];
                     
                 }];
                 [alert addAction:alertAction];
                 
                 [self presentViewController:alert animated:YES completion:nil];
                 
             }
         }];
        
    }
    @catch (NSException *exception) {
        [self.view hideToastActivity];
        NSLog(@"%@",exception);
    }
    
  }
- (void) reFetchData {
    if (isCalled) {
        [self fetchData];
        isCalled = YES;
    }
    
}
- (void)checkForUpdatedData {
    
    BOOL fromGrpisEmpty = ([fromGroupsName count] == 0);
    BOOL fromGrpIDisEmpty = ([fromGroupID count] == 0);
    BOOL toGrpisEmpty = ([toGroupsName count] == 0);
    BOOL toGrpIDisEmpty = ([toGroupID count] == 0);
    BOOL defalutRidePriceisEmpty = ([defaultRidePrice count] == 0);
    BOOL timeArrisEmpty = ([rideTimeArray count] == 0);
    
    if (fromGrpisEmpty) {
        
        [self fetchData];
    }
    if (!fromGrpisEmpty && !toGrpisEmpty) {
        
        NSString *tmpStr = [NSString stringWithFormat:@"%@ > %@",(fromGroupsName)[(NSUInteger) 0],(toGroupsName)[(NSUInteger) 0]];
        [btnSelect setTitle:tmpStr forState:UIControlStateNormal];
        
    }
    if (!defalutRidePriceisEmpty) {
        ridePriceDefault = [(defaultRidePrice)[(NSUInteger) 0] integerValue];
        ridePriceMaximum = [(maxRidePriceArray) [(NSUInteger) 0] integerValue];
        self.lblRidePrice.text = [NSString stringWithFormat:@"Rs.%@",(defaultRidePrice)[(NSUInteger) 0]];
        recommendedPrice.text = [NSString stringWithFormat:@"Rs. %ld Recommended price (per Rider)",(long)ridePriceDefault];
    }
    if (!fromGrpIDisEmpty && !toGrpIDisEmpty)
    {
        strStartGroupID = [NSString stringWithFormat:@"%@",(fromGroupID)[(NSUInteger) 0]];
        strEndGroupID = [NSString stringWithFormat:@"%@",(toGroupID)[(NSUInteger) 0]];
    }
    
    if (!timeArrisEmpty)
    {
        
       
        self.selectedTime = [[NSDate date] dateByAddingTimeInterval:2700];
        NSInteger minuteInterval = 5;
        //clamp date
        NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
        NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
        NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
        
        if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
            timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
        }
        
        self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
        
       
        NSDateFormatter *formatter;
        NSString *createdTime;
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        createdTime = [formatter stringFromDate:self.selectedTime];
        
        [self.btnSelectTime setTitle:createdTime forState:UIControlStateNormal];
       
        
    }
    
}

#pragma mark - Refresh Dropdown data delegate
/*********** Refresh Data Delegate method ***************/
- (void)refreshDropdown:(ManageGroupsVC *)refreshData {
    [self fetchData];
    [self checkForUpdatedData];
}
/*******************************************************/

#pragma mark - Textfield Delegate methods
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if([_commentText resignFirstResponder])
        commentBorder.borderColor = [UIColor lightGrayColor].CGColor;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    if (textField == _commentText) {
        commentBorder.borderColor = [UIColor groupoolMainColor].CGColor;
        
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Gender Selection
- (IBAction)segmentAction:(UISegmentedControl *)sender {
    theDayString = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
    isDaySelected = YES;
    
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isDayRepeated"];
    if (sender.selectedSegmentIndex == 0) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *now = [[NSDate alloc] init];
        tDate = [dateFormat stringFromDate:now];
        
    }else{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *now = [[NSDate alloc] init];
        int daysToAdd = 1;
        NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
        tDate = [dateFormat stringFromDate:tomorrowsDate];
        
    }
    
    
}

#pragma mark - Repeat Days Actions
- (IBAction)btnRepeatDays:(id)sender {
    
    if(repeatDays){
        dayRepeatView.hidden = FALSE;
        daySelection.hidden = TRUE;
        
    }
    else {
        dayRepeatView.hidden = TRUE;
        daySelection.hidden = FALSE;

    }
    repeatDays = !repeatDays;
    [btnRepeatDays setImage:[UIImage imageNamed:repeatDays ? @"un-selected.png" :@"selected.png"] forState:UIControlStateNormal];
   
    
    
}
- (IBAction)selectDay:(id)sender {
    UIButton *dayButtons = (UIButton *)sender;
    isDaySelected = YES;
    
    switch (dayButtons.tag)
    {
            
        case 0:
            if(isMonday){
                mondayString = @"Mon";
            }
            else {
                mondayString = @"";
            }
            isMonday = !isMonday;
            
            [moday setBackgroundColor:isMonday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            break;
            
        case 1:
            if(isTuesday){
                tuesdayString = @"Tue";
                
            }
            else {
                tuesdayString = @"";
            }
            
            isTuesday = !isTuesday;
            [tuesday setBackgroundColor:isTuesday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            
            break;
            
        case 2:
            if(isWednesday){
                wednesdayString = @"Wed";
            }
            else {
                wednesdayString = @"";
            }
            isWednesday = !isWednesday;
            [wednesday setBackgroundColor:isWednesday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            
            break;
            
        case 3:
            if(isThursday){
                thursdayString = @"Thu";
                
            }
            else {
                thursdayString = @"";
            }
            isThursday = !isThursday;
            [thursday setBackgroundColor:isThursday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            break;
            
        case 4:
            if(isFriday){
                fridayString = @"Fri";
                
            }
            else {
                fridayString = @"";
            }
            isFriday = !isFriday;
            [friday setBackgroundColor:isFriday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            break;
            
        case 5:
            if(isSaturday){
                saturdayString = @"Sat";
                
            }
            else {
                saturdayString = @"";
                
            }
            isSaturday = !isSaturday;
            [saturday setBackgroundColor:isSaturday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            break;
            
        case 6:
            if(isSunday){
                sundayString = @"Sun";
                
            }
            else {
                sundayString = @"";
                
            }
            isSunday = !isSunday;
            [sunday setBackgroundColor:isSunday ? [UIColor lightGrayColor]:[UIColor groupoolMainColor]];
            break;
            
    }
    
    
    
}
- (NSString *)getDaysString {
    
    NSString *tmpString;
    
    if ([mondayString isEqualToString:@""] || mondayString == nil) {
        
        // Blank String
        
    }else{
        tmpString = [NSString stringWithFormat:@"%@",mondayString];
    }
    
    if ([tuesdayString isEqualToString:@""] || tuesdayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,tuesdayString];

    }
    
    if ([wednesdayString isEqualToString:@""] || wednesdayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,wednesdayString];

    }
    
    if ([thursdayString isEqualToString:@""] || thursdayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,thursdayString];

    }
    
    if ([fridayString isEqualToString:@""] || fridayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,fridayString];

    }
    
    if ([saturdayString isEqualToString:@""] || saturdayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,saturdayString];

    }
    
    if ([sundayString isEqualToString:@""] || sundayString == nil) {
        // Blank String
    } else {
        tmpString = [NSString stringWithFormat:@"%@,%@",tmpString,sundayString];

    }
    NSString *undesired = @"(null),";
    
    tmpString =  [tmpString stringByReplacingOccurrencesOfString:undesired
                                                               withString:@""];
    return tmpString;
    
}


#pragma mark - Select Group Dropdown Action
- (IBAction)selectClicked:(id)sender {
    
    NSString *tmpObject;
    NSString *tmpObject1;
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    reverseRouteArray = [[NSMutableArray alloc] init];
    reverseRouteRidePriceArray = [[NSMutableArray alloc] init];
    reverseRouteDefaultRidePriceArray= [[NSMutableArray alloc] init];
    reverseRouteMaximumRidePriceArray = [[NSMutableArray alloc] init];
    reverseRouteFromGroupIDArray = [[NSMutableArray alloc] init];
    reverseRouteToGroupIDArray = [[NSMutableArray alloc] init];
    
    for (int i=0;i<[fromGroupsName count];i++)
    {
       
        // From - To group name
        tmpObject=[NSString stringWithFormat:@"%@ > %@",
                   [fromGroupsName objectAtIndex:i],
                   [toGroupsName objectAtIndex:i]];
        tmpObject1 = [NSString stringWithFormat:@"%@ > %@",
                      [toGroupsName objectAtIndex:i],
                      [fromGroupsName objectAtIndex:i]];
        
        
        [newArray addObject:tmpObject];
        [newArray addObject:tmpObject1];
        [reverseRouteArray addObject:tmpObject];
        [reverseRouteArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        //  Ride Price
        tmpObject = [defaultRidePrice objectAtIndex:i];
        tmpObject1 = tmpObject;
        [reverseRouteRidePriceArray addObject:tmpObject];
        [reverseRouteRidePriceArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        // Default Ride Price
        tmpObject = [defaultRidePrice objectAtIndex:i];
        tmpObject1 = tmpObject;
        [reverseRouteDefaultRidePriceArray addObject:tmpObject];
        [reverseRouteDefaultRidePriceArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        // Maximum Ride Price
        tmpObject = [maxRidePriceArray objectAtIndex:i];
        tmpObject1 = tmpObject;
        [reverseRouteMaximumRidePriceArray addObject:tmpObject];
        [reverseRouteMaximumRidePriceArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        // StartGroup ID
        tmpObject = [fromGroupID objectAtIndex:i];
        tmpObject1 = [toGroupID objectAtIndex:i];
        [reverseRouteFromGroupIDArray addObject:tmpObject];
        [reverseRouteFromGroupIDArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
        // EndGroup ID
        tmpObject = [toGroupID objectAtIndex:i];
        tmpObject1 = [fromGroupID objectAtIndex:i];
        [reverseRouteToGroupIDArray addObject:tmpObject];
        [reverseRouteToGroupIDArray addObject:tmpObject1];
        tmpObject=nil;
        tmpObject1=nil;
        
    }

    if (!([_userSelectedGroup isEqualToString:@""]) && (_userSelectedGroup != nil)) {
   
        [self.btnSelect setTitle:_userSelectedGroup forState:UIControlStateNormal];
        
    }
    

    [ActionSheetStringPicker showPickerWithTitle:@"Select Route" rows:reverseRouteArray initialSelection:self.selectedIndex target:self successAction:@selector(groupSelected:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];

}
- (void)groupSelected:(NSNumber *)selectedIndex element:(id)element {
   
    self.selectedIndex = [selectedIndex intValue];
  
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    
    btnSelect.titleLabel.text = (reverseRouteArray)[(NSUInteger) self.selectedIndex];
   
    _userSelectedGroup = (reverseRouteArray)[(NSUInteger) self.selectedIndex];

    self.lblRidePrice.text = [NSString stringWithFormat:@"Rs.%@",(reverseRouteRidePriceArray)[(NSUInteger) self.selectedIndex]];
    ridePriceMaximum = [(reverseRouteMaximumRidePriceArray)[(NSUInteger) self.selectedIndex] integerValue];
    ridePriceDefault = [(reverseRouteDefaultRidePriceArray)[(NSUInteger) self.selectedIndex] integerValue];
    strStartGroupID = [NSString stringWithFormat:@"%@",(reverseRouteFromGroupIDArray)[(NSUInteger) self.selectedIndex]];
    strEndGroupID = [NSString stringWithFormat:@"%@",(reverseRouteToGroupIDArray)[(NSUInteger) self.selectedIndex]];
    recommendedPrice.text = [NSString stringWithFormat:@"Rs. %ld Recommended price (per Rider)",(long)ridePriceDefault];
    
}
- (void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Offer Ride Action
- (IBAction)offerRideAction:(id)sender {
    
    
    
    NSString *createdTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    createdTime = [dateFormatter stringFromDate:self.selectedTime];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    
    
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    if(formStTime < nowTime && (daySelection.selectedSegmentIndex == 0) && (daySelection.hidden == NO)) {
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionCenter style:style];
        return;
        
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  
    _ride.startGrpID = strStartGroupID;
    _ride.womenOnly = @"0";
    _ride.isActice = @"1";
    _ride.carNumber = [defaults valueForKey:@"CarNumber"];
    _ride.carType = [defaults valueForKey:@"CarType"];
    _ride.carDetails = [defaults valueForKey:@"CarDetails"];
    
    
    _ride.editedID = @"-1";
    _ride.offeredSeat = @"3";
    _ride.userID  = userID;
    _ride.time = self.btnSelectTime.titleLabel.text;
    _ride.price = [[self.lblRidePrice.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    
    _ride.endGrpID = strEndGroupID;
    _ride.comment = _commentText.text;
    _ride.phpID = @"0";
    _ride.OS = @"I";
    _ride.rideID = @"0";
    
    if (isDaySelected) {
        _ride.dayString = [self getDaysString];
        
    }else{
        _ride.dayString = theDayString;
        _ride.theDate = tDate ;
    }
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [[NSDate alloc] init];
    
    if ([_ride.dayString isEqualToString:@""] || (_ride.dayString == nil && [btnRepeatDays.currentImage isEqual:[UIImage imageNamed:@"un-selected.png"]])) {
        
        if (daySelection.selectedSegmentIndex == 0)
        {
            tDate = [dateFormat stringFromDate:now];
            _ride.theDate = tDate;
            
        }
        else
        {
            int daysToAdd = 1;
            NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
            tDate = [dateFormat stringFromDate:tomorrowsDate];
            _ride.theDate = tDate;
            
        }
        
    }

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"CarDetails"] isEqualToString:@""]
             || [[[NSUserDefaults standardUserDefaults] valueForKey:@"CarType"] isEqualToString:@""]
             || [[[NSUserDefaults standardUserDefaults] valueForKey:@"CarNumber"] isEqualToString:@""])
    {
      
        CarDetailsVC  *modalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CarDetailsVC"];
      
        modalVC.userID = _ride.userID;
        modalVC.startGrpID = _ride.startGrpID;
        modalVC.endGrpID = _ride.endGrpID;
        modalVC.womenOnly = _ride.womenOnly;
        modalVC.isActice = _ride.isActice;
        modalVC.carNumber = _ride.carNumber;
        modalVC.carType = _ride.carType;
        modalVC.carDetails = _ride.carDetails;
        modalVC.editedID = _ride.editedID;
        modalVC.offeredSeat = _ride.offeredSeat;
        modalVC.time = _ride.time;
        modalVC.price = _ride.price;
        modalVC.comment = _ride.comment;
        modalVC.phpID = _ride.phpID;
        modalVC.OS = _ride.OS;
        modalVC.rideID = _ride.rideID;
        modalVC.dayString = _ride.dayString;
        modalVC.theDate = _ride.theDate;
        
        UINavigationController *navModal = [[UINavigationController alloc] initWithRootViewController:modalVC];
        
        [self.navigationController presentViewController:navModal animated:YES completion:nil];
        
        
    }
     else
     {
         
         
         if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"1"]
             && [[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"1"])
         {
             
             if ([btnRepeatDays.currentImage isEqual:[UIImage imageNamed:@"selected.png"]] && _ride.dayString == nil)
             {
                 [self.view hideToastActivity];
                 
                 CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle ];
                 style.messageFont = [UIFont systemFontOfSize:12];
                 
                 [self.view makeToast:@"Please select ride day!" duration:2.0 position:CSToastPositionBottom style:style];
                 
             }
             else
             {
                 
                 
                 @try {
                     
                     NSMutableDictionary *dataDict = [NSMutableDictionary new];
                     
                     [dataDict setValue:_ride.userID forKey:@"created_by"];
                     [dataDict setValue:_ride.startGrpID forKey:@"start_group_id"];
                     [dataDict setValue:_ride.womenOnly forKey:@"women_only"];
                     [dataDict setValue:_ride.isActice forKey:@"is_active"];
                     [dataDict setValue:_ride.carNumber forKey:@"car_number"];
                     [dataDict setValue:_ride.editedID forKey:@"edited_id"];
                     [dataDict setValue:_ride.carType forKey:@"car_type"];
                     [dataDict setValue:_ride.offeredSeat forKey:@"offered_seats"];
                     [dataDict setValue:_ride.rideID forKey:@"id"];
                     [dataDict setValue:_ride.time forKey:@"time"];
                     [dataDict setValue:_ride.price forKey:@"price"];
                     [dataDict setValue:_ride.carDetails forKey:@"car_details"];
                     [dataDict setValue:_ride.dayString forKey:@"days"];
                     [dataDict setValue:_ride.endGrpID forKey:@"end_group_id"];
                     [dataDict setValue:_ride.theDate forKey:@"tdate"];
                     [dataDict setValue:_ride.userID forKey:@"user_id"];
                     [dataDict setValue:_ride.comment forKey:@"comment"];
                     [dataDict setValue:_ride.phpID forKey:@"phpid"];
                     [dataDict setValue:_ride.OS forKey:@"offered_via"];
                     
                     
                     
                     NSError *error;
                     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                                        options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                          error:&error];
                     
                     NSString *postStr;
                     if (! jsonData) {
                         NSLog(@"Got an error: %@", error);
                         [self.view hideToastActivity];
                     } else {
                         NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                         
                         postStr = [NSString stringWithFormat:@"[%@]",jsonString];
                         
                     }
                     
                     NSError *err = nil;
                     NSURLResponse *response;
                     NSData *localData = nil;
                     
                     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:OFFER_RIDE_URL]];
                     
                     [request setHTTPMethod:@"POST"];
                     [request setHTTPBody:[postStr dataUsingEncoding:NSUTF8StringEncoding]];
                     [self.view makeToastActivity:CSToastPositionCenter];
                     
                     [NSURLConnection sendAsynchronousRequest:request
                                                        queue:[NSOperationQueue mainQueue]
                                            completionHandler:^(NSURLResponse *response,
                                                                NSData *data, NSError *connectionError)
                      {
                          if (data.length > 0 && connectionError == nil)
                              
                          {
                              
                              [self.view hideToastActivity];
                              
                              [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"RideOffered"];
                              
                              CustomPagerViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomPagerViewController"] ;
                              
                              UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeView];
                              
                              [self.navigationController presentViewController:navHome animated:YES completion:nil];
                              
                          }
                          else
                          {
                              [self.view hideToastActivity];
                              
                              CSToastStyle *style;
                              style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                              [self.view makeToast:@"Could not connect to server. Please try again" duration:1 position:CSToastPositionBottom style:style];

                          }
                          
                          
                      }];
                     
                 }
                 @catch (NSException *exception) {
                     [self.view hideToastActivity];
                     NSLog(@"%@", exception);
                 }
             }
             
             
             
         }
         
         else
         {
             [self.view hideToastActivity];
             NSString *alertMessage;

             if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"OfficeEmailVerified"] isEqualToString:@"0"]) {
                 alertMessage = @"For the Security reason, please complete Office email verification.Contact info@groupool.in for any issue";
             }else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"MobileVerified"] isEqualToString:@"0"]) {
                 alertMessage = @"For the Security reason, please complete Mobile verification.";
             }
             
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *actionGoTo = [UIAlertAction actionWithTitle:@"GO TO PROFILE" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                 
                 ProfileVC *profileView = [[self storyboard] instantiateViewControllerWithIdentifier:@"ProfileVC"];
                 UINavigationController *navProfileVC = [[UINavigationController alloc]initWithRootViewController:profileView];
                 [self.navigationController presentViewController:navProfileVC animated:YES completion:nil];
                 
             }];
             
             UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                 
             }];
             [alert addAction:actionGoTo];
             [alert addAction:actionCancel];
             [self presentViewController:alert animated:YES completion:nil];
             
             
             
         }
     }
}

#pragma mark - Time Selection Action & Implementation
- (IBAction)timeSelectorPressed:(id)sender {
    
    self.selectedTime = [[NSDate new] dateByAddingTimeInterval:2700];
   
    NSInteger minuteInterval = 5;
    //clamp date
    NSInteger referenceTimeInterval = (NSInteger)[self.selectedTime timeIntervalSinceReferenceDate];
    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
    
    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
    }
    
    
    self.selectedTime = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedTo5Minutes];
    
    
    ActionSheetDatePicker *aDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Time" datePickerMode:UIDatePickerModeTime  selectedDate:self.selectedTime target:self action:@selector(timeWasSelected:element:) origin:sender cancelAction:@selector(cancelTimeSelection:)];
    aDatePicker.minuteInterval = minuteInterval;
    
    [aDatePicker showActionSheetPicker];
    [aDatePicker setLocale:[NSLocale systemLocale]];
    
}
- (void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    self.selectedTime = selectedTime;
    
    NSString *createdTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];

    [dateFormatter setDateFormat:@"HH:mm"];
    createdTime = [dateFormatter stringFromDate:selectedTime];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];
    NSInteger currHr = [components hour];
    NSInteger currtMin = [components minute];
    
    
    NSInteger stHr = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
    NSInteger stMin = [[[createdTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
    
    NSInteger formStTime = (stHr*60)+stMin;
    
    NSInteger nowTime = (currHr*60) + currtMin;
    
    if(formStTime < nowTime && (daySelection.selectedSegmentIndex == 0) && (daySelection.hidden == NO)) {

        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.messageFont = [UIFont systemFontOfSize:12];
        [self.view makeToast:@"Selected time has already passed!" duration:2 position:CSToastPositionCenter style:style];
        
    }
    else
    {
        [self.btnSelectTime setTitle:createdTime forState:UIControlStateNormal];
       
    }
    
}
- (void)cancelTimeSelection:(id)sender {
    NSLog(@"Time selection canceled..");
}

#pragma mark - Increase & Decrease ride price Action
- (IBAction)decreaseRidePrice:(id)sender {
    NSInteger tmp;
    if (ridePriceDefault > 0 ) {
        tmp = ridePriceDefault - 5;
        ridePriceDefault = tmp;
        
        self.lblRidePrice.text = [NSString stringWithFormat:@"Rs %ld",(long)tmp];
    }
}
- (IBAction)increaseRidePrice:(id)sender {
    NSInteger tmp;
    
    
    if (ridePriceDefault < ridePriceMaximum) {
        tmp = ridePriceDefault + 5;
        ridePriceDefault = tmp;
        
        self.lblRidePrice.text = [NSString stringWithFormat:@"Rs %ld",(long)tmp];
    }
    else{
        
        CSToastStyle *style;
        style.messageFont = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        [self.view makeToast:@"Max price reached. Capped at 5Rs/km" duration:1 position:CSToastPositionCenter style:style];
        
    }
}

#pragma mark - Manage Route Button Action
- (IBAction)manageRouteAction:(id)sender {
    
    
    //    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
    ManageGroupsVC *manageRoute =[self.storyboard instantiateViewControllerWithIdentifier:@"ManageGroupsVC"];
    manageRoute.mDelegate = self;
    UINavigationController *navManageRoute = [[UINavigationController alloc] initWithRootViewController:manageRoute];
    //    [self.navigationController pushViewController:manageRoute   animated:YES];
    [self.navigationController presentViewController:navManageRoute animated:YES completion:nil];
    
    
}



@end
