//
//  FeedVC.m
//  Groupool
//
//  Created by HN on 30/10/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "FeedVC.h"
#import <UIKit/UIKit.h>
#import "UIColor+GroupoolColors.h"
#import "Feeds.h"
#import "CustomTableCell.h"


@interface FeedVC ()
{
    NSMutableArray *feedsArray;
    BOOL isCalled;
}
@end

@implementation FeedVC
@synthesize messageArray,feedTimeArray,txtColourArray,typesArray,userID;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchData];
    isCalled = NO;
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.showsVerticalScrollIndicator = NO;
   
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self
                            action:@selector(fetchData)
                  forControlEvents:UIControlEventValueChanged];
    [self.myTableView addSubview:self.refreshControl];
    
    
}
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"RideRequested"] isEqualToString:@"YES"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"RideRequested"];
        [self fetchData];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"RideOffered"] isEqualToString:@"YES"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"RideOffered"];
        [self fetchData];
    }
    
}

#pragma mark - Tableview Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (feedsArray) {
        return [feedsArray count];
    }
    
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Feeds *newFeed = [feedsArray objectAtIndex:indexPath.row];
    
    if (newFeed.feedID < 5) {
        return 60;
    }
    
    return 80;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Feeds *newFeed = [feedsArray objectAtIndex:indexPath.row];
    
    
    if (newFeed.feedID < 5 )
    {
        
        CustomTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aCell" forIndexPath:indexPath];
        
        cell.feedMessage.text = newFeed.feedMessage;
        
        if ([newFeed.feedColor isEqualToString:@"0"]) {
            cell.feedMessage.textColor = [UIColor blackColor];
        }
        if ([newFeed.feedColor isEqualToString:@"1"]) {
            cell.feedMessage.textColor = [UIColor systemMessageColor];
        }
        if ([newFeed.feedColor isEqualToString:@"2"]) {
            cell.feedMessage.textColor = [UIColor redColor];
        }
        
        return cell;
        
    }
    
    CustomTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    
    
    if ([newFeed.feedColor isEqualToString:@"0"]) {
        cell.feedMessage.textColor = [UIColor blackColor];
    }
    if ([newFeed.feedColor isEqualToString:@"1"]) {
        cell.feedMessage.textColor = [UIColor groupoolMainColor];
    }
    if ([newFeed.feedColor isEqualToString:@"2"]) {
        cell.feedMessage.textColor = [UIColor redColor];
    }
    
    cell.feedMessage.text = newFeed.feedMessage;
    
    
    cell.feedTime.text = [self getFormattedDate:newFeed.feedTime];
    
    if ([newFeed.feedType isEqualToString:@"JG"]) {
        cell.carType.image = [UIImage imageNamed:@"ic_joined.png"];
    }
    if ([newFeed.feedType isEqualToString:@"OF"]) {
        cell.carType.image = [UIImage imageNamed:@"ic_cartype.png"];
    }
    if ([newFeed.feedType isEqualToString:@"JO"]) {
        cell.carType.image = [UIImage imageNamed:@"ic_joined.png"];
    }
    if ([newFeed.feedType isEqualToString:@"RE"]) {
        cell.carType.image = [UIImage imageNamed:@"thumb_press"];
    }
    
    
    return cell;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, tableView.frame.size.width, 18)];
    [label setFont:[UIFont systemFontOfSize:12]];
    label.textAlignment = NSTextAlignmentCenter;
    NSString *string = @"* Feeds from your Groups and other nearby Groups";
    
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:187.0/255.0 green:222.0/255.0 blue:230.0/255.0 alpha:1.0]];
    return view;
}
- (NSString *)getFormattedDate:(NSString *)strDate{
    
    NSDateFormatter *formatter       =   [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *temp =   [formatter dateFromString:strDate];
    
    [formatter setDateFormat:@"dd-MM HH:mm"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSString *returnStr     =   [formatter stringFromDate: temp];
    
    
    
    return returnStr;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Feeds *newFeed = [feedsArray objectAtIndex:indexPath.row];
    
//    if ([newFeed.feedType isEqualToString:@"RE"])
            if ([newFeed.feedType isEqualToString:@"00"])
    {
        CGFloat viewHeight;
        CGFloat viewWidth;
        CGFloat viewX;
        CGFloat viewY;
        // popup view dimensions
        viewHeight = (self.view.frame.size.height/2) + (self.view.frame.size.height/2.5);
        viewWidth  = (self.view.frame.size.width/2) + (self.view.frame.size.width/3);
        viewX = (self.view.frame.size.width/12);
        viewY = (self.view.frame.size.height/20 - 20);
        
        self.customView = [[UIView alloc] initWithFrame:CGRectMake(viewX, viewY, viewWidth, viewHeight)];
        
        self.customView.backgroundColor = [UIColor whiteColor];
        // popup View Shadow
        self.customView.layer.cornerRadius = 3;
        self.customView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.customView.layer.shadowOpacity = 0.8;
        self.customView.layer.shadowRadius = 12;
        self.customView.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
        self.customView.tag = 100;
        
        CGFloat allControlYPos = (self.customView.frame.size.height) - (self.customView.frame.size.height);
        
        CGFloat allControlXPos = (self.customView.frame.size.width) - (self.customView.frame.size.width);
        
        
        // Ride Time Label
        UILabel *feedMessage = [[UILabel alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+46, self.customView.frame.size.width - 30, 85)];
        feedMessage.font = [UIFont systemFontOfSize:14];
        feedMessage.textColor = [UIColor grayTextColor];
        feedMessage.numberOfLines = 4;
        feedMessage.lineBreakMode = NSLineBreakByWordWrapping;
        feedMessage.text = newFeed.feedMessage;
        
        // Ride price label
        
        
        
        // UI Bar
        UIView  *popupBar = [[UIView alloc] initWithFrame:CGRectMake(5, allControlYPos+134, self.customView.frame.size.width-10, 2)];
        popupBar.backgroundColor = [UIColor colorWithRed:242.00/255.00 green:242.00/255.00 blue:242.00/255.00 alpha:1];
        
        // Ride Locations(From and To) Label
        UILabel *popupFromRideLocations = [[UILabel alloc] initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+138, self.customView.frame.size.width - 30, 120)];
        
        popupFromRideLocations.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        popupFromRideLocations.textColor = [UIColor grayTextColor];
        
        popupFromRideLocations.lineBreakMode = NSLineBreakByWordWrapping;
        popupFromRideLocations.numberOfLines = 0;
        
        
        
        // User Pic Imageview
        UIImageView *popupUserImage = [[UIImageView alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+260, 52, 52)];
        popupUserImage.layer.cornerRadius = 26;
        popupUserImage.layer.masksToBounds = YES;
        
        
        // User Name Label
        UILabel *popupUserName = [[UILabel alloc]initWithFrame:CGRectMake(allControlXPos+8, allControlYPos+312, 100, 36)];
        popupUserName.font = [UIFont systemFontOfSize:17];
        popupUserName.numberOfLines = 2;
        popupUserName.lineBreakMode = NSLineBreakByWordWrapping;
        popupUserName.textColor = [UIColor groupoolMainColor];
        popupUserName.textAlignment = NSTextAlignmentLeft;
        
        //Works At Label
        UILabel *popupCompanyName = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 205, allControlYPos+295, 195, 15)];
        popupCompanyName.textColor = [UIColor grayTextColor];
        popupCompanyName.font = [UIFont systemFontOfSize:13];
        popupCompanyName.textAlignment = NSTextAlignmentRight;
        
        //Mobile Verified Label
        UILabel *popupMobileVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 142, allControlYPos+312, 132, 15)];
        popupMobileVerified.font = [UIFont systemFontOfSize:13];
        popupMobileVerified.textColor = [UIColor grayTextColor];
        popupMobileVerified.textAlignment = NSTextAlignmentRight;
        
        
        //Office Email Verified
        UILabel *popupOfficeEmailVerified = [[UILabel alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 158, allControlYPos+329, 148, 15)];
        popupOfficeEmailVerified.font = [UIFont systemFontOfSize:13];
        popupOfficeEmailVerified.textColor = [UIColor grayTextColor];
        popupOfficeEmailVerified.textAlignment = NSTextAlignmentRight;
        //
        //Comment From car owner Label
        
        // OK Button
        
        
        UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - 35, allControlYPos+5, 30, 30)];
        
        btnOK.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnOK setTitle:@"OK" forState:UIControlStateNormal];
        [btnOK setTitleColor:[UIColor colorWithRed:81.00/255.00 green:165.00/255.00 blue:243.00/255.00 alpha:1] forState:UIControlStateNormal];
        
        
        [btnOK addTarget:self action:@selector(closePopup:)  forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *btnCall = [[UIButton alloc] initWithFrame:CGRectMake(allControlXPos+25, self.customView.frame.size.height - 40, 50, 30)];
        btnCall.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnCall setTitle:@"CALL" forState:UIControlStateNormal];
        [btnCall setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
        
        UIButton *btnSMS = [[UIButton alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - self.customView.frame.size.width/2, self.customView.frame.size.height - 40, 50, 30)];
        btnSMS.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnSMS setTitle:@"SMS" forState:UIControlStateNormal];
        [btnSMS setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
        
        UIButton *btnChat = [[UIButton alloc] initWithFrame:CGRectMake(self.customView.frame.size.width - self.customView.frame.size.width/4, self.customView.frame.size.height - 40, 50, 30)];
        btnChat.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [btnChat setTitle:@"CHAT" forState:UIControlStateNormal];
        [btnChat setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
        
        feedMessage.backgroundColor = [UIColor lightGrayColor];
        popupFromRideLocations.backgroundColor = [UIColor lightGrayColor];
        popupUserName.backgroundColor = [UIColor lightGrayColor];
        popupCompanyName.backgroundColor = [UIColor lightGrayColor];
        popupMobileVerified.backgroundColor = [UIColor lightGrayColor];
        popupOfficeEmailVerified.backgroundColor = [UIColor lightGrayColor];
        btnOK.backgroundColor = [UIColor lightGrayColor];
        btnCall.backgroundColor = [UIColor lightGrayColor];
        btnSMS.backgroundColor = [UIColor lightGrayColor];
        btnChat.backgroundColor = [UIColor lightGrayColor];
        
        // Add popup controls to popupview
        [self.customView addSubview:feedMessage];
        [self.customView addSubview:popupFromRideLocations];
        [self.customView addSubview:popupBar];
        [self.customView addSubview:popupUserImage];
        [self.customView addSubview:popupUserName];
        [self.customView addSubview:popupCompanyName];
        [self.customView addSubview:popupMobileVerified];
        [self.customView addSubview:popupOfficeEmailVerified];
        
        [self.customView addSubview:btnOK];
        [self.customView addSubview:btnCall];
        [self.customView addSubview:btnSMS];
        [self.customView addSubview:btnChat];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSURLSession *session = [NSURLSession sharedSession];
            [[session dataTaskWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@/%ld",GET_USER_DETAILS_URL,userID,newFeed.feedStartGrpID,newFeed.feedEndGrpID,(long)newFeed.feedID]]
                    completionHandler:^(NSData *data,
                                        NSURLResponse *response,
                                        NSError *error) {
                        // handle response
                        
                        if (data.length > 0 && !error)
                        {
                            NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

                            NSString *startLocation;
                            NSString *endLocation;
                            UIImage *image;
                            NSString *userName;
                            NSString *companyName;
                            NSString *isMobileVerified;
                            NSString *isEmailVerified;
                            
                            for (NSDictionary *detail in jsonArray)
                            {
                                startLocation = detail[@"start_group_location"];
                                endLocation = detail[@"end_group_location"];
                                NSURL *imageURL = [NSURL URLWithString:detail[@"profile_pic"]];
                                NSData *imageData = [NSData dataWithContentsOfURL: imageURL];
                                
                                image = [[UIImage alloc] initWithData:imageData];
                                userName = detail[@"name"];
                                companyName = detail[@"company"];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                
                                
                                
                                popupFromRideLocations.text = [NSString stringWithFormat:@"%@ \n>>\n%@",startLocation,endLocation];
                                if (image)
                                {
                                    popupUserImage.image = image;
                                }
                                popupUserName.text = userName;
                                popupCompanyName.text = [NSString stringWithFormat:@"Works at: %@",companyName];
                                if ([isMobileVerified isEqualToString:@"1"]) {
                                    popupMobileVerified.text = @"Mobile Verified: Yes";
                                }else{
                                    popupMobileVerified.text = @"Mobile Verified: No";
                                }
                                if ([isEmailVerified isEqualToString:@"1"]) {
                                    popupOfficeEmailVerified.text = @"Office Email Verified:Yes";
                                }else{
                                    popupOfficeEmailVerified.text = @"Office Email Verified:No";
                                }
                                
                                
                                
                                // Show custom view animated
                                self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                                [self.view addSubview:self.customView];
                                [UIView animateWithDuration:0.1
                                                 animations:
                                 ^{
                                     self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                                     self.myTableView.userInteractionEnabled = NO;
                                     self.navigationController.navigationBar.userInteractionEnabled = NO;
                                 }];
                            });
                            
                        }
                        else
                        {
                            
                            NSLog(@"Connection could not be made");
                            
                        }
                        
                    }] resume];
            
        });
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
/************ Ride Details popup close Implementation **************/
- (void) closePopup:(id)sender {
    
    
    
    self.customView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    
    [UIView animateWithDuration:0.5
                     animations:
     ^{
         self.customView.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
         self.myTableView.userInteractionEnabled = YES;
         
     }
     ];
    
    [self.customView removeFromSuperview];
    
}


#pragma mark - Get Data from Server
- (void)fetchData {
    @try {
        {
            {
                userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
                
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",GET_FEED_URL,userID]];
                
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response,
                                                           NSData *data, NSError *connectionError)
                {
                    if (data.length > 0 && connectionError == nil)
                    {
                        
                        NSError *e = nil;
                        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
                        
                        feedsArray = [NSMutableArray new];
                        
                        for (NSDictionary *names in jsonArray) {
                            
                            Feeds *newFeed = [[Feeds alloc] init];
                            
                            newFeed.feedMessage = [names objectForKey:@"message"];
                            newFeed.feedTime = [names objectForKey:@"created_time"];
                            newFeed.feedType = [names objectForKey:@"type"];
                            newFeed.feedColor = [names objectForKey:@"color"];
                            newFeed.feedID = [[names objectForKey:@"id"] integerValue];
                            newFeed.feedStartGrpID = [names objectForKey:@"start_group_id"];
                            newFeed.feedEndGrpID = [names objectForKey:@"end_group_id"];
                            [feedsArray addObject:newFeed];
                            
                        }
                        
                        if (self.refreshControl) {
                            [self.refreshControl endRefreshing];
                        }
                        
                        [self reloadTableData];
                        
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Could not load Feeds" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            [self reconnectToServer];
                            
                        }];
                        [alert addAction:alertAction];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        
                    }
                }];
            }
        }
    }
    @catch (NSException *exception) {
        
        NSLog(@"%@",exception.description);
    }
    
    
}
- (void)reloadTableData{
    
    // Reload table data
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    [self.myTableView reloadData];
        
    });
    
}
- (void)reconnectToServer {
    if (isCalled) {
        [self fetchData];
        isCalled = YES;
    }
    
}
@end
