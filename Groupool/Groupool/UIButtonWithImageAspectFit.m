//
//  UIButtonWithImageAspectFit.m
//  Groupool
//
//  Created by Parth Pandya on 02/05/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "UIButtonWithImageAspectFit.h"

@implementation UIButtonWithImageAspectFit

- (void) awakeFromNib {
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
}


@end
