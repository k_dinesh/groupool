//
//  MyChatsVC.m
//  Groupool
//
//  Created by HN on 02/11/15.
//  Copyright (c) 2015 Varahi. All rights reserved.
//

#import "MyChatsVC.h"
#import "UIColor+GroupoolColors.h"

@interface MyChatsVC ()

@end

@implementation MyChatsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"My Chats";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} ];
    
    self.navigationController.navigationBar.barTintColor = [UIColor groupoolMainColor];
    
    // Back Button
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [menuButton setImage:[UIImage imageNamed:@"ic_back_1.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton ];
}


- (void)backButtonAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
